﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Domain
{
	/// <summary>
	/// Domain obejct for the posts that are intended to be displayed.
	/// </summary>
	public class DisplayPostEntry
	{
		public ulong PostId { get; set; }
		public string Title { get; set; }
		public string Body { get; set; }
		public int Score { get; set; }
		public DateTime CreationDate { get; set; }
		public ulong? AcceptedAnswerId { get; set; }
		public string Username { get; set; }
		public int CommentCount { get; set; }
		public bool Favourite { get; set; }
		public string Annotation { get; set; }
	}
}
