﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Domain
{
	/// <summary>
	/// Domain object for a post search entry.
	/// </summary>
    public class PostSearchEntry
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public int AnswerCount { get; set; }
        public string ValidatedAnswer { get; set; }
        public string Author { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Favourite { get; set; }
        public string Annotation { get; set; }
        public ulong Id { get; set; }
    }
}
