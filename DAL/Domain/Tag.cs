﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Domain
{
	/// <summary>
	/// Domain object for a tag.
	/// </summary>
    public class Tag
    {
        public ulong Id { get; set; }
        public string TagName { get; set; }
        public long Count { get; set; }
    }
}
