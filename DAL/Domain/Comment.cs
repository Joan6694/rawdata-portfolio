﻿using System;

namespace DAL.Domain
{
	/// <summary>
	/// Domain object for the comments.
	/// </summary>
    public class Comment
    {
        public ulong Id { get; set; }
        public ulong PostId { get; set; }
        public int Score { get; set; }
        public string Text { get; set; }
        public DateTime CreationDate { get; set; }
        public ulong UserId { get; set; }
    }
}
