﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Domain
{
	/// <summary>
	/// Domain objects for the comments that are intended to be displayed.
	/// </summary>
	public class DisplayCommentEntry
	{
		public ulong PostId { get; set; }
		public string Text { get; set; }
		public int Score { get; set; }
		public DateTime CreationDate { get; set; }
		public string Username { get; set; }
	}
}
