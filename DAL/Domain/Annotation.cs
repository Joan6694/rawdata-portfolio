﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Domain
{
	/// <summary>
	/// Domain object for the annotations.
	/// </summary>
    public class Annotation
    {
        public ulong PostId { get; set; }
        public bool Favourite { get; set; }
        public string Text { get; set; }
    }
}
