﻿namespace DAL.Domain
{
	/// <summary>
	/// Domain object for the users.
	/// </summary>
    public class User
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
    }
}
