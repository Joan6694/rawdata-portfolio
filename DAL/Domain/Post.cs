﻿using System;

namespace DAL.Domain
{
	/// <summary>
	/// Domain object for the posts.
	/// </summary>
    public class Post
    {
        public ulong Id { get; set; }
        public int PostTypeId { get; set; }
        public ulong? ParentId { get; set; }
        public ulong? AcceptedAnswerId { get; set; }
        public DateTime CreationDate { get; set; }
        public int Score { get; set; }
        public int ViewCount { get; set; }
        public string Body { get; set; }
        public ulong? OwnerUserId { get; set; }
        public string Title { get; set; }
        public string Tags { get; set; }
        public int AnswerCount { get; set; }
        public int CommentCount { get; set; }
    }
}
