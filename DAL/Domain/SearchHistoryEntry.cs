﻿using System;

namespace DAL.Domain
{
	/// <summary>
	/// Domain object for a search history entry.
	/// </summary>
    public class SearchHistoryEntry
    {
        public ulong Id { get; set; }
        public string SearchString { get; set; }
        public DateTime Date { get; set; }
        public int Mark { get; set; }
        public int ResultsNumber { get; set; }
        public short SearchType { get; set; }
    }
}
