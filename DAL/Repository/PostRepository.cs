﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for Posts
    /// </summary>
    public class PostRepository : IRepository<Post>
    {
        /// <summary>
        /// Select all field in Post: 
        /// Id, PostTypeId, ParentId, AcceptedAnswerId, CreationDate, Score, ViewCount, Body,
        /// OwnerUserId, Title, Tags, AnswerCount, CommentCount
        /// </summary>
        /// <param name="limit"> limit the number of information selected, default 25</param>
        /// <param name="offset"> "pagination like", default 0</param>
        /// <returns>return list of object Post</returns>
        public IEnumerable<Post> GetAll(int limit = 25, int offset = 0)
        {
            string sql = string.Format(
                "SELECT * FROM posts LIMIT {0} OFFSET {1}",
                limit, offset);
            foreach (var post in ExecuteQuery(sql))
            {
                yield return post;
            }

        }

        /// <summary>
        /// Get the post matching the id given as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Get a specific post</returns>
        public Post GetById(ulong id)
        {
            var sql = string.Format(
                    "select * from posts where Id =  {0}", id);
            return ExecuteQuery(sql).FirstOrDefault();
        }

        /// <summary>
        /// Do to request and get the Post object matching
        /// </summary>
        /// <param name="sql">Request string</param>
        /// <returns>Return an object post</returns>
        private static IEnumerable<Post> ExecuteQuery(string sql)
        {
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(sql, connection);

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.HasRows && rdr.Read())
                    {
                        yield return new Post
                        {
							Id = rdr.GetUInt64("Id"),
							PostTypeId = rdr.GetInt32("PostTypeId"),
							ParentId = rdr.GetNullUInt64Safe("ParentId"),
 							AcceptedAnswerId = rdr.GetNullUInt64Safe("AcceptedAnswerId"),
                            CreationDate = rdr.GetDateTime("CreationDate"),
							Score = rdr.GetInt32("Score"),
							ViewCount = rdr.GetInt32Safe("ViewCount"),
                            Body = rdr.GetString("Body"),
							OwnerUserId = rdr.GetNullUInt64Safe("OwnerUserId"),
                            Title = rdr.GetStringSafe("Title"),
                            Tags = rdr.GetStringSafe("Tags"),
							AnswerCount = rdr.GetInt32Safe("AnswerCount"),
							CommentCount = rdr.GetInt32Safe("CommentCount")
                        };
                    }
                }
            }
        } 
    }
}
