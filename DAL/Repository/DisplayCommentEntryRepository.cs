﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for Comment Entry : use a stored procedure
    /// </summary>
    public class DisplayCommentEntryRepository
    {
        /// <summary>
        /// Get the comments of a post specifiying the Id of the post
        /// </summary>
        /// <param> name="SearchPostId"the Id of the post whose comments we want to see</param>
        /// <returns></returns>
        public IEnumerable<DisplayCommentEntry> GetResult(int PostId)
        {
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand();

                cmd.CommandText = "DetailsPosts_Comments";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SearchPostId", PostId);

                cmd.Connection = connection;

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.HasRows && rdr.Read())
                    {
                        yield return new DisplayCommentEntry
                        {
                            PostId = rdr.GetUInt64("PostId"),
                            Text = rdr.GetStringSafe("Text"),
                            Score = rdr.GetInt32("Score"),
                            Username = rdr.GetString("Username"),
                            CreationDate = rdr.GetDateTime("CreationDate"),
                        };
                    }
                }
                connection.Close();
            }

        }
    }
}
