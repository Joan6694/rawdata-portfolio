﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
	/// <summary>
	/// Interface for the generic read only repositories
	/// </summary>
	/// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
		/// <summary>
		/// Get all the resources with a limit of <paramref name="limit"/> and an offset of <paramref name="offset"/>.
		/// </summary>
		/// <param name="limit"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
        IEnumerable<T> GetAll(int limit = 25, int offset = 0);
		/// <summary>
		/// Get a specific resource specified by the id <paramref name="id"/>
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        T GetById(ulong id);
    }
}
