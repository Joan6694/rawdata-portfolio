﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
	/// <summary>
	/// Interface for the generic read/write repositories.
	/// </summary>
	/// <typeparam name="T"></typeparam>
    public interface IUpdatableRepository<T> : IRepository<T> where T: class
    {
		/// <summary>
		/// Insert a new resource.
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
        bool Insert(T entity);
		/// <summary>
		/// Update the resource identified by the id <paramref name="id"/> with the filed contained in the <paramref name="entity"/> object.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="id"></param>
		/// <returns></returns>
        bool Update(T entity, ulong id);
		/// <summary>
		/// Delete the resource identified by the id <paramref name="id"/>
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        bool Delete(ulong id);
    }
}
