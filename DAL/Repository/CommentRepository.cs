﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for comments
    /// </summary>
    public class CommentRepository : IRepository<Comment>
    {
		/// <summary>
		/// Select all field in Comment
		/// Id, PostId, Score, Text, CreationDate, UserId
		/// </summary>
		/// <param name="limit"> limit the number of information selected, default 25</param>
		/// <param name="offset"> "pagination like", default 0</param>
		/// <returns>return list of object Comment</returns>
		public IEnumerable<Comment> GetAll(int limit = 25, int offset = 0)
		{
			string sql = string.Format(
				"SELECT * FROM comments LIMIT {0} OFFSET {1}",
				limit, offset);
			foreach (var comment in ExecuteQuery(sql))
			{
				yield return comment;
			}

		}

		/// <summary>
		/// Get the comments of a certain post.
		/// </summary>
		/// <param name="postId"></param>
		/// <returns></returns>
		public IEnumerable<Comment> GetPostComments(ulong postId)
		{
			string sql = string.Format(
				"SELECT * FROM comments WHERE PostId = {0}", postId);
			foreach (var comment in ExecuteQuery(sql))
			{
				yield return comment;
			}
		}

        /// <summary>
        /// Get the Comment matching the id given as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Get a specific Comment</returns>
        public Comment GetById(ulong id)
        {
            var sql = string.Format(
                    "select * from comments where Id =  {0}", id);
            return ExecuteQuery(sql).FirstOrDefault();
        }

        /// <summary>
        /// Do to request and get the Comment object matching
        /// </summary>
        /// <param name="sql">Request string</param>
        /// <returns>Return an object Comment</returns>
        private static IEnumerable<Comment> ExecuteQuery(string sql)
        {
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(sql, connection);

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.HasRows && rdr.Read())
                    {
                        yield return new Comment
                        {
							Id = rdr.GetUInt64("Id"),
							PostId = rdr.GetUInt64("PostId"),
							Score = rdr.GetInt32("Score"),
                            Text = rdr.GetString("Text"),
                            CreationDate = rdr.GetDateTime("CreationDate"),
							UserId = rdr.GetUInt64("UserId")
                        };
                    }
                }
            }
        }
    }
}