﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
	/// <summary>
	/// Class that adds some extensions methods to the MySqlDataReader class.
	/// </summary>
	public static class DALExtensions
	{
		// STRING

		/// <summary>
		/// Returns the string value of the row having the index <paramref name="colIndex"/>. If the value is null, doesn't throw an exception but returns a null value instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="colIndex"></param>
		/// <returns></returns>
		public static string GetStringSafe(this MySqlDataReader reader, int colIndex)
		{
			return GetStringSafe(reader, colIndex, null);
		}

		/// <summary>
		/// Returns the string value of the row having the index <paramref name="colIndex"/>. If the value is null, doesn't throw an exception but returns the value <paramref name="defaultValue"/> instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="colIndex"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static string GetStringSafe(this MySqlDataReader reader, int colIndex, string defaultValue)
		{
			if (!reader.IsDBNull(colIndex))
				return reader.GetString(colIndex);
			return defaultValue;
		}

		/// <summary>
		/// Returns the string value of the row having the column name <paramref name="indexName"/>. If the value is null, doesn't throw an exception but returns a null value instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="indexName"></param>
		/// <returns></returns>
		public static string GetStringSafe(this MySqlDataReader reader, string indexName)
		{
			return GetStringSafe(reader, reader.GetOrdinal(indexName));
		}

		/// <summary>
		/// Returns the string value of the row having the column name <paramref name="indexName"/>. If the value is null, doesn't throw an exception but returns the value <paramref name="defaultValue"/> instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="indexName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static string GetStringSafe(this MySqlDataReader reader, string indexName, string defaultValue)
		{
			return GetStringSafe(reader, reader.GetOrdinal(indexName), defaultValue);
		}

		// NULL UINT64

		/// <summary>
		/// Returns the nullable uint64 value of the row having the index <paramref name="colIndex"/>. If the value is null, doesn't throw an exception but returns a null value instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="colIndex"></param>
		/// <returns></returns>
		public static ulong? GetNullUInt64Safe(this MySqlDataReader reader, int colIndex)
		{
			return GetNullUInt64Safe(reader, colIndex, null);
		}

		/// <summary>
		/// Returns the nullable uint64 value of the row having the index <paramref name="colIndex"/>. If the value is null, doesn't throw an exception but returns the value <paramref name="defaultValue"/> instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="colIndex"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static ulong? GetNullUInt64Safe(this MySqlDataReader reader, int colIndex, ulong? defaultValue)
		{
			if (!reader.IsDBNull(colIndex))
				return reader.GetUInt64(colIndex);
			return defaultValue;
		}

		/// <summary>
		/// Returns the nullable uint64 value of the row having the column name <paramref name="indexName"/>. If the value is null, doesn't throw an exception but returns a null value instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="indexName"></param>
		/// <returns></returns>
		public static ulong? GetNullUInt64Safe(this MySqlDataReader reader, string indexName)
		{
			return GetNullUInt64Safe(reader, reader.GetOrdinal(indexName));
		}

		/// <summary>
		/// Returns the nullable uint64 value of the row having the column name <paramref name="indexName"/>. If the value is null, doesn't throw an exception but returns the value <paramref name="defaultValue"/> instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="indexName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static ulong? GetNullUInt64Safe(this MySqlDataReader reader, string indexName, ulong? defaultValue)
		{
			return GetNullUInt64Safe(reader, reader.GetOrdinal(indexName), defaultValue);
		}

		/// <summary>
		/// Returns the int32 value of the row having the index <paramref name="colIndex"/>. If the value is null, doesn't throw an exception but returns default(int) instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="colIndex"></param>
		/// <returns></returns>
		public static int GetInt32Safe(this MySqlDataReader reader, int colIndex)
		{
			return GetInt32Safe(reader, colIndex, default(int));
		}

		/// <summary>
		/// Returns the int32 value of the row having the index <paramref name="colIndex"/>. If the value is null, doesn't throw an exception but returns the value <paramref name="defaultValue"/> instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="colIndex"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static int GetInt32Safe(this MySqlDataReader reader, int colIndex, int defaultValue)
		{
			if (!reader.IsDBNull(colIndex))
				return reader.GetInt32(colIndex);
			return defaultValue;
		}

		/// <summary>
		/// Returns the int32 value of the row having the column name <paramref name="indexName"/>. If the value is null, doesn't throw an exception but returns default(int) instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="indexName"></param>
		/// <returns></returns>
		public static int GetInt32Safe(this MySqlDataReader reader, string indexName)
		{
			return GetInt32Safe(reader, reader.GetOrdinal(indexName));
		}

		/// <summary>
		/// Returns the int32 value of the row having the column name <paramref name="indexName"/>. If the value is null, doesn't throw an exception but returns the value <paramref name="defaultValue"/> instead.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="indexName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static int GetInt32Safe(this MySqlDataReader reader, string indexName, int defaultValue)
		{
			return GetInt32Safe(reader, reader.GetOrdinal(indexName), defaultValue);
		}
	}
}
