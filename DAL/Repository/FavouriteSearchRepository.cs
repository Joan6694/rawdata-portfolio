﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for Search History: use a store procedure
    /// </summary>
    public class FavouriteSearchRepository
    {
        /// <summary>
        /// Get the result of the favourites  depending of the user entry from a Stored procedure
        /// </summary>
        /// <param name="uSearch">user search</param>
        /// <param name="uIsWithBody">parameter to look for the keywords in the body</param>
        /// <param name="uIsExact">ask for an exact match on the order of the words</param>
        /// <param name="uIsAnnotated">get only annotated favourites</param>
        /// <param name="uIsSolved">get only favourites solved</param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public IEnumerable<FavouriteSearchEntry> GetResult(string uSearch, bool uIsWithBody, bool uIsExact, bool uIsAnnotated, bool uIsSolved,
            int limit = 25, int offset = 0)
        {
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand();

                cmd.CommandText = "Result_FavouritesPost";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserSearch", uSearch);
                cmd.Parameters.AddWithValue("@IsWithBody", uIsWithBody);
                cmd.Parameters.AddWithValue("@IsExact", uIsExact);
                cmd.Parameters.AddWithValue("@IsAnnotated", uIsAnnotated);
                cmd.Parameters.AddWithValue("@IsSolved", uIsSolved);
                cmd.Parameters.AddWithValue("@ULimit", limit);
                cmd.Parameters.AddWithValue("@UOffset", offset);

                cmd.Connection = connection;

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.HasRows && rdr.Read())
                    {
                        yield return new FavouriteSearchEntry
                        {
                            Title = rdr.GetStringSafe("Title"),
                            Body = rdr.GetStringSafe("Body"),
                            AnswerCount = rdr.GetInt32("AnswerCount"),
                            ValidatedAnswer = rdr.GetString("ValidatedAnswer"),
                            Author = rdr.GetString("Author"),
                            CreationDate = rdr.GetDateTime("CreationDate"),
                            Annotation = rdr.GetString("Annotations"),
                            Id = rdr.GetUInt64("Id"),
                        };
                    }
                }
                connection.Close();
            }

        }
    }
}
