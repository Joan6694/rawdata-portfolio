﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for Posts Entries : use a stored procedure
    /// </summary>
	public class DisplayPostEntryRepository
	{
		/// <summary>
		/// Returns a list of posts containing the question post (which is identified by <paramref name="uPostId"/>) and all the answers to this post.
		/// </summary>
		/// <param name="uPostId"></param>
		/// <returns></returns>
		public IEnumerable<DisplayPostEntry> GetResult(int uPostId)
		{
			using (
				MySqlConnection connection =
					new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
			{
				connection.Open();

				MySqlCommand cmd = new MySqlCommand();

				cmd.CommandText = "DetailsPosts";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@SearchPostId", uPostId);

				cmd.Connection = connection;

				using (var rdr = cmd.ExecuteReader())
				{
					while (rdr.HasRows && rdr.Read())
					{
						yield return new DisplayPostEntry
						{
							PostId = rdr.GetUInt64("Id"),
							Title = rdr.GetStringSafe("Title"),
							Body = rdr.GetStringSafe("Body"),
							Score = rdr.GetInt32("Score"),
							CreationDate = rdr.GetDateTime("CreationDate"),
							AcceptedAnswerId = rdr.GetNullUInt64Safe("AcceptedAnswerId"),
							Username = rdr.GetString("Username"),
							CommentCount = rdr.GetInt32("CommentCount"),
							Favourite = rdr.GetInt32("Favourite") == 1,
							Annotation = rdr.GetStringSafe("Annotation"),
						};
					}
				}
				connection.Close();
			}

		}

	}
}
