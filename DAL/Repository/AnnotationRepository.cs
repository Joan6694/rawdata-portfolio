﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for annotations : use a stored procedure
    /// </summary>
    public class AnnotationRepository : IUpdatableRepository<Annotation>
    {
        /// <summary>
        /// Select all field in Annotation:
        /// PostId, Favourite, Text
        /// </summary>
        /// <param name="limit"> limit the number of information selected, default 25</param>
        /// <param name="offset"> "pagination like", default 0</param>
        /// <returns>return list of object Annotation</returns>
        public IEnumerable<Annotation> GetAll(int limit = 25, int offset = 0)
        {
            string sql = string.Format(
                "SELECT * FROM postannotation LIMIT {0} OFFSET {1}",
                limit, offset);
            foreach (var annotation in ExecuteQuery(sql))
            {
                yield return annotation;
            }

        }

        /// <summary>
        /// Get the annotation matching the id given as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Get a specific Annotation</returns>
        public Annotation GetById(ulong id)
        {
            var sql = string.Format(
                    "select * from postannotation where PostId =  {0}", id);
            return ExecuteQuery(sql).FirstOrDefault();
        }


        /// <summary>
        /// Do to request and get the Annotation object matching
        /// </summary>
        /// <param name="sql">Request string</param>
        /// <returns>Return an object Annotation</returns>
        private static IEnumerable<Annotation> ExecuteQuery(string sql)
        {
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(sql, connection);

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.HasRows && rdr.Read())
                    {
                        yield return new Annotation
                        {
                            PostId = rdr.GetUInt64("PostId"),
                            Favourite = rdr.GetBoolean("Favourite"),
                            Text = rdr.GetStringSafe("Annotation")
                        };
                    }
                }
            }
        }

		/// <summary>
		/// Inserts a new annotation in the database. This function locks the postannotation table in WRITE and the posts table in READ.
		/// </summary>
		/// <param name="entity">The new annotation to insert</param>
		/// <returns>True if the insertion went well, false otherwise</returns>
        public bool Insert(Annotation entity)
        {
            bool insertionWentOk = true;
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand("LOCK TABLES postannotation WRITE, posts READ", connection);
                cmd.ExecuteNonQuery();

                cmd = new MySqlCommand();

				//try
                {
                    cmd.CommandText = "Insert_Annotation";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pid", entity.PostId);
                    cmd.Parameters.AddWithValue("@fav", entity.Favourite);
                    cmd.Parameters.AddWithValue("@anno", entity.Text);
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
	                entity.PostId = (ulong)cmd.LastInsertedId;
                }
				//catch (MySqlException)
				//{
				//	insertionWentOk = false;
				//}
                cmd = new MySqlCommand("UNLOCK TABLES", connection);
                cmd.ExecuteNonQuery();
                connection.Close();
			}
            return insertionWentOk;
        }

		/// <summary>
		/// Updates the annotation having the id <paramref name="id"/> with the fields given in the <paramref name="entity"/> object.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="id"></param>
		/// <returns>True if the update wentwell, false otherwise</returns>
        public bool Update(Annotation entity, ulong id)
        {
            bool updateWentOk = true;
            using (
               MySqlConnection connection =
                   new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand();

                try
                {
                    cmd.CommandText = "Update_Annotation";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pid", id);
                    cmd.Parameters.AddWithValue("@fav", entity.Favourite);
                    cmd.Parameters.AddWithValue("@anno", entity.Text);
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                catch (SqlException)
                {
                    updateWentOk = false;
                }
            }
            return updateWentOk;
        }

		/// <summary>
		/// Delete an annotation having the id <paramref name="id"/>.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>True if the delete went well, false otherwise</returns>
        public bool Delete(ulong id)
        {
            bool deleteWentOk = true;
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand();

                try
                {
                    cmd.CommandText = string.Format("Delete_Annotation_Text");
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pid", id);
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                catch (MySqlException)
                {
                    deleteWentOk = false;
                }
            }
            return deleteWentOk;
        }
    }
}