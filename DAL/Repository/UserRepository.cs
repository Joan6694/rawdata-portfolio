﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for users
    /// </summary>
    public class UserRepository : IRepository<User>
    {

        /// <summary>
        /// Select all field in User:
        /// Id, Name, UpVotes, DownVotes
        /// </summary>
        /// <param name="limit"> limit the number of information selected, default 25</param>
        /// <param name="offset"> "pagination like", default 0</param>
        /// <returns>return list of object user</returns>
        public IEnumerable<User> GetAll(int limit = 25, int offset = 0)
        {
            string sql = string.Format(
                "SELECT * FROM users LIMIT {0} OFFSET {1}",
                limit, offset);
            foreach (var user in ExecuteQuery(sql))
            {
                yield return user;
            }

        }

        /// <summary>
        /// Get the User matching the id given as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Get a specific User</returns>
        public User GetById(ulong id)
        {
            var sql = string.Format(
                    "select * from users where Id =  {0}", id);
            return ExecuteQuery(sql).FirstOrDefault();
        }

        /// <summary>
        /// Do to request and get the User object matching
        /// </summary>
        /// <param name="sql">Request string</param>
        /// <returns>Return an object user</returns>
        private static IEnumerable<User> ExecuteQuery(string sql)
        {
            using (
                MySqlConnection connection =
                    new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(sql, connection);

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.HasRows && rdr.Read())
                    {
                        yield return new User
                        {
                            Id = rdr.GetUInt64("Id"),
                            Name = rdr.GetString("Name"),
                            UpVotes = rdr.GetInt32("UpVotes"),
							DownVotes = rdr.GetInt32("DownVotes")
                        };
                    }
                }
            }
        }
    }
}