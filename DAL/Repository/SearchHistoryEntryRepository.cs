﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
	/// <summary>
	/// Repository for Search History Entry : use a stored procedure
	/// </summary>
	public class SearchHistoryEntryRepository
	{
		/// <summary>
		/// Get the result of the history depending of the user entry from a Stored procedure
		/// </summary>
		/// <param name="uSearch">User string search</param>
		/// <param name="uMarked">User mark request</param>
		/// <param name="limit"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public IEnumerable<SearchHistoryEntry> GetResult(string uSearch, int uMarked = -1,
			int limit = 25, int offset = 0)
		{
			using (
				MySqlConnection connection =
					new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
			{
				connection.Open();

				MySqlCommand cmd = new MySqlCommand();

				cmd.CommandText = "SearchHistory";
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("@UserSearch", uSearch);
				cmd.Parameters.AddWithValue("@Marked", uMarked);
				cmd.Parameters.AddWithValue("@ULimit", limit);
				cmd.Parameters.AddWithValue("@UOffset", offset);
				cmd.Connection = connection;

				using (var rdr = cmd.ExecuteReader())
				{
					while (rdr.HasRows && rdr.Read())
					{
						yield return new SearchHistoryEntry
						{
							Id = rdr.GetUInt64("Id"),
							SearchString = rdr.GetStringSafe("SearchString"),
							Date = rdr.GetDateTime("Date"),
							Mark = rdr.GetInt32("Mark"),
							ResultsNumber = rdr.GetInt32("ResultsNumber"),
							SearchType = rdr.GetInt16("SearchType")
						};
					}
				}
				connection.Close();
			}

		}

		/// <summary>
		/// Get the search history entry matching the <paramref name="searchString"/> and the searchType (represented by the four booleans).
		/// </summary>
		/// <param name="searchString"></param>
		/// <param name="uIsWithBody"></param>
		/// <param name="uIsExact"></param>
		/// <param name="uIsAnnotated"></param>
		/// <param name="uIsSolved"></param>
		/// <returns></returns>
		public SearchHistoryEntry GetBySearchStringAndType(string searchString, bool uIsWithBody, bool uIsExact, bool uIsAnnotated, bool uIsSolved)
		{
			int type = Convert.ToInt32(uIsWithBody) | (Convert.ToInt32(uIsExact) << 1) | (Convert.ToInt32(uIsAnnotated) << 2) |
					   (Convert.ToInt32(uIsSolved) << 3);
			string sql = "select * from searchhistory where SearchString = @SearchString AND SearchType = @SearchType";
			SearchHistoryEntry res = null;
			using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
			{
				connection.Open();

				MySqlCommand cmd = new MySqlCommand(sql, connection);

				cmd.Parameters.AddWithValue("@SearchString", searchString);
				cmd.Parameters.AddWithValue("@SearchType", type);

				using (var rdr = cmd.ExecuteReader())
				{
					if (rdr.HasRows && rdr.Read())
					{
						res = new SearchHistoryEntry
						{
							Id = rdr.GetUInt64("Id"),
							SearchString = rdr.GetStringSafe("SearchString"),
							Date = rdr.GetDateTime("Date"),
							Mark = rdr.GetInt32("Mark"),
							ResultsNumber = rdr.GetInt32("ResultsNumber"),
							SearchType = rdr.GetInt16("SearchType")
						};
					}
				}
				connection.Close();
			}
			return (res);
		}

		public bool UpdateMark(string searchString, int newMark, bool uIsWithBody, bool uIsExact, bool uIsAnnotated, bool uIsSolved)
		{
			bool ok = true;
			int type = Convert.ToInt32(uIsWithBody) | (Convert.ToInt32(uIsExact) << 1) | (Convert.ToInt32(uIsAnnotated) << 2) |
					   (Convert.ToInt32(uIsSolved) << 3);
			string sql = "UPDATE searchhistory SET Mark = @Mark where SearchString = @SearchString AND SearchType = @SearchType";

			using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
			{
				connection.Open();

				MySqlCommand cmd = new MySqlCommand(sql, connection);

				cmd.Parameters.AddWithValue("@SearchString", searchString);
				cmd.Parameters.AddWithValue("@Mark", newMark);
				cmd.Parameters.AddWithValue("@SearchType", type);
				try
				{
					cmd.ExecuteNonQuery();
				}
				catch (MySqlException)
				{
					ok = false;
				}
				connection.Close();
			}
			return (ok);
		}

	}
}
