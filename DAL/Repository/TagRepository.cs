﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Domain;
using MySql.Data.MySqlClient;

namespace DAL.Repository
{
    /// <summary>
    /// Repository for Tags
    /// </summary>
    public class TagRepository : IRepository<Tag>
    {

        /// <summary>
        /// Select all field in Tag:
        /// Id, TagName, Count
        /// </summary>
        /// <param name="limit"> limit the number of information selected, default 25</param>
        /// <param name="offset"> "pagination like", default 0</param>
        /// <returns>return list of object Tag</returns>
        public IEnumerable<Tag> GetAll(int limit = 25, int offset = 0)
        {
            string sql = string.Format(
                "SELECT * FROM tags LIMIT {0} OFFSET {1}",
                limit, offset);
            foreach (var tag in ExecuteQuery(sql))
            {
                yield return tag;
            }

        }

        /// <summary>
        /// Get the Tag matching the id given as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Get a specific Tag</returns>
        public Tag GetById(ulong id)
        {
            var sql = string.Format(
                    "select * from tags where Id =  {0}", id);
            return ExecuteQuery(sql).FirstOrDefault();
        }

        /// <summary>
        /// Do to request and get the Annotation object matching
        /// </summary>
        /// <param name="sql">Request string</param>
        /// <returns>Return an object Annotation</returns>
        private static IEnumerable<Tag> ExecuteQuery(string sql)
        {
            using (
                MySqlConnection connection =
                   new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))


            {
                connection.Open();

                MySqlCommand cmd = new MySqlCommand(sql, connection);

                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.HasRows && rdr.Read())
                    {
                        yield return new Tag
                        {
							Id = rdr.GetUInt64("Id"),
                            TagName = rdr.GetString("TagName"),
							Count = rdr.GetInt32("Count")
                        };
                    }
                }
            }
        }
    }
}
