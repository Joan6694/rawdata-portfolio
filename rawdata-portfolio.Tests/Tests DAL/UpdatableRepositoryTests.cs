﻿using System.Collections.Generic;
using DAL.Domain;
using DAL.Repository;
using NUnit.Framework;

namespace rawdata_portfolio.Tests.Tests_DAL
{
    /// <summary>
    /// Tests for updatable repositories.
    /// </summary>
    public class UpdatableRepositoryTests
    {
        //CHECK THIS REFERENCE FOR CRUD REPOSITORY UNIT TESTING
        //http://www.bradoncode.com/blog/2012/12/how-to-unit-test-repository.html

        AnnotationRepository AnnoRepository = new AnnotationRepository();

        /// <summary>
        /// Testing the display of annotations from a particular post.
        /// Test class: AnnotationRepository.
        /// Function called: GetResult(7664).
        /// Returns: particular annotation.
        /// </summary>
        [Test]
        public void AnnotationRepository_GetById_ReturnsAnnotation()
        {
            Annotation result = AnnoRepository.GetById(7664);
            Annotation expected = new Annotation
            {
                PostId = 7664,
                Favourite = true,
                Text = "Integrate C++ code BSD Project"
            };

            Assert.AreEqual(expected.PostId, result.PostId);
            Assert.AreEqual(expected.Favourite, result.Favourite);
            Assert.AreEqual(expected.Text, result.Text);
        }

        /// <summary>
        /// Testing the display of annotations from a particular unexisting post. 
        /// Test class: AnnotationRepository. 
        /// Function called: GetResult(1). 
        /// Returns: null. 
        /// </summary>
        [Test]
        public void AnnotationRepository_GetByIdUnexisting_ReturnsEmpty()
        {
            Annotation result = AnnoRepository.GetById(1);

            Assert.IsNull(result);
        }

        /// <summary>
        /// Testing the display of all annotations. 
        /// Test class: AnnotationRepository. 
        /// Function called: GetAll(4, 0); 
        /// Returns: IEnumerable Annotation.
        /// </summary>
        [Test]
        public void AnnotationRepository_GetAll_returnIEnumerable()
        {
            IEnumerable<Annotation> result = AnnoRepository.GetAll(4, 0);
            List<Annotation> expected = new List<Annotation>();

            expected.Add(new Annotation { PostId = 7664, Favourite = true, Text = "Integrate C++ code BSD Project" });
            expected.Add(new Annotation { PostId = 45325, Favourite = false, Text = "Visual Studio control error" });
            expected.Add(new Annotation { PostId = 85553, Favourite = true, Text = "MSDN struct" });
            expected.Add(new Annotation { PostId = 105568, Favourite = false, Text = "Best RTOS at Windows" });

            int i = 0;
            foreach (Annotation annotation in result)
            {
                Assert.AreEqual(expected[i].PostId, annotation.PostId);
                Assert.AreEqual(expected[i].Favourite, annotation.Favourite);
                Assert.AreEqual(expected[i].Text, annotation.Text);
                i++;
            }
        }

        /// <summary>
        /// Testing the CRUD operation INSERT. 
        /// Test class: AnnotationRepository. 
        /// Function called: Insert(entity). 
        /// Returns: true. 
        /// </summary>
        [Test]
        public void AnnotationRepository_Insert_CreatesNewInstance()
        {
            Annotation entity = new Annotation
            {
                PostId = 209638,
                Favourite = true,
                Text = "We are trying to INSERT a new Annotation into the Database"
            };

            bool returnValue = AnnoRepository.Insert(entity);

            Assert.IsTrue(returnValue); // <- insert worked

            //We delete this annotation so that we restore the database to its original state
            returnValue = AnnoRepository.Delete(209638);
            Assert.IsTrue(returnValue); // <- delete worked
        }

        /// <summary>
        /// Testing the CRUD operation INSERT with an already existing annotation. 
        /// Test class: AnnotationRepository. 
        /// Function called: Insert(entity). 
        /// Returns: false. 
        /// </summary>
        [Test]
        public void AnnotationRepository_InsertExistingAnnotation_SqlCrashes()
        {
            Annotation entity = new Annotation
            {
                PostId = 7664,
                Favourite = false,
                Text = "We are trying to INSERT an ALREADY EXISTING Annotation in the Database"
            };

            bool returnValue = AnnoRepository.Insert(entity);

            Assert.IsFalse(returnValue); // <- insert didnt work
        }

        /// <summary>
        /// Testing the CRUD operation UPDATE. 
        /// Test class: AnnotationRepository. 
        /// Function called: Update(entity). 
        /// Returns: true. 
        /// </summary>
        [Test]
        public void AnnotationRepository_Update_ModifiesExistingInstance()
        {
            Annotation entity = new Annotation
            {
                PostId = 7664, //THIS FIELD DOESNT MATTER AT ALL
                Favourite = false,
                Text = "We are trying to UPDATE an Annotation in the Database"
            };

            bool returnValue = AnnoRepository.Update(entity, 7664);

            Assert.IsTrue(returnValue);

            //We update this annotation again so that we restore the database to its original state
            entity.Favourite = true;
            entity.Text = "Integrate C++ code BSD Project";
            returnValue = AnnoRepository.Update(entity, 7664);
            Assert.IsTrue(returnValue); // <- update worked
        }

        /// <summary>
        /// Testing the CRUD operation UPDATE with a non existing annotation. 
        /// Test class: AnnotationRepository. 
        /// Function called: Update(entity). 
        /// Returns: true. 
        /// </summary>
        [Test]
        public void AnnotationRepository_UpdateUnexistingAnnotation_InsertsNewAnnotation()
        {
            Annotation entity = new Annotation
            {
                PostId = 219574, //This field doesn't matter
                Favourite = false,
                Text = "We are trying to UPDATE a NON EXISTING Annotation in the Database"
            };

            bool returnValue = AnnoRepository.Update(entity, 219574);

            Assert.IsTrue(returnValue);

            //We delete this annotation so that we restore the database to its original state
            returnValue = AnnoRepository.Delete(219574);
            Assert.IsTrue(returnValue); // <- delete worked
        }

        /// <summary>
        /// Testing the CRUD operation DELETE. 
        /// Test class: AnnotationRepository. 
        /// Function called: Delete(entity). 
        /// Returns: true. 
        /// </summary>
        [Test]
        public void AnnotationRepository_Delete_InstanceDisappears()
        {
            bool returnValue = AnnoRepository.Delete(7664);

            Assert.IsTrue(returnValue);

            //We insert this annotation so that we restore the database to its original state
            Annotation entity = new Annotation
            {
                PostId = 7664,
                Favourite = true,
                Text = "Integrate C++ code BSD Project"
            };

            returnValue = AnnoRepository.Insert(entity);
            Assert.IsTrue(returnValue); // <- insert worked
        }

        /// <summary>
        /// Testing the CRUD operation DELETE with an unexisting annotation. 
        /// Test class: AnnotationRepository. 
        /// Function called: Delete(entity). 
        /// Returns: true. 
        /// </summary>
        [Test]
        public void AnnotationRepository_DeleteUnexisting_NothingHappens()
        {
            bool returnValue = AnnoRepository.Delete(7665);

            Assert.IsTrue(returnValue);
        }
    }
}
