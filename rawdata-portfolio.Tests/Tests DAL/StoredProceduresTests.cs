﻿using DAL.Domain;
using DAL.Repository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rawdata_portfolio.Tests.Tests_DAL
{
    /// <summary>
    /// Tests for StoredProcedures.
    /// </summary>
    public class StoredProceduresTests
    {

        /// <summary>
        /// Testing the display of comments when giving a post with two comments
        /// Test class: DisplayCommentEntryRepository
        /// Function called: GetResult(45325)
        /// Returns: Ienumerable DisplayCommentEntry with two elements
        /// </summary>
        [Test]
        public void DisplayCommentEntryRepo_GetResult_ReturnsIEnumerable()
        {
            DisplayCommentEntryRepository repository = new DisplayCommentEntryRepository();
            IEnumerable<DisplayCommentEntry> result = repository.GetResult(45325);
            List<DisplayCommentEntry> expected = new List<DisplayCommentEntry>();

            DateTime creationDate1 = new DateTime(2012, 11, 21, 15, 6, 5); //2012-11-21 15:06:05
            DateTime creationDate2 = new DateTime(2014, 7, 15, 3, 41, 13); //2014-07-15 03:41:13

            expected.Add(new DisplayCommentEntry
            {
                PostId = 45325,
                Text = "It's it ridiculous that in 2012 Visual Studio still doesn't have a context menu item to regenerate designer files from source files? It's akin to not being able to rebuild a corrupt"
                + " DLL. I don't understand it.. Especially with external source control and external editors you leave Visual Studio crippled. Waste of money, if you ask me. I'm actually trying to regenerate"
                + " resource file designers. I managed to get *edit* it regenerated, but it's through obscurity... Augh!!!1",
                Score = 26,
                CreationDate = creationDate1,
                Username = "Glenn Slaven"
            });
            expected.Add(new DisplayCommentEntry
            {
                PostId = 45325,
                Text = "@bamccaig, if I had a penny for every weird \"feature\" like this, I would be richer than Bill Gates. If you think this is bad, look at where Outlook Express stores its mail data. "
                + "It's buried deep in the user folder under a cryptic folder name that's  hidden by default.",
                Score = 0,
                CreationDate = creationDate2,
                Username = "Glenn Slaven"
            });

            int i = 0;
            foreach (DisplayCommentEntry dce in result)
            {
                Assert.AreEqual(dce.PostId, expected[i].PostId);
                Assert.AreEqual(dce.Text, expected[i].Text);
                Assert.AreEqual(dce.Score, expected[i].Score);
                Assert.AreEqual(dce.CreationDate, expected[i].CreationDate);
                Assert.AreEqual(dce.Username, expected[i].Username);
                i++;
            }
        }

        /// <summary>
        /// Testing the display of comments when giving an incorrect post. 
        /// Test class: DisplayCommentEntryRepository. 
        /// Function called: GetResult(1). 
        /// Returns: Ienumerable DisplayCommentEntry empty. 
        /// </summary>
        [Test]
        public void DisplayCommentEntryRepo_GetResultUnexisting_ReturnsEmpty()
        {
            DisplayCommentEntryRepository repository = new DisplayCommentEntryRepository();
            IEnumerable<DisplayCommentEntry> result = repository.GetResult(1);

            Assert.IsEmpty(result);
        }

        /// <summary>
        /// Testing the display of posts with answers when giving a post without answers. 
        /// Test class: DisplayPostEntryRepository. 
        /// Function called: GetResult(7664). 
        /// Returns: Ienumerable DisplayPostEntry with one element.
        /// </summary>
        [Test]
        public void DisplayPostEntryRepo_GetResultFromPostWithoutAnswer_ReturnsIEnumerable()
        {
            DisplayPostEntryRepository repository = new DisplayPostEntryRepository();
            IEnumerable<DisplayPostEntry> result = repository.GetResult(7664);
            List<DisplayPostEntry> expected = new List<DisplayPostEntry>();

            DateTime creationDate = new DateTime(2008, 8, 11, 10, 38, 51); //2008-08-11 10:38:51

            expected.Add(new DisplayPostEntry
            {
                PostId = 7664,
                Title = "Windows C++: How can I redirect stderr for calls to fprintf?",
                Body = "<p>I am wrapping existing C++ code from a BSD project in our own custom wrapper and I want to integrate it to our code with as few changes as possible. "
                + "This code uses <code>fprintf</code> to print to <strong>stderr</strong> in order to log / report errors.</p>&#xA;&#xA;<p>I want to redirect this to an alternative"
                + " place within the same process. On Unix I have done this with a socketpair and a thread: one end of the socket is where I send <strong>stderr</strong> "
                + "(via a call to <code>dup2</code>) and the other end is monitored in a thread, where I can then process the output.</p>&#xA;&#xA;<p>This does not work on windows"
                + " though because a socket is not the same as a file handle.</p>&#xA;&#xA;<p>All documents I have found on the web show how to redirect output from a child process,"
                + " which is not what I want.  How can I redirect <strong>stderr</strong> within the same process getting a callback of some sort when output is written?  "
                + "(and before you say so, I've tried <code>SetStdHandle</code> but cannot find any way to make this work)...</p>&#xA;",
                Score = 4,
                CreationDate = creationDate,
                AcceptedAnswerId = 7669,
                Username = "jkp",
                CommentCount = 0,
                Favourite = true,
                Annotation = "Integrate C++ code BSD Project"
            });

            Assert.AreEqual(result.FirstOrDefault().PostId, expected[0].PostId);
            Assert.AreEqual(result.FirstOrDefault().Title, expected[0].Title);
            Assert.AreEqual(result.FirstOrDefault().Body, expected[0].Body);
            Assert.AreEqual(result.FirstOrDefault().Score, expected[0].Score);
            Assert.AreEqual(result.FirstOrDefault().CreationDate, expected[0].CreationDate);
            Assert.AreEqual(result.FirstOrDefault().AcceptedAnswerId, expected[0].AcceptedAnswerId);
            Assert.AreEqual(result.FirstOrDefault().Username, expected[0].Username);
            Assert.AreEqual(result.FirstOrDefault().Favourite, expected[0].Favourite);
            Assert.AreEqual(result.FirstOrDefault().Annotation, expected[0].Annotation);
        }

        /// <summary>
        /// Testing the display of posts with answers when giving an answer post. 
        /// Test class: DisplayPostEntryRepository. 
        /// Function called: GetResult(142838). 
        /// Returns: Ienumerable DisplayPostEntry with one element.
        /// </summary>
        [Test]
        public void DisplayPostEntryRepo_GetResultFromAnswer_ReturnsIEnumerable()
        {
            DisplayPostEntryRepository repository = new DisplayPostEntryRepository();
            IEnumerable<DisplayPostEntry> result = repository.GetResult(142838);
            List<DisplayPostEntry> expected = new List<DisplayPostEntry>();

            DateTime creationDate = new DateTime(2008, 9, 27, 3, 1, 31); //2008-09-27T03:01:31

            expected.Add(new DisplayPostEntry
            {
                PostId = 142838,
                Title = null,
                Body = "<p>Well, first, you must download the Microsoft XNA 3.0 CTP. Read the documentation, which will explain "
                +"the capabilities. But, from memory:</p>&#xA;&#xA;<ul>&#xA;<li>No hardware accelerated 3d (obviously, you can"
                +" create a software 3d engine and then render the result to a 2d sprite, but... Don't expect much in terms of"
                +" performance ;))</li>&#xA;<li>No XACT, you must use a new sound API</li>&#xA;</ul>&#xA;",
                Score = 4,
                CreationDate = creationDate,
                AcceptedAnswerId = null,
                Username = "TraumaPony",
                CommentCount = 0,
                Favourite = false,
                Annotation = null
            });

            Assert.AreEqual(result.FirstOrDefault().PostId, expected[0].PostId);
            Assert.AreEqual(result.FirstOrDefault().Title, expected[0].Title);
            Assert.AreEqual(result.FirstOrDefault().Body, expected[0].Body);
            Assert.AreEqual(result.FirstOrDefault().Score, expected[0].Score);
            Assert.AreEqual(result.FirstOrDefault().CreationDate, expected[0].CreationDate);
            Assert.AreEqual(result.FirstOrDefault().AcceptedAnswerId, expected[0].AcceptedAnswerId);
            Assert.AreEqual(result.FirstOrDefault().Username, expected[0].Username);
            Assert.AreEqual(result.FirstOrDefault().Favourite, expected[0].Favourite);
            Assert.AreEqual(result.FirstOrDefault().Annotation, expected[0].Annotation);
        }

        /// <summary>
        /// Testing the display of posts with answers when giving a post with answers. 
        /// Test class: DisplayPostEntryRepository. 
        /// Function called: GetResult(142816). 
        /// Returns: Ienumerable DisplayPostEntry with 5 elements (one question, four answers). 
        /// </summary>
        [Test]
        public void DisplayPostEntryRepo_GetResultFromPostWithAnswers_ReturnsIEnumerable()
        {
            DisplayPostEntryRepository repository = new DisplayPostEntryRepository();
            IEnumerable<DisplayPostEntry> result = repository.GetResult(142816);
            List<DisplayPostEntry> expected = new List<DisplayPostEntry>();

            DateTime creationDate1 = new DateTime(2008, 9, 27, 2, 51, 22); //2008-09-27T02:51:22
            DateTime creationDate2 = new DateTime(2008, 9, 27, 3, 1, 31); //2008-09-27T03:01:31
            DateTime creationDate3 = new DateTime(2008, 9, 27, 3, 1, 45); //2008-09-27T03:01:45
            DateTime creationDate4 = new DateTime(2008, 11, 18, 17, 46, 10); //2008-11-18T17:46:10
            DateTime creationDate5 = new DateTime(2008, 12, 17, 12, 18, 43); //2008-12-17T12:18:43

            expected.Add(new DisplayPostEntry
            {
                PostId = 142816,
                Title = "How to get started with game programming on the Zune",
                Body = "<p>My zune just updated to 3.0 (didn't even realize they were releasing something new!) and the update came"
                +" with two games, but the Zune marketplace does not have games.  </p>&#xA;&#xA;<p>Where do I go to get started, and"
                +" what are the capabilities of the Zune in terms of games/apps?</p>&#xA;",
                Score = 6,
                CreationDate = creationDate1,
                AcceptedAnswerId = 142838,
                Username = "Adam Davis",
                CommentCount = 0,
                Favourite = false,
                Annotation = null
            });

            expected.Add(new DisplayPostEntry
            {
                PostId = 142838,
                Title = null,
                Body = "<p>Well, first, you must download the Microsoft XNA 3.0 CTP. Read the documentation, which will explain the"
                +" capabilities. But, from memory:</p>&#xA;&#xA;<ul>&#xA;<li>No hardware accelerated 3d (obviously, you can create"
                +" a software 3d engine and then render the result to a 2d sprite, but... Don't expect much in terms of performance"
                +" ;))</li>&#xA;<li>No XACT, you must use a new sound API</li>&#xA;</ul>&#xA;",
                Score = 4,
                CreationDate = creationDate2,
                AcceptedAnswerId = null,
                Username = "TraumaPony",
                CommentCount = 0,
                Favourite = false,
                Annotation = null
            });

            expected.Add(new DisplayPostEntry
            {
                PostId = 142841,
                Title = null,
                Body = "<p>I was hoping someone here would have better resources, but as this seems to be a new area of development,"
                +" <a href=\"http://blog.paranoidferret.com/index.php/2008/09/19/zune-game-development-getting-started/\" "
                +"rel=\"nofollow\">here's one resource</a> that appears to give all the steps for a newbie to get started "
                +"(too many assume you already have Visual studio, etc).</p>&#xA;&#xA;<p>I'm really interested in a better "
                +"in-depth overview of the capabilities as well, though.</p>&#xA;&#xA;&#xA;",
                Score = 1,
                CreationDate = creationDate3,
                AcceptedAnswerId = null,
                Username = "Adam Davis",
                CommentCount = 0,
                Favourite = true,
                Annotation = null
            });

            expected.Add(new DisplayPostEntry
            {
                PostId = 299542,
                Title = null,
                Body = "<p>Just an update but note that XNA 3.0 has been <a href=\"http://www.microsoft.com/downloads/details.aspx"
                +"?familyid=7d70d6ed-1edd-4852-9883-9a33c0ad8fee&amp;displaylang=en\" rel=\"nofollow\">released</a>.   It requires"+
                " some flavor of Visual Studio 2008.</p>&#xA;&#xA;<p>I downloaded it and coded &amp; deployed \"hello world\" to my"
                +" Zune in no time at all.  Very easy.</p>&#xA;",
                Score = 2,
                CreationDate = creationDate4,
                AcceptedAnswerId = null,
                Username = "Mark",
                CommentCount = 0,
                Favourite = false,
                Annotation = null
            });

            expected.Add(new DisplayPostEntry
            {
                PostId = 374374,
                Title = null,
                Body = "<p>You should check out the blog of <a href=\"http://www.robmiles.com/xna-book/\" rel=\"nofollow\">Rob Miles"
                +"</a>. He has a few chapters of his book on his site. Great place to start.</p>&#xA;",
                Score = 2,
                CreationDate = creationDate5,
                AcceptedAnswerId = null,
                Username = "Sorskoot",
                CommentCount = 0,
                Favourite = false,
                Annotation = null
            });

            Assert.AreEqual(result.FirstOrDefault().PostId, expected[0].PostId);
            Assert.AreEqual(result.FirstOrDefault().Title, expected[0].Title);
            Assert.AreEqual(result.FirstOrDefault().Body, expected[0].Body);
            Assert.AreEqual(result.FirstOrDefault().Score, expected[0].Score);
            Assert.AreEqual(result.FirstOrDefault().CreationDate, expected[0].CreationDate);
            Assert.AreEqual(result.FirstOrDefault().AcceptedAnswerId, expected[0].AcceptedAnswerId);
            Assert.AreEqual(result.FirstOrDefault().Username, expected[0].Username);
            Assert.AreEqual(result.FirstOrDefault().Favourite, expected[0].Favourite);
            Assert.AreEqual(result.FirstOrDefault().Annotation, expected[0].Annotation);
        }

        /// <summary>
        /// Testing the display of posts with answers when giving an unexisting post. 
        /// Test class: DisplayPostEntryRepository. 
        /// Function called: GetResult(7665). 
        /// Returns: Ienumerable DisplayPostEntry  empty.
        /// </summary>
        [Test]
        public void DisplayPostEntryRepo_GetResultUnexisting_ReturnsEmpty()
        {
            DisplayPostEntryRepository repository = new DisplayPostEntryRepository();
            IEnumerable<DisplayPostEntry> result = repository.GetResult(7665);

            Assert.IsEmpty(result);
        }

        /// <summary>
        /// testing the display of posts when searching favourited ones with default settings. 
        /// test class: FavouriteSearchRepository. 
        /// function called: GetResult("java", false, false, false, false, 25, 0). 
        /// parameters: "java" string, without body, not exact match, not necessarily annotated and not solved. 
        /// returns: iEnumerable FavouriteSearchEntry with two elements. 
        /// </summary>
        [Test]
        public void FavouriteSearchRepo_GetResultDefault_ReturnsIEnumerable()
        {
            FavouriteSearchRepository repository = new FavouriteSearchRepository();
            IEnumerable<FavouriteSearchEntry> result = repository.GetResult("java", false, false, false, false, 25, 0);
            List<FavouriteSearchEntry> expected = new List<FavouriteSearchEntry>();

            DateTime creationDate1 = new DateTime(2009, 8, 21, 6, 57, 17); //2009-08-21T06:57:17
            DateTime creationDate2 = new DateTime(2011, 5, 4, 4, 27, 19); //2011-05-04T04:27:19

            expected.Add(new FavouriteSearchEntry
            {
                Body = "<p>As part of a web app, once images have been downloaded and rendered on a web page, I need to dete [...]",
                Title = "Determining image file size + dimensions via Javascript?",
                AnswerCount = 6,
                ValidatedAnswer = "Yes",
                Author = "scottru",
                CreationDate = creationDate1,
                Annotation = "Yes",
                Id = 1310378
            });
            expected.Add(new FavouriteSearchEntry
            {
                Body = "<p>I have some strings that look like this:</p>&#xA;&#xA;<pre>&#xA;1.     Some stuff&#xA;2.     some [...]",
                Title = "Javascript remove leading number label from string",
                AnswerCount = 5,
                ValidatedAnswer = "Yes",
                Author = "Chris Dutrow",
                CreationDate = creationDate2,
                Annotation = "Yes",
                Id = 5878536
            });

            int i = 0;
            foreach (FavouriteSearchEntry fse in result)
            {
                Assert.AreEqual(fse.Body, expected[i].Body);
                Assert.AreEqual(fse.Title, expected[i].Title);
                Assert.AreEqual(fse.AnswerCount, expected[i].AnswerCount);
                Assert.AreEqual(fse.Author, expected[i].Author);
                Assert.AreEqual(fse.CreationDate, expected[i].CreationDate);
                Assert.AreEqual(fse.Annotation, expected[i].Annotation);
                Assert.AreEqual(fse.Id, expected[i].Id);
                i++;
            }
        }

        /// <summary>
        /// testing the display of posts when searching favourited ones with settings enabled.
        /// test class: FavouriteSearchRepository.
        /// function called: GetResult("", true, true, true, true, 2, 2);   
        /// parameters: empty string, searching in body, exact match, necessarily annotated and solved, with limit 2 and offset 2. 
        /// returns: iEnumerable FavouriteSearchEntry with two elements.
        /// </summary>
        [Test]
        public void FavouriteSearchRepo_GetResultFlagsTrue_ReturnsIEnumerable()
        {
            FavouriteSearchRepository repository = new FavouriteSearchRepository();
            IEnumerable<FavouriteSearchEntry> result = repository.GetResult("", true, true, true, true, 2, 2);
            List<FavouriteSearchEntry> expected = new List<FavouriteSearchEntry>();

            DateTime creationDate1 = new DateTime(2009, 8, 21, 6, 57, 17); //2009-08-21 06:57:17
            DateTime creationDate2 = new DateTime(2011, 2, 25, 12, 23, 37); //2011-02-25 12:23:37

            expected.Add(new FavouriteSearchEntry
            {
                Body = "<p>As part of a web app, once images have been downloaded and rendered on a web page, I need to dete [...]",
                Title = "Determining image file size + dimensions via Javascript?",
                AnswerCount = 6,
                ValidatedAnswer = "Yes",
                Author = "scottru",
                CreationDate = creationDate1,
                Annotation = "Yes",
                Id = 1310378
            });
            expected.Add(new FavouriteSearchEntry
            {
                Body = "<p>I'm writing a console app in c++ using ncurses and I'd like to output a solid ascii block. It'd b [...]",
                Title = "Output a nice \"block\" character with ncurses and C++?",
                AnswerCount = 1,
                ValidatedAnswer = "Yes",
                Author = "Darkenor",
                CreationDate = creationDate2,
                Annotation = "Yes",
                Id = 5117171
            });

            int i = 0;
            foreach (FavouriteSearchEntry fse in result)
            {
                Assert.AreEqual(fse.Body, expected[i].Body);
                Assert.AreEqual(fse.Title, expected[i].Title);
                Assert.AreEqual(fse.AnswerCount, expected[i].AnswerCount);
                Assert.AreEqual(fse.Author, expected[i].Author);
                Assert.AreEqual(fse.CreationDate, expected[i].CreationDate);
                Assert.AreEqual(fse.Annotation, expected[i].Annotation);
                Assert.AreEqual(fse.Id, expected[i].Id);
                i++;
            }
        }

        /// <summary>
        /// testing the search for posts with default settings. 
        /// test class: PostSearchRepository. 
        /// function called: GetResult("sql", false, false, false, false, 3, 0);  
        /// parameters: "sql" string, not in body, not exact match, not necessarily annotated and solved.
        /// returns: iEnumerable PostSearchEntry with three elements.
        /// </summary>
        [Test]
        public void PostSearchRepo_GetResultDefault_ReturnsIEnumerable()
        {
            PostSearchRepository repository = new PostSearchRepository();
            IEnumerable<PostSearchEntry> result = repository.GetResult("sql", false, false, false, false, 3, 0);
            List<PostSearchEntry> expected = new List<PostSearchEntry>();

            DateTime creationDate1 = new DateTime(2008, 11, 6, 2, 34, 0); //2008-11-06 02:34:00
            DateTime creationDate2 = new DateTime(2009, 10, 16, 19, 5, 12); //2009-10-16 19:05:12
            DateTime creationDate3 = new DateTime(2009, 10, 20, 22, 58, 43); //2009-10-20 22:58:43

            expected.Add(new PostSearchEntry
            {
                Body = "<p>I'm having some trouble figuring out how to use more than one left outer join using LINQ to SQL.  [...]",
                Title = "Linq to Sql: Multiple left outer joins",
                AnswerCount = 5,
                ValidatedAnswer = "Yes",
                Author = "Bryan Roth",
                CreationDate = creationDate1,
                Annotation = "No",
                Id = 267488,
                Favourite = false
            });
            expected.Add(new PostSearchEntry
            {
                Body = "<p>Is it possible to accomplish something like this using linqtosql?</p>&#xA;&#xA;<pre><code>select  [...]",
                Title = "Multiple Left Outer Joins in LinqToSql?",
                AnswerCount = 4,
                ValidatedAnswer = "Yes",
                Author = "Scott",
                CreationDate = creationDate2,
                Annotation = "No",
                Id = 1579829,
                Favourite = false
            });
            expected.Add(new PostSearchEntry
            {
                Body = "<p>I'm using Delphi 7 Personal. To access MySQL database I'm using libmysql.dll + very simple wrappe [...]",
                Title = "Delphi 7 Personal, MySQL using libmysql.dll + UTF8",
                AnswerCount = 2,
                ValidatedAnswer = "Yes",
                Author = "migajek",
                CreationDate = creationDate3,
                Annotation = "No",
                Id = 1597768,
                Favourite = false
            });

            int i = 0;
            foreach (PostSearchEntry pse in result)
            {
                Assert.AreEqual(pse.Body, expected[i].Body);
                Assert.AreEqual(pse.Title, expected[i].Title);
                Assert.AreEqual(pse.AnswerCount, expected[i].AnswerCount);
                Assert.AreEqual(pse.ValidatedAnswer, expected[i].ValidatedAnswer);
                Assert.AreEqual(pse.Author, expected[i].Author);
                Assert.AreEqual(pse.CreationDate, expected[i].CreationDate);
                Assert.AreEqual(pse.Annotation, expected[i].Annotation);
                Assert.AreEqual(pse.Id, expected[i].Id);
                i++;
            }
        }

        /// <summary>
        /// testing the search for posts with settings enabled. 
        /// test class: PostSearchRepository. 
        /// function called: GetResult("java", true, true, true, true, 2, 2);   
        /// parameters: "java" string, in body, exact match, necessarily annotated and solved, with limit 2 and offset 2. 
        /// returns: iEnumerable PostSearchEntry with two elements. 
        /// </summary>
        [Test]
        public void PostSearchRepo_GetResultFlagsTrue_ReturnsIEnumerable()
        {
            PostSearchRepository repository = new PostSearchRepository();
            IEnumerable<PostSearchEntry> result = repository.GetResult("java", true, true, true, true, 2, 2);
            List<PostSearchEntry> expected = new List<PostSearchEntry>();

            DateTime creationDate1 = new DateTime(2009, 8, 21, 6, 57, 17); //2009-08-21 06:57:17
            DateTime creationDate2 = new DateTime(2011, 8, 24, 15, 30, 15); //2011-08-24 15:30:15

            expected.Add(new PostSearchEntry
            {
                Body = "<p>As part of a web app, once images have been downloaded and rendered on a web page, I need to dete [...]",
                Title = "Determining image file size + dimensions via Javascript?",
                AnswerCount = 6,
                ValidatedAnswer = "Yes",
                Author = "scottru",
                CreationDate = creationDate1,
                Annotation = "Yes",
                Id = 1310378,
                Favourite = true
            });
            expected.Add(new PostSearchEntry
            {
                Body = "<p>I have inherited some Java RMI client/server code, and while it runs fine on one machine, I haven [...]",
                Title = "Java RMI ServerException - java.lang.ClassNotFoundException: org.prog.rmi.RmiServer_Stub",
                AnswerCount = 2,
                ValidatedAnswer = "Yes",
                Author = "Loftx",
                CreationDate = creationDate2,
                Annotation = "Yes",
                Id = 7178137,
                Favourite = false
            });

            int i = 0;
            foreach (PostSearchEntry pse in result)
            {
                Assert.AreEqual(pse.Body, expected[i].Body);
                Assert.AreEqual(pse.Title, expected[i].Title);
                Assert.AreEqual(pse.AnswerCount, expected[i].AnswerCount);
                Assert.AreEqual(pse.ValidatedAnswer, expected[i].ValidatedAnswer);
                Assert.AreEqual(pse.Author, expected[i].Author);
                Assert.AreEqual(pse.CreationDate, expected[i].CreationDate);
                Assert.AreEqual(pse.Annotation, expected[i].Annotation);
                Assert.AreEqual(pse.Id, expected[i].Id);
                i++;
            }
        }

        /// <summary>
        /// testing the search for posts with default settings and a space in the search input. 
        /// test class: PostSearchRepository. 
        /// function called: GetResult("sql java", false, false, false, false, 2, 0); 
        /// parameters: "sql java" string, in body, not exact match, not necessarily annotated and solved.
        /// returns: iEnumerable PostSearchEntry with three elements.
        /// </summary>
        [Test]
        public void PostSearchRepo_GetResultSpaceSearch_ReturnsIEnumerable()
        {
            PostSearchRepository repository = new PostSearchRepository();
            IEnumerable<PostSearchEntry> result = repository.GetResult("sql java", true, false, false, false, 2, 0);
            List<PostSearchEntry> expected = new List<PostSearchEntry>();

            DateTime creationDate1 = new DateTime(2008, 10, 08, 0, 22, 10); //2008-10-08T00:22:10
            DateTime creationDate2 = new DateTime(2008, 10, 20, 19, 30, 13); //2008-10-20T19:30:13

            expected.Add(new PostSearchEntry
            {
                Body = "<p>It's my understanding that common wisdom says to only use exceptions for truly exceptional condit [...]",
                Title = "Are exceptions really for exceptional errors?",
                AnswerCount = 13,
                ValidatedAnswer = "Yes",
                Author = "Esteban Araya",
                CreationDate = creationDate1,
                Annotation = "No",
                Id = 180937,
                Favourite = false
            });
            expected.Add(new PostSearchEntry
            {
                Body = "<p>One thing I've run into a few times is a service class (like a JBoss service) that has gotten ove [...]",
                Title = "Large Inner classes and private variables",
                AnswerCount = 5,
                ValidatedAnswer = "Yes",
                Author = "Chris Kessel",
                CreationDate = creationDate2,
                Annotation = "No",
                Id = 219574,
                Favourite = false
            });

            int i = 0;
            foreach (PostSearchEntry pse in result)
            {
                Assert.AreEqual(pse.Body, expected[i].Body);
                Assert.AreEqual(pse.Title, expected[i].Title);
                Assert.AreEqual(pse.AnswerCount, expected[i].AnswerCount);
                Assert.AreEqual(pse.ValidatedAnswer, expected[i].ValidatedAnswer);
                Assert.AreEqual(pse.Author, expected[i].Author);
                Assert.AreEqual(pse.CreationDate, expected[i].CreationDate);
                Assert.AreEqual(pse.Annotation, expected[i].Annotation);
                Assert.AreEqual(pse.Id, expected[i].Id);
                i++;
            }
        }

        /// <summary>
        /// testing the search for searchEntries in the searchHistory by Id.  
        /// test class: SearchHistoryEntryRepository. 
        /// function called: GetById(1);  
        /// returns: SearchHistoryEntry. 
        /// </summary>
        [Test]
        public void SearchHistoryEntryRepo_GetById_ReturnsIEnumerable()
        {
            SearchHistoryEntryRepository repository = new SearchHistoryEntryRepository();
            SearchHistoryEntry result = repository.GetBySearchStringAndType("Java error outofbounds", true, false, false, false);

            DateTime date = new DateTime(2013, 8, 30, 19, 5, 0); //"2013-08-30T19:05:00"

            SearchHistoryEntry expected = new SearchHistoryEntry
            {
                Id = 1,
                SearchString = "Java error outofbounds",
                Date = date,
                Mark = 0,
                ResultsNumber = 50,
                SearchType = 1
            };

            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.SearchString, result.SearchString);
            Assert.AreEqual(expected.Date, result.Date);
            Assert.AreEqual(expected.Mark, result.Mark);
            Assert.AreEqual(expected.ResultsNumber, result.ResultsNumber);
        }

        /// <summary>
        /// testing the search for searchEntries in the searchHistory by unexisting Id.  
        /// test class: SearchHistoryEntryRepository. 
        /// function called: GetById(0);   
        /// returns: null. 
        /// </summary>
        [Test]
        public void SearchHistoryEntryRepo_GetByIdUnexisting_ReturnsNull()
        {
            SearchHistoryEntryRepository repository = new SearchHistoryEntryRepository();
            SearchHistoryEntry result = repository.GetBySearchStringAndType("", false, false, false, false);

            Assert.IsNull(result);
        }

        /// <summary>
        /// testing the search for searchEntries in the searchHistory with default settings. 
        /// test class: SearchHistoryEntryRepository. 
        /// function called: GetResult("java ", -1, 3, 0);   
        /// parameters: "java" string, not necessarily marked, limit 3 and offset 0. 
        /// returns: iEnumerable SearchHistoryEntry with three elements. 
        /// </summary>
        [Test]
        public void SearchHistoryEntryRepo_GetResultDefault_ReturnsIEnumerable()
        {
            SearchHistoryEntryRepository repository = new SearchHistoryEntryRepository();
            IEnumerable<SearchHistoryEntry> result = repository.GetResult("java ", -1, 3, 0);
            List<SearchHistoryEntry> expected = new List<SearchHistoryEntry>();

            DateTime creationDate1 = new DateTime(2013, 8, 30, 19, 5, 0); //2013-08-30 19:05:00
            DateTime creationDate2 = new DateTime(2013, 8, 30, 19, 6, 4); //2013-08-30 19:06:04
            DateTime creationDate3 = new DateTime(2013, 9, 2, 19, 7, 5); //2013-09-02 19:07:05

            expected.Add(new SearchHistoryEntry
            {
                Id = 1,
                SearchString = "Java error outofbounds",
                Date = creationDate1,
                Mark = 0,
                ResultsNumber = 50,
                SearchType = 1
            });

            expected.Add(new SearchHistoryEntry
            {
                Id = 6,
                SearchString = "I still do not understand Java exceptions",
                Date = creationDate2,
                Mark = -1,
                ResultsNumber = 20,
                SearchType = 2
            });

            expected.Add(new SearchHistoryEntry
            {
                Id = 7,
                SearchString = "Java error stack overflow",
                Date = creationDate3,
                Mark = 0,
                ResultsNumber = 25,
                SearchType = 1
            });

            int i = 0;
            foreach (SearchHistoryEntry she in result)
            {
                Assert.AreEqual(expected[i].Id, she.Id);
                Assert.AreEqual(expected[i].SearchString, she.SearchString);
                Assert.AreEqual(expected[i].Date, she.Date);
                Assert.AreEqual(expected[i].Mark, she.Mark);
                Assert.AreEqual(expected[i].ResultsNumber, she.ResultsNumber);
                i++;
            }
        }


        /// <summary>
        /// testing the search for searchEntries in the searchHistory with marked enabled. 
        /// test class: SearchHistoryEntryRepository. 
        /// function called: GetResult("", 1, 2, 1);   
        /// parameters: empty string, necessarily marked, limit 2 and offset 1. 
        /// returns: iEnumerable SearchHistoryEntry with two elements. 
        /// </summary>
        [Test]
        public void SearchHistoryEntryRepo_GetResultMarked_ReturnsIEnumerable()
        {
            SearchHistoryEntryRepository repository = new SearchHistoryEntryRepository();
            IEnumerable<SearchHistoryEntry> result = repository.GetResult("", 1, 2, 1);
            List<SearchHistoryEntry> expected = new List<SearchHistoryEntry>();

            DateTime creationDate1 = new DateTime(2020, 8, 30, 19, 5, 9); //2020-08-30 19:05:09
            DateTime creationDate2 = new DateTime(2000, 9, 2, 19, 4, 59); //2000-09-02 19:04:59

            expected.Add(new SearchHistoryEntry
            {
                Id = 5,
                SearchString = "What is the difference between PC and Mac",
                Date = creationDate1,
                Mark = 1,
                ResultsNumber = 500,
                SearchType = 1
            });

            expected.Add(new SearchHistoryEntry
            {
                Id = 8,
                SearchString = "searchedString8",
                Date = creationDate2,
                Mark = 1,
                ResultsNumber = 2,
                SearchType = 2
            });

            int i = 0;
            foreach (SearchHistoryEntry she in result)
            {
                Assert.AreEqual(expected[i].Id, she.Id);
                Assert.AreEqual(expected[i].SearchString, she.SearchString);
                Assert.AreEqual(expected[i].Date, she.Date);
                Assert.AreEqual(expected[i].Mark, she.Mark);
                Assert.AreEqual(expected[i].ResultsNumber, she.ResultsNumber);
                i++;
            }
        }
    }
}
