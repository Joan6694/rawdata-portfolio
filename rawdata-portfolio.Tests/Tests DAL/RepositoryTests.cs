﻿using System;
using System.Collections.Generic;
using DAL.Domain;
using DAL.Repository;
using NUnit.Framework;

namespace rawdata_portfolio.Tests.Tests_DAL
{
    /// <summary>
    /// Tests for the repositories.
    /// </summary>
    public class RepositoryTests
    {
        /// <summary>
        /// Testing the display of a particular user. 
        /// Test class: UserRepository. 
        /// Function called: GetById(1). 
        /// Returns: User. 
        /// </summary>
        [Test]
        public void UserRepository_GetById_ReturnsUser()
        {
            UserRepository repository = new UserRepository();
            User result = repository.GetById(1);
            User expected = new User
            {
                Id = 1,
                Name = "Jeff Atwood",
                DownVotes = 1299,
                UpVotes = 3225
            };

            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Name, result.Name);
            Assert.AreEqual(expected.DownVotes, result.DownVotes);
            Assert.AreEqual(expected.UpVotes, result.UpVotes);
        }

        /// <summary>
        /// Testing the display of a particular unexisting user. 
        /// Test class: UserRepository. 
        /// Function called: GetById(2). 
        /// Returns: null. s
        /// </summary>
        [Test]
        public void UserRepository_GetByIdUnexisting_ReturnsNull()
        {
            UserRepository repository = new UserRepository();
            User result = repository.GetById(2);

            Assert.IsNull(result);
        }

        /// <summary>
        /// Testing the display of all the users (with a limit of 4). 
        /// Test class: UserRepository. 
        /// Function called: GetAll(4, 0). 
        /// Returns: IEnumerable User. 
        /// </summary>
        [Test]
        public void UserRepository_GetAll_returnIEnumerable()
        {
            UserRepository repository = new UserRepository();
            IEnumerable<User> result = repository.GetAll(4, 0);
            List<User> expected = new List<User>();

            expected.Add(new User { Id = 1, Name = "Jeff Atwood", DownVotes = 1299, UpVotes = 3225 });
            expected.Add(new User { Id = 4, Name = "Joel Spolsky", DownVotes = 94, UpVotes = 760 });
            expected.Add(new User { Id = 36, Name = "Pat", DownVotes = 10, UpVotes = 406 });
            expected.Add(new User { Id = 51, Name = "Yaakov Ellis", DownVotes = 44, UpVotes = 2428 });

            int i = 0;
            foreach (User user in result)
            {
                Assert.AreEqual(user.Id, expected[i].Id);
                Assert.AreEqual(user.Name, expected[i].Name);
                Assert.AreEqual(user.DownVotes, expected[i].DownVotes);
                Assert.AreEqual(user.UpVotes, expected[i].UpVotes);
                i++;
            }
        }

        /// <summary>
        /// Testing the display of a particular tag. 
        /// Test class: TagRepository. 
        /// Function called: GetById(1). 
        /// Returns: Tag. 
        /// </summary>
        [Test]
        public void TagRepository_GetById_ReturnsTag()
        {
            TagRepository repository = new TagRepository();
            Tag result = repository.GetById(1);
            Tag expected = new Tag
            {
                Id = 1,
                TagName = ".net",
                Count = 99
            };

            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.TagName, result.TagName);
            Assert.AreEqual(expected.Count, result.Count);
        }

        /// <summary>
        /// Testing the display of a particular unexisting tag. 
        /// Test class: tagRepository. 
        /// Function called: GetById(6). 
        /// Returns: null. 
        /// </summary>
        [Test]
        public void TagRepository_GetByIdUnexisting_ReturnsNull()
        {
            TagRepository repository = new TagRepository();
            Tag result = repository.GetById(6);

            Assert.IsNull(result);
        }

        /// <summary>
        /// Testing the display of all the tags (with a limit of 4). 
        /// Test class: TagRepository. 
        /// Function called: GetAll(4, 0). 
        /// Returns: IEnumerable Tag. 
        /// </summary>
        [Test]
        public void TagRepository_GetAll_returnIEnumerable()
        {
            TagRepository repository = new TagRepository();
            IEnumerable<Tag> result = repository.GetAll(4, 0);
            List<Tag> expected = new List<Tag>();

            expected.Add(new Tag { Id = 1, TagName = ".net", Count = 99 });
            expected.Add(new Tag { Id = 2, TagName = "html", Count = 68 });
            expected.Add(new Tag { Id = 3, TagName = "javascript", Count = 117 });
            expected.Add(new Tag { Id = 4, TagName = "css", Count = 48 });

            int i = 0;
            foreach (Tag tag in result)
            {
                Assert.AreEqual(tag.Id, expected[i].Id);
                Assert.AreEqual(tag.TagName, expected[i].TagName);
                Assert.AreEqual(tag.Count, expected[i].Count);
                i++;
            }
        }

        /// <summary>
        /// Testing the display of a particular post. 
        /// Test class: PostRepository. 
        /// Function called: GetById(7664). 
        /// Returns: Post. 
        /// </summary>
        [Test]
        public void PostRepository_GetById_ReturnsPost()
        {
            PostRepository repository = new PostRepository();
            Post result = repository.GetById(7664);
            DateTime creationDate = new DateTime(2008, 8, 11, 10, 38, 51); //2008-08-11T10:38:51
            Post expected = new Post
            {
                Id = 7664,
                PostTypeId = 1,
                ParentId = null,
                AcceptedAnswerId = 7669,
                CreationDate = creationDate,
                Score = 4,
                ViewCount = 8551,
                Body = "<p>I am wrapping existing C++ code from a BSD project in our own custom wrapper and I want to integrate it to our code with as few changes as possible."
                + " This code uses <code>fprintf</code> to print to <strong>stderr</strong> in order to log / report errors.</p>&#xA;&#xA;<p>I want to redirect this to an "
                + "alternative place within the same process. On Unix I have done this with a socketpair and a thread: one end of the socket is where I send "
                + "<strong>stderr</strong> (via a call to <code>dup2</code>) and the other end is monitored in a thread, where I can then process the output."
                + "</p>&#xA;&#xA;<p>This does not work on windows though because a socket is not the same as a file handle.</p>&#xA;&#xA;<p>All documents I have found on"
                + " the web show how to redirect output from a child process, which is not what I want.  How can I redirect <strong>stderr</strong> within the same process"
                + " getting a callback of some sort when output is written?  (and before you say so, I've tried <code>SetStdHandle</code> but cannot find any way to make "
                + "this work)...</p>&#xA;",
                OwnerUserId = 912,
                Title = "Windows C++: How can I redirect stderr for calls to fprintf?",
                Tags = "<c++><windows><redirect>",
                AnswerCount = 3,
                CommentCount = 0
            };


            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.PostTypeId, result.PostTypeId);
            Assert.AreEqual(expected.ParentId, result.ParentId);
            Assert.AreEqual(expected.AcceptedAnswerId, result.AcceptedAnswerId);
            Assert.AreEqual(expected.CreationDate, result.CreationDate);
            Assert.AreEqual(expected.Score, result.Score);
            Assert.AreEqual(expected.ViewCount, result.ViewCount);
            Assert.AreEqual(expected.Body, result.Body);
            Assert.AreEqual(expected.OwnerUserId, result.OwnerUserId);
            Assert.AreEqual(expected.Title, result.Title);
            Assert.AreEqual(expected.Tags, result.Tags);
            Assert.AreEqual(expected.AnswerCount, result.AnswerCount);
            Assert.AreEqual(expected.CommentCount, result.CommentCount);
        }

        /// <summary>
        /// Testing the display of a particular unexisting post. 
        /// Test class: PostRepository. 
        /// Function called: GetById(1). 
        /// Returns: null. 
        /// </summary>
        [Test]
        public void PostRepository_GetByIdUnexisting_ReturnsNull()
        {
            PostRepository repository = new PostRepository();
            Post result = repository.GetById(1);

            Assert.IsNull(result);
        }

        /// <summary>
        /// Testing the display of all the posts (with a limit of 3). 
        /// Test class: postRepository. 
        /// Function called: GetAll(3, 0). 
        /// Returns: IEnumerable Post. 
        /// </summary>
        [Test]
        public void PostRepository_GetAll_returnIEnumerable()
        {
            PostRepository repository = new PostRepository();
            IEnumerable<Post> result = repository.GetAll(3, 0);
            List<Post> expected = new List<Post>();

            DateTime creationDate1 = new DateTime(2008, 8, 11, 10, 38, 51); //2008-08-11T10:38:51
            DateTime creationDate2 = new DateTime(2008, 9, 5, 6, 21, 6); //2008-09-05T06:21:06
            DateTime creationDate3 = new DateTime(2008, 9, 17, 17, 20, 49); //2008-09-17T17:20:49

            expected.Add(new Post
            {
                Id = 7664,
                PostTypeId = 1,
                ParentId = null,
                AcceptedAnswerId = 7669,
                CreationDate = creationDate1,
                Score = 4,
                ViewCount = 8551,
                Body = "<p>I am wrapping existing C++ code from a BSD project in our own custom wrapper and I want to integrate it to our code with as few changes as possible."
                + " This code uses <code>fprintf</code> to print to <strong>stderr</strong> in order to log / report errors.</p>&#xA;&#xA;<p>I want to redirect this to an "
                + "alternative place within the same process. On Unix I have done this with a socketpair and a thread: one end of the socket is where I send "
                + "<strong>stderr</strong> (via a call to <code>dup2</code>) and the other end is monitored in a thread, where I can then process the output."
                + "</p>&#xA;&#xA;<p>This does not work on windows though because a socket is not the same as a file handle.</p>&#xA;&#xA;<p>All documents I have found on"
                + " the web show how to redirect output from a child process, which is not what I want.  How can I redirect <strong>stderr</strong> within the same process"
                + " getting a callback of some sort when output is written?  (and before you say so, I've tried <code>SetStdHandle</code> but cannot find any way to make "
                + "this work)...</p>&#xA;",
                OwnerUserId = 912,
                Title = "Windows C++: How can I redirect stderr for calls to fprintf?",
                Tags = "<c++><windows><redirect>",
                AnswerCount = 3,
                CommentCount = 0
            });
            expected.Add(new Post
            {
                Id = 45325,
                PostTypeId = 1,
                ParentId = null,
                AcceptedAnswerId = 45327,
                CreationDate = creationDate2,
                Score = 220,
                ViewCount = 143052,
                Body = "<p>Sometimes when I'm editing page or control the .designer files stop being updated with the new controls I'm putting on the page."
                + "  I'm not sure what's causing this to happen, but I'm wondering if there's any way of forcing Visual Studio to regenerate the .designer file."
                + "  I'm using Visual Studio 2008</p>&#xA;&#xA;<p><strong>EDIT:</strong> Sorry I should have noted I've already tried:</p>&#xA;&#xA;<ul>&#xA;"
                + "<li>Closing &amp; re-opening all the files &amp; Visual Studio</li>&#xA;<li>Making a change to a runat=\"server\" control on the page</li>&#xA;<li>Deleting"
                + " &amp; re-adding the page directive</li>&#xA;</ul>&#xA;",
                OwnerUserId = 2975,
                Title = "How do you force Visual Studio to regenerate the .designer files for aspx/ascx files?",
                Tags = "<asp.net><visual-studio><visual-studio-2008>",
                AnswerCount = 38,
                CommentCount = 2
            });
            expected.Add(new Post
            {
                Id = 85553,
                PostTypeId = 1,
                ParentId = null,
                AcceptedAnswerId = 85656,
                CreationDate = creationDate3,
                Score = 195,
                ViewCount = 31740,
                Body = "<p>MSDN says that you should use structs when you need lightweight objects. Are there any other scenarios when a struct is preferable over a "
                + "class?</p>&#xA;&#xA;<p><strong>Edit:</strong><br />&#xA;Some people have forgotten that:<br />&#xA;1. <em>structs</em> can have methods!<br />&#xA;2."
                + " <em>structs</em> have no inheritance capabilites.</p>&#xA;&#xA;<p><strong>Another Edit:</strong><br />&#xA;I understand the technical differences,"
                + " I just don't have a good feel for <em>WHEN</em> to use a struct.</p>&#xA;",
                OwnerUserId = 781,
                Title = "When should I use a struct instead of a class?",
                Tags = "<.net><design><oop>",
                AnswerCount = 13,
                CommentCount = 2
            });

            int i = 0;
            foreach (Post post in result)
            {
                Assert.AreEqual(expected[i].Id, post.Id);
                Assert.AreEqual(expected[i].PostTypeId, post.PostTypeId);
                Assert.AreEqual(expected[i].ParentId, post.ParentId);
                Assert.AreEqual(expected[i].AcceptedAnswerId, post.AcceptedAnswerId);
                Assert.AreEqual(expected[i].CreationDate, post.CreationDate);
                Assert.AreEqual(expected[i].Score, post.Score);
                Assert.AreEqual(expected[i].ViewCount, post.ViewCount);
                Assert.AreEqual(expected[i].Body, post.Body);
                Assert.AreEqual(expected[i].OwnerUserId, post.OwnerUserId);
                Assert.AreEqual(expected[i].Title, post.Title);
                Assert.AreEqual(expected[i].Tags, post.Tags);
                Assert.AreEqual(expected[i].AnswerCount, post.AnswerCount);
                Assert.AreEqual(expected[i].CommentCount, post.CommentCount);
                i++;
            }
        }

        /// <summary>
        /// Testing the display of a particular comment. 
        /// Test class: CommentRepository. 
        /// Function called: GetById(92449). 
        /// Returns: Comment. 
        /// </summary>
        [Test]
        public void CommentRepository_GetById_ReturnsComment()
        {
            CommentRepository repository = new CommentRepository();
            Comment result = repository.GetById(92449);
            DateTime creationDate = new DateTime(2008, 10, 22, 10, 14, 24); //2008-10-22T10:14:24
            Comment expected = new Comment
            {
                Id = 92449,
                PostId = 225026,
                Score = 0,
                Text = "That's interesting. I'm still wondering if that also applies to Ruby.NET or any other interpreters I don't know about. "
                + "Maybe try to write my own 'simple' .NET binding that'd work on Xbox?",
                CreationDate = creationDate,
                UserId = 23153
            };

            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.PostId, result.PostId);
            Assert.AreEqual(expected.Score, result.Score);
            Assert.AreEqual(expected.Text, result.Text);
            Assert.AreEqual(expected.CreationDate, result.CreationDate);
            Assert.AreEqual(expected.UserId, result.UserId);
        }

        /// <summary>
        /// Testing the display of a particular unexisting comment. 
        /// Test class: CommentRepository. 
        /// Function called: GetById(1). 
        /// Returns: null. 
        /// </summary>
        [Test]
        public void CommentRepository_GetByIdUnexisting_ReturnsNull()
        {
            CommentRepository repository = new CommentRepository();
            Comment result = repository.GetById(1);

            Assert.IsNull(result);
        }

        /// <summary>
        /// Testing the display of all the comments (with a limit of 4). 
        /// Test class: CommentRepository. 
        /// Function called: GetAll(4, 0). 
        /// Returns: IEnumerable Comment. 
        /// </summary>
        [Test]
        public void CommentRepository_GetAll_returnIEnumerable()
        {
            CommentRepository repository = new CommentRepository();
            IEnumerable<Comment> result = repository.GetAll(4, 0);
            List<Comment> expected = new List<Comment>();

            DateTime creationDate1 = new DateTime(2008, 10, 22, 10, 14, 24); //2008-10-22T10:14:24
            DateTime creationDate2 = new DateTime(2008, 12, 24, 19, 2, 29); //2008-12-24T19:02:29
            DateTime creationDate3 = new DateTime(2008, 12, 24, 19, 6, 3); //2008-12-24T19:06:03
            DateTime creationDate4 = new DateTime(2008, 12, 24, 19, 15, 24); //2008-12-24T19:15:24

            expected.Add(new Comment
            {
                Id = 92449,
                PostId = 225026,
                Score = 0,
                Text = "That's interesting. I'm still wondering if that also applies to Ruby.NET or any other interpreters I don't know about. "
                + "Maybe try to write my own 'simple' .NET binding that'd work on Xbox?",
                CreationDate = creationDate1,
                UserId = 23153
            });
            expected.Add(new Comment
            {
                Id = 219181,
                PostId = 392022,
                Score = 0,
                Text = "Usually when you kill the parent, the children die as well.",
                CreationDate = creationDate2,
                UserId = 15689
            });
            expected.Add(new Comment
            {
                Id = 219191,
                PostId = 392022,
                Score = 22,
                Text = "Well technically they become Zombies...&#xA;That scares me and they need to go!",
                CreationDate = creationDate3,
                UserId = 26658
            });
            expected.Add(new Comment
            {
                Id = 219201,
                PostId = 392022,
                Score = 19,
                Text = "Do not do this on Christmas, please.",
                CreationDate = creationDate4,
                UserId = 10740
            });

            int i = 0;
            foreach (Comment comment in result)
            {
                Assert.AreEqual(expected[i].Id, comment.Id);
                Assert.AreEqual(expected[i].PostId, comment.PostId);
                Assert.AreEqual(expected[i].Score, comment.Score);
                Assert.AreEqual(expected[i].Text, comment.Text);
                Assert.AreEqual(expected[i].CreationDate, comment.CreationDate);
                Assert.AreEqual(expected[i].UserId, comment.UserId);
                i++;
            }
        }
    }
}
