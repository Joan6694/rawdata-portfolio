
\documentclass[a4paper]{report}

% Letters instead of numbers in chapter
\renewcommand{\thechapter}{\Alph{chapter}} 

\usepackage[dvipsnames*,svgnames]{xcolor}

\definecolor{linksColor}{rgb}{0.0, 0.0, 0.7}

% Listings params
\usepackage{listings}

\lstset{ %
  commentstyle=\color{blue},
  extendedchars=true,
  keywordstyle=\color{cyan},
  stringstyle=\color{green!70!black},
  breaklines=true,
  backgroundcolor=\color{white!80!lightgray},
  basicstyle=\ttfamily,
  frame=single,
  aboveskip=15pt,
  belowskip=15pt,
  otherkeywords={USE, CALL},
  numbers=left,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  rulecolor=\color{black},
}

\usepackage{array}
\usepackage{appendix}
\usepackage{textcomp}

\usepackage{graphicx}
\graphicspath{{./Pictures/}}

\usepackage{hyperref}
\hypersetup{hidelinks, colorlinks=true, breaklinks=true, urlcolor=linksColor, urlbordercolor=linksColor, linkcolor=linksColor, citecolor=linksColor, bookmarksopen=false, pdftitle={Title}, pdfauthor={Author}}

\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage{microtype}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\title{RAWDATA Portfolio Project: SOVA}
\date{\today}
\author{Joan \textsc{Frutozo}, Sarah \textsc{Philippe}, Héctor \textsc{Moreno Cervera}, \and Mikel \textsc{Aranburu Guridi}}

\begin{document}

\pagenumbering{roman}

\maketitle

\vspace*{\fill}

\section*{Introduction}

This document is a report concerning the part one of the RAWDATA Portfolio Project. It is called SOVA, Stack Overflow Viewer and Annotator. This is a search engine used to search for posts and comments in Stack Overflow. It is also possible to label any post that you want to remember, add personal notes and keep a search history. The project is meant to be made with ASP.NET and MySQL. Here, we will just address the first part of the project, the database design.

\vspace*{\fill}

\thispagestyle{empty}

\tableofcontents
\thispagestyle{empty}

\newpage

\pagenumbering{arabic}

\chapter{Application Design}

SOVA (Stack Overflow Viewer and Annotator) is a search engine for Stack Overflow. The basic features that are required are:

\begin{itemize}
\item Search for posts and comments.
\item Track of search history.
\item A marking option for posts.
\item Annotations for posts.
\item A graphic display of this information.
\end{itemize}

Surely, these features will be included in our implementation of the Stackoverflow Viewer and Annotator. We do not intend to add more features for now, even though the project work may require us to modify or add functionalities.\par{}

\section{Post search}\label{application_design:post_search}

The post search will be the main page of the application. The search will be limited to the question posts (for performance and logic reasons). By default, the application will split all the words in the search string and seek those post titles that contain these words.\par{}

Besides the default mode, four options will be provided to improve the search:

\begin{description}
\item[With Body:] Also search in the body of the posts.
\item[Exact Search:] Show only the posts that contain the exact match of the search string in their title (or body if the first option is enabled).
\item[Only Annotated:] Show only the posts which are annotated.
\item[Only Solved:] Show only the posts which are solved.
\end{description}

Developers might need these options if the default search does not satisfy them. As expected, it is possible to combine different search modes. Post search results will be displayed in a table with the following columns:

\begin{enumerate}
\item The title of the post. It will be clickable and will bring the user the page with the question post and all the related answers.
\item An extract of the post. The length will be decided later.
\item Whether the post was solved or not.
\item The author of the post.
\item The date of creation.
\item Whether the post was favorited.
\item Whether the post was annotated.
\item A button to mark/unmark the post as favorite.
\end{enumerate}

Of course, the page will not display \emph{all} the matches. We will limit the results to a certain number per page. At the end of the search, the user will also have the possibility to evaluate the research by giving (+1) or (-1) to express whether this research helped him find the answer to his question or not.\par{}

\section{Search-History search}\label{application_design:search_history_search}

Besides storing the search history, the application will allow users to search through it and find what queries have been requested. The search method will be the same as the default post search and the results will also be displayed in a similar table with the following columns:

\begin{enumerate}
\item The keywords typed (a.k.a. the search string).
\item The score of the search (whether the search was useful or not). It will be +1, 0 or -1.
\item The number of results this query returned.
\item The last datetime the query was searched.
\end{enumerate}

The results will be ordered by the score of the search in descending order.\par{}

There will also be an option to the search. It will be possible to select only the search that the user rated as "good" (+1), all the search that are either "good" or "normal" (+1, 0), or everything (+1, 0, -1). The default value for this options is "everything".\par{}

\section{Annotations and marking}\label{application_design:annotations_marking}

There will be an option to annotate question posts (i.e. personal notes). It will be used to add some precisions to posts. The annotation will be displayed as a text input that can be modified by the user. This input will be visible by the user no matter where he is on the page.\par{}

It will also be possible to mark/unmark a post as favorite just by clicking on a button on the post page or on the post results page (see \ref{application_design:post_search}). After marking some posts, the user will be able to view all of his favorites and search through them on a dedicated page. It will be shown like the post search with the following columns:

\begin{enumerate}
\item The title of the post. It will be clickable and will show up a page with the question post and all the related answers.
\item An extract of the post.
\item Whether the post was solved or not.
\item The author of the post.
\item The date of creation.
\item Whether the post was annotated.
\item A button to mark/unmark the post as favorite.
\end{enumerate}

\chapter{The QA-model}\label{qa}

The Question Answer model, the core of the SOVA, is a group of tables that contain the posts, comments, users, tags and votes that were imported from Stack Overflow. What we did here was to take the raw database from the "stackoverflow\_sample.sql" file and create a new database that fitted our needs based on the previous one.\par{}

\section{Redesign}\label{qa:redesign}

Here is a diagram of the provided raw database:

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.95]{qa-before}
  \caption{The stackoverflow\_sample tables.}
\end{figure}

As you can see, all the tables are unrelated. Even though they seem to have fields that can be interpreted as foreign keys, the constraints are not present. Foreign key constraints are essential, because they ensure that the database is kept clean and they avoid big mistakes during the insertion/deletion of rows of the database, for example.\par{}

Our first task was to implement all of these constraints. The following diagram shows the relationships between the tables after adding the foreign keys:

\begin{figure}[h]
  \centering
  \includegraphics{qa-with-constraints}
  \caption{Constraints added to the stackoverflow\_sample.}
\end{figure}

The tables "badges" and "posthistory" are not linked to any other table, because we decided that we wouldn't need them in our final model. The following list explains all the constraints we added:

\begin{description}
\item[votes \textrightarrow{} users and votes \textrightarrow{} posts:] a user can have many votes due to the field "UserId" in the "votes" table. A vote is also linked to a post by the field "PostId" of the "votes" table. A post can have many votes. To be more specific, it means that all the users can upvote/downvote every post.
\item[comments \textrightarrow{} users and comments \textrightarrow{} posts:] these two relations mean practically the same as those in the "votes" table. All  the users can comment on all the posts. "Comments" is linked to users with its field "UserId" and to "posts" with "PostId".
\item[ti \textrightarrow{} tags and ti \textrightarrow{} posts:] "ti" is a table that helps create a many-to-many relationship between "posts"and "tags". It only contains two foreign keys which are "pid" and "tid" that respectively link "ti" with "posts" and "tags". A post can have many tags and a tag can have many posts.
\item[postlinks \textrightarrow{} posts:] this relationship is written twice (this is normal). The fields "PostId" and "RelatedPostId" from the table "postlinks" are both linked to the field "id" of the "posts" table. In this case, a post can have several related posts, and the table "postlink" is here to create this relation. It is also a many-to-many relationship between "posts" and "posts".
\item[posts \textrightarrow{} posts:] there are two relationships like this. The first is made by the field "ParentId". This one is set only if it's an "answer" post (see the field "PostTypeId" for the type of the post), otherwise it is equal to NULL. Also, this foreign key always points to a "question" post.\newline
The second is made with the field "AcceptedAnswerId". It is set only in "question" posts if the question was solved. It always points to an "answer" post.
\end{description}

Finally, we deleted the fields and table that we thought to be useless for the continuation of the project. We also renamed some of the fields:

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{qa-final}
  \caption{The final Question-Answer model.}
\end{figure}

Some tables won't be used at all in the first part of the SOVA (like, "ti" or "tags") but we decided to keep them for later for display purposes.

\newpage

\section{Database creation}\label{qa:database_creation}

After the design was finished we created the script to create the database. In fact, all the design part was done with MySQL Workbench, so we just had to generate the script from the software. Here is an example of a schema creation and a table creation:

\lstinputlisting[language=SQL, firstline=1, lastline=24]{"../SQL databases/Combined model.sql"} 

The statement "SET FOREIGN\_KEY\_CHECKS=0;" allows us to create the foreign key constraints even if some tables are missing. This is really useful during database creation and big insertions.

\section{Data import}\label{qa:data_import}

The data is imported by using a SQL script that inserts the data from the "stackoverflow\_sample" database. Here is an extract of the script:

\lstinputlisting[language=SQL, firstline=18, lastline=32]{"../SQL databases/Combined Model Insertions.sql"}

See the Appendix \ref{app:insertion_script} for the whole script.

\section{Data access optimization}\label{qa:dao}

In order to optimize the access to the data, we added indexes to the following fields:

\begin{description}
\item["PostId" in "postlinks":] "postlinks" is a table used to refer to the posts related to a certain post. It contains a field called "PostId" which is the id of the post we are currently looking at. By adding an index to this field, we will be able to retrieve all the related posts (the "RelatedPostId" field) as fast as possible just by selecting all the postlinks that have their "PostId" equal to the post we are currently looking at.
\item["ParentId" in "posts":] the field "ParentId" in the "post" table refers to the question post which the answer post is linked to. Adding an index to the "ParentId" field will improve the answer posts retrieval when searching all the posts with their "ParentId" equal to the id of the current post.
\item["Body" and "Title" from "posts":] as said in \ref{application_design:post_search}, the post search will be done by searching through the title \emph{and} the body of the post. In order to do that, we added an index of type "FULLTEXT" (more informations in the \ref{annotation_model:dao:important_note} section) to the fields "Body" and "Title" of "posts" and we used the "MATCH/AGAINST" functions.
\item["PostId" of "votes":] adding an index to the "PostId" of the "votes" table will be useful to retrieve and display all the votes of a certain post.
\item["PostId" of "comments":] same as the "votes" table, it will be useful to retrieve all the comments of a post.

\end{description}

\chapter{Annotation model}\label{annotation_model}

\section{The design}\label{annotation_model:design}

Here is the list of requirements for the Annotation model:

\begin{itemize}
\item Track of search history.
\item A marking option for posts.
\item Annotations for posts.
\end{itemize}

And this is the annotation model we created:

\begin{figure}[h]
  \centering
  \includegraphics{annotation}
  \caption{The Annotation model.}
\end{figure}

\subsection{The "searchhistory" table}\label{annotation_model:design:searchhistory}

\begin{description}
\item[Id:] the unique id of the search.
\item[SearchString:] the sentence the user wanted to search for.
\item[Date:] the date at which this search was last done.
\item[Mark:] the score of the search (see \ref{application_design:search_history_search})
\item[ResultsNumber:] the number of results retrieved by the query.
\item[SearchType:] the type of the search. We will use it a bit field. It means that each bit of this int will be used as a boolean to define the options that were used ofr this research (see \ref{application_design:post_search}).
\end{description} 

\subsection{The "postannotation" table}\label{annotation_model:design:postannotation}

\begin{description}
\item[PostId:] the unique id of the annotation but also the id of the post that was annotated.
\item[Favourite:] if the post was favorited or not.
\item[Annotation:] the annotation string.
\end{description}

\section{Merging the two models}\label{annotation_model:merging}

After creating the two models we merged them, here is the result:

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{combined}
  \caption{The Combined model.}
\end{figure}

The only thing that we added to our previous model was a foreign key constraint between "PostId" in the "postannotation" table and the "Id" in the "posts" table.

\section{Data access optimization}\label{annotation_model:dao}

In view of the fact that it is possible to search through the search history, we decided to add an index to the "searchhistory" table. The index is of type "FULLTEXT" (more informations in the \ref{annotation_model:dao:important_note} section) and is applied to the field "SearchString" in the "searchhistory" table. By doing that, it will be possible to search through the search history in the same way that the posts search.

\subsection{Important Note}\label{annotation_model:dao:important_note}

\emph{The procedures described in \ref{functions_procedures:post_search}, \ref{functions_procedures:search_history_search} and \ref{functions_procedures:favorites_search} don't work on the university server. The fact it that "FULLTEXT" indexes on "InnoDB" type tables are not supported by MySQL versions older than 5.6. The heart of these procedures use "FULLTEXT" indexes, therefore, they can only be run on a (at least) local MySQL 5.6 server.}

\chapter{Functions and procedures}\label{functions_procedures}

This chapter describes the functions and procedures that we implemented in our database and will be used in the next versions of this project.

\section{Post search}\label{functions_procedures:post_search}

"Result\_BasicSearch" is the main procedure of our application, it is the search algorithm we use to retrieve those posts that match with the search string. Like said in \ref{application_design:post_search}, the post search takes a string as parameter but also four booleans that corresponds to the four options that we want for our search:

\begin{description}
\item[VARCHAR(200) UserSearch:] the search string.
\item[BOOL IsWithBody:] if set to TRUE, the application will also perform the search in the body of the posts.
\item[BOOL IsExact:] if set to TRUE, the application will only search for the exact string in the database.
\item[BOOL IsAnnotated:] if set to TRUE, select only the posts that have been annotated.
\item[BOOL IsSolved:] if set to TRUE, select only the posts that are solved.
\end{description}

This procedure will return a table with the following fields:

\begin{enumerate}
\item The id of the question post
\item The title of the post.
\item An extract of the post.
\item Whether the post was solved or not.
\item The author of the post.
\item The date of creation.
\item Whether the post was favorited.
\item Whether the post was annotated.
\end{enumerate}

Our complete implementation is available in the Apendix \ref{app:fp:post_search}.

\newpage

\section{Search-History search}\label{functions_procedures:search_history_search}

This one is used to search through your search history. The algorithm is the same as the default post search and it has one option:

\begin{description}
\item[VARCHAR(200) UserSearch:] the string introduced to look for in the search history.
\item[INT Marked:] this option corresponds to the minimum value that the score of the search has to be. If it is set to -1 (default value), the procedure will ignore the score and return everything, if it is for example 1, the procedure will return all those queries that have a score greater or equal to 1.
\end{description}

The procedure will return a table with the following fields.

\begin{enumerate}
\item The keywords typed (a.k.a. the search string).
\item The score of the search.
\item The number of results this query returned.
\item The last datetime the query was searched.
\item The id of the search in the database.
\end{enumerate}

Our complete implementation is available in the Apendix \ref{app:fp:search_history_search}.

\section{Favorites search}\label{functions_procedures:favorites_search}

It is also possible to search through the favorited posts. The procedure is almost the same as the post search one. However, some differences are noticeable:

\begin{itemize}
\item The procedure will only search through the favorites.
\item The user can enter an empty string (it will show all of his favorites).
\end{itemize}

The procedure will return a table with the following fields:

\begin{enumerate}
\item The id of the question post
\item The title of the post.
\item An extract of the post.
\item Whether the post was solved or not.
\item The author of the post.
\item The date of creation.
\item Whether the post was annotated.
\end{enumerate}

Our complete implementation is available in the Apendix \ref{app:fp:favorites_search}.

\section{Post retrieval}\label{functions_procedures:post}

In order to display a post with all of its answers we had to implement another procedure. This one retrieves the question post and all of the answers (ordered by date). It takes only one parameter which is the id of the question post we want to retrieve.

The procedure returns these fields:

\begin{enumerate}
\item The id of the post
\item The title of the post.
\item The body of the post.
\item The score of the post (a.k.a. upvotes and downvotes).
\item The date of creation.
\item The id of the accepted answer (only for the question post).
\item The author of the post.
\item The number of comments.
\item Whether the post was favorited.
\item The annotation (if any).
\end{enumerate}

Our complete implementation is available in the Apendix \ref{app:fp:post_retrieval}.

\section{Comments retrieval}\label{functions_procedures:comments}

Each post (question or answer) might have some comments that need to be displayed below every post. We did a procedure that takes as input the id of the question post and returns all the comments related to this post and its answers.

"DetailsPosts\_Comments" also returns a table with these fields:

\begin{enumerate}
\item The id of the post the comment is linked to.
\item The text of the comment.
\item The score of the comment.
\item The creation date of the comment.
\item The name of the user who wrote this comment.
\end{enumerate}

The results are ordered by the id of the post they are linked to, and then by their creation date.\newline

Our complete implementation is available in the Apendix \ref{app:fp:comments_retrieval}.\newline

However, more procedures may be needed later, but since they are only small procedures, we decided to implement them later if needed.

\section{Tests}\label{functions_procedures:tests}

This section describes the scripts that we created to test our procedures.

\subsection{Post search (Result\_BasicSearch)}\label{Functions_procedures:tests:post_search}

Here is an extract of the script we use to test our post search:

\lstinputlisting[language=SQL, firstline=1, lastline=11]{"../SQL Tests/Result_BasicSearch_Test.sql"}

This is \emph{one} of the tests that are done within this script. First, the procedure is called. It sets a variable called "NumRows" which is the number of rows the procedure returns. Then, this number is copied into another variable. A real query that corresponds to what we need to retrieve is done, and finally, the number of rows returned by the procedure \emph{and} the query are compared. If the two numbers are the same, it means that the procedure works. Otherwise, it means that it failed. \emph{All our tests are like this}. 

\subsection{Search history search (SearchHistory)}\label{Functions_procedures:tests:search_history_search}

Here is an extract of the script we use to test our search history search:

\lstinputlisting[language=SQL, firstline=1, lastline=13]{"../SQL Tests/SearchHistory_Test.sql"}

\subsection{Post retrieval (DetailsPosts)}\label{Functions_procedures:tests:post_retrieval}

Here is an extract of the script we use to test the post retrieval:

\lstinputlisting[language=SQL, firstline=1, lastline=15]{"../SQL Tests/DetailsPosts_Test.sql"}

\newpage

\subsection{Comments retrieval (DetailsPosts\_Comments)}\label{Functions_procedures:tests:comments_retrieval}

Here is an extract of the script we use to test the comments retrieval:

\lstinputlisting[language=SQL, firstline=1, lastline=16]{"../SQL Tests/DetailsPosts_Comments_Test.sql"}

\chapter{Small and Big databases comparison}\label{small_big}

This section shows a speed comparison between some of our procedures showing the values from the "stackoverflow\_sample" database and the values from the "stackoverflow\_portfolio" database. Unfortunately, we had troubles importing the data from the big database and then haven't had enough time to make the tests with the big sample. However, here are some results:\newline

\centering
\begin{tabular}[H]{|l||r|r|}
  \hline
  \textbf{Stored Procedure} & \textbf{Small database} & \textbf{Big database} \\
  \hline
  \textbf{Result\_BasicSearch}("SQL Server", false, false, false, false) & 7 rows / 0.0s & TBA \\
  \textbf{Result\_BasicSearch}("SQL Server", true, false, false, false) & 147 rows / 0.062s & TBA \\
  \textbf{Result\_BasicSearch}("SQL Server", false, true, false, false) & 5 rows / 0.0s & TBA \\
  \hline
  \textbf{DetailsPosts}(201323) & 1 row / 0.016s & TBA \\
  \textbf{DetailsPosts\_Comments}(201323) & 10 rows / 0.015s & TBA \\
  \hline
\end{tabular}

\appendix
\appendixpage
\addappheadtotoc

\chapter{Creation script}\label{app:creation_script}

\lstinputlisting[language=SQL]{"../SQL databases/Combined Model Schema.sql"}

\chapter{Insertion script}\label{app:insertion_script}

\lstinputlisting[language=SQL]{"../SQL databases/Combined Model Insertions.sql"}

\chapter{Functions and procedures implementations}\label{app:fp}

\section{Post search}\label{app:fp:post_search}

\lstinputlisting[language=SQL]{"../SQL Stored procedures/Result_BasicSearch.sql"}

\section{Search history search}\label{app:fp:search_history_search}

\lstinputlisting[language=SQL]{"../SQL Stored procedures/SearchHistory.sql"}

\section{Favorites search}\label{app:fp:favorites_search}

\lstinputlisting[language=SQL]{"../SQL Stored procedures/Result_FavouritesPost.sql"}

\section{Post retrieval}\label{app:fp:post_retrieval}

\lstinputlisting[language=SQL]{"../SQL Stored procedures/DetailsPosts.sql"}

\newpage

\section{Comments retrieval}\label{app:fp:comments_retrieval}

\lstinputlisting[language=SQL]{"../SQL Stored procedures/DetailsPosts_Comments.sql"}

\end{document}
