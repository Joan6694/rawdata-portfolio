﻿\documentclass[a4paper]{report}

%%\renewcommand{\thechapter}{\Alph{chapter}}

\usepackage{titlesec}

\titleformat{\chapter}[display]
{\normalfont\huge\bfseries}{\chaptertitlename\ \thechapter}{20pt}{\Huge}
\titlespacing*{\chapter}{0pt}{0pt}{3pt}

\usepackage[dvipsnames*,svgnames]{xcolor}

\definecolor{linksColor}{rgb}{0.0, 0.0, 0.7}

\def\signed #1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip2em
  \hbox{}\nobreak\hfil(#1)%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

\newsavebox\mybox
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}}
  {\signed{\usebox\mybox}\end{quote}}


% Listings params
\usepackage{listings}

\lstset{
  language=SQL,
  keywordstyle=\color{cyan}\ttfamily,
  commentstyle=\color{blue}\ttfamily,
  stringstyle=\color{green!70!black}\ttfamily,
  numberstyle=\color[rgb]{0.205, 0.142, 0.73}\ttfamily,
  extendedchars=true,
  breaklines=true,
  backgroundcolor=\color{white!80!lightgray},
  basicstyle=\footnotesize\ttfamily,
  frame=single,
  aboveskip=\baselineskip,
  belowskip=\baselineskip,
  numbers=left,
  showspaces=false,
  showstringspaces=false,
  otherkeywords={USE, CALL, DECLARE, BEGIN},
  showtabs=false,
  rulecolor=\color{black},
  morecomment=[l][\color{magenta}]{\#},
  columns=fixed
}

\usepackage{array}
\usepackage{appendix}
\usepackage{textcomp}

\usepackage{graphicx}
\graphicspath{{./Pictures/}}

\usepackage{hyperref}
\hypersetup{hidelinks, colorlinks=true, breaklinks=true, urlcolor=linksColor, urlbordercolor=linksColor, linkcolor=linksColor, citecolor=linksColor, bookmarksopen=false, pdftitle={Title}, pdfauthor={Author}}

\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}
\usepackage{microtype}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fancyvrb}

\usepackage{textgreek}

\title{RAWDATA Portfolio Project: SOVA}
\date{\today}
\author{Joan \textsc{Frutozo}, Sarah \textsc{Philippe}, Héctor \textsc{Moreno Cervera}, Mikel \textsc{Aranburu Guridi}}

\begin{document}

\pagenumbering{roman}

\maketitle

\vspace*{\fill}

\section*{Introduction}

This document describes the second part of our RAWDATA Portfolio project. This time, we had to create the Web Service backend of the application. This REST API will be used by the next and last section of the SOVA project to retrieve data from the database created in the first portfolio.

\vspace*{\fill}

\thispagestyle{empty}

\tableofcontents
\thispagestyle{empty}

\newpage

\pagenumbering{arabic}

\chapter{Application design}\label{app_design}

This chapter describes our preliminary design for the Web Service part of our SOVA application.

\section{Use cases}\label{app_design:use_cases}

In order to design our application, we examined the use cases that any user would want to perform. This is the list of the use cases that our application will cover:

\begin{itemize}
\item See all posts.
\item Search for posts, specifying certain parameters (for example, search only posts that have been favorited).
\item Display a particular post (this implies that the answer posts will be displayed too, and the possibility to see comments and annotations) .
\item See the basic information of a user.
\item Create a new annotation for a specific post.
\item Delete an annotation.
\item Update an annotation.
\item See all search history entries.
\item Search for search history entries.
\end{itemize}

Based on this list of basic use cases, we have created a backend system that provides the data and business logic for our application.

\section{Architecture}\label{app_design:architecture}

The architecture of the application goes like this from bottom to top:

\begin{enumerate}
\item The first and bottommost layer is the actual database. It is a MySQL database that we created during the first section of the portfolio project. It was inspired by the stackoverflow database model provided to us. We also added some tables to this model in order to handle the annotation and the search history functionalities.\newline
Most of the business logic is handled in the database thanks to the stored procedures we created (see Chapter \ref{bl} for the procedures). For example, when the user search for a post, the stored procedure that handles the search adds (or updates) an entry in the search history table. For more information, refer to the report of the rawdata portfolio part one.
\item The next layer is the Data Access Layer (further information can be found in Chapter \ref{dal}). It contains the domain objects, which are the raw objects extracted from database, and the repository classes, which are the classes used to connect to the database, send requests and in our case, create domain objects.
\item Finally, the last layer is the Web Service Layer (further information can be found in Chapter \ref{wsl}). This one is used to create a RESTful API in order to make the data retrievable and updatable by HTTP requests. The layer contains is made up of controller classes used to handle the needed HTTP requests, that create and call the repositories, retrieve the data, create the model objects (which are the objects sent to the clients) and send them back to the client.
\end{enumerate}

\section{The layers}\label{app_design:layers}

\subsection{Data Access Layer}\label{app_design:layers:dal}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{RepositoryAndDomain}
  \caption{Repository interfaces and example classes, domain objects}
\end{figure}

The first layer (the Data Access Layer) is mainly composed of 2 parts: the repositories and the domain objects.\par{}
The basic structure is as shown in the figure above.

\subsubsection{Repositories}\label{app_design:layers:dal:repo}

Repositories can implement 2 possible interfaces:

\begin{itemize}
\item \texttt{IRepository} defines the structure of read only repositories. It implements the operations \texttt{GetAll} and \texttt{GetById}.
\item \texttt{IUpdatableRepository} defines the structure of updatable repositories, which due to the fact that they also include the read operations, will always implement \texttt{IRepository}. In our particular application, the only updatable repository is \texttt{AnnotationRepository}. This classes have the operations \texttt{Delete}, \texttt{Insert} and \texttt{Update}. 
\end{itemize}

\subsubsection{Domain objects}\label{app_design:layers:dal:domains}

Domain objects consist of classes without functionalities whose attributes match the fields in the database tables.\par{} 
This allows repositories to know which information to gather from the database and create the matching object from the query results.

\newpage

\subsection{Web Service Layer}\label{app_design:layers:wsl}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{ControllerAndModel}
  \caption{Controller generic classes, Model example classes and interfaces}
\end{figure}

The second layer is the Web Service Layer used to handle web requests and display the data retrieved from the Data Access Layer. With a similar structure to that of the DAL, it consists of Controllers and Models.

\subsubsection{Controllers}\label{app_design:layer:wsl:cont}

The controllers have one main purpose: call the functions inside the repositories to fill the domain objects and be able to fetch the data gathered in the models. They also set an \texttt{HttpResponseMessage} to inform if the operation worked or not.

Similar to the interfaces defined for the repositories, controllers can inherit from 2 different abstract classes (which are defined over a particular repository, domain and model):

\begin{itemize}
\item \texttt{AbstractController<Repository, Domain, Model>} defines the operations of read only controllers. It consists of two \texttt{Get} operations. \texttt{GetAll(limit, offset)} to fetch the data from the read-only repository as a list of domain objects and fill the model from that list, and \texttt{Get(id)} to fill a model from only one particular instance. 	
\item \texttt{AbstractUpdatableController<UpdatableRepository, Domain, Model>} implements \texttt{Abstract\-Controller} and also the operations of updatable controllers (For the moment, only \texttt{Annotation\-Controller} inherits from this interface). As opposed to \texttt{AbstractController}, it includes the operations \texttt{Put} (to create), \texttt{Post} (to update) and \texttt{Delete} from the repository side, operations that are not filling a domain but modifying the database.
\end{itemize}

\subsubsection{Models}\label{app_design:layer:wsl:models}

In our application, models are used to display the knowledge from the database in the webpage as user-friendly as possible. The models are basically classes having the same attributes than the domain object, except that the IDs, which are integers used as primary or foreign keys in the database, are replaced with some Urls that correspond to the path of the resource. The only operation contained in the class is \texttt{FillWithDomain}.\par{}

The models can inherit from 2 different interfaces:

\begin{itemize}
\item \texttt{IFillableModel} which is for read only models. The only operation is \texttt{FillWithDomain} in order to fill the model from the \texttt{DomainObject}. 
\item \texttt{IDumpableModel} which is for dumpable models (for the moment only \texttt{AnnotationModel} implements this interface, for more information see chapter \ref{bl:res_bs}). The only operation is \texttt{DumpDomain} which allows to create a domain object from a model.
\end{itemize}

For an exhaustive list of all the domain objects as well as the XML/JSON object, see the Appendix \ref{appendix:domains_models}.

\chapter{Data Access Layer}\label{dal}

This chapter explains what we did concerning the Data Access Layer (DAL) part of the project. 
In our case, for the sake of simplicity, we have decided to think of our database as only one database, including both the original Stackoverflow data and our annotation model data.

 \section{Repositories}
In order to make the application more flexible regarding knowledge sources, we have implemented repositories for our data. \par{}
Our repositories make SQL queries to the MySQL database, and make use of the stored procedures we implemented in the first part of the project (these stored procedures contain a big part of the business logic of the application, because they manage the different searches and involved parameters, see Chapter \ref{bl} for more information).\par{}
Following the logical division of the Stackoverflow data (read only) and the annotation model data (read/write), we have created a standard repository interface (\texttt{IRepository}) and a writable repository interface (\texttt{IUpdatableRepository}). The repository that manages \texttt{Annotations} (\texttt{AnnotationRepository}) implements \texttt{IUpdatableRepository}, and the rest of domain objects are managed by repositories that implement \texttt{IRepository}.\par{}
Finally, the repositories that use the stored procedures are implemented without any interfaces because the parameters and the return values of some stored procedures are more complex and specific.

\section{Data Mapping}
Our application queries raw data from the database, which in turn has to be transformed into a Domain Object in order for us to work with it. \par{}
This mapping is done in the repositories, that is for example, in the \texttt{PostRepository} a \texttt{Post} (Domain Object) will be created when the data of that post is queried from the database.

\chapter{Business Layer}\label{bl}

We decided not to implement a business layer in the backend part because all the logic was already being done in the database. On the database side, we have 7 stored procedures which include this logic.
In the following part we will describe the main use of each procedure, and where are they called in the backend.

\section{Result\_BasicSearch}\label{bl:res_bs}

This stored procedure performs the search of posts depending on 7 different parameters:
\begin{description}
\item[\texttt{UserSearch}:] a String corresponding to the keywords the user is looking for.
\item[\texttt{IsWithBody}:] a Boolean to define whether we have to search for the keywords only in the title or also in the body of the post.
\item[\texttt{IsExact}:] a Boolean to define whether the match with the keyword has to be exact.
\item[\texttt{IsAnnotated}:] a Boolean to define whether we want only annotated posts.
\item[\texttt{IsSolved}:] a Boolean to define whether we want only solved posts.
\item[\texttt{ULimit}:] an Integer to define the maximum number or results returned.
\item[\texttt{UOffset}:] an Integer defining the offset.
\end{description}

This basic search also automatically inserts the search done by the user in the searchhistory table, and if the search already existed, it is updated.\par{}

To see the full code, see the Appendix \ref{appendix:stored_pro:res_bs}.

\section{Result\_FavouritesPost}\label{bl:res_fp}

This stored procedure has 7 parameters and adapts the search of posts depending on these parameters. It only searches for posts which have been set as favourites. The parameters are:

\begin{description}
\item[\texttt{UserSearch}:] a String corresponding to the keywords the user is looking for.
\item[\texttt{IsWithBody}:] a Boolean to define whether we have to search for the keywords only in the title or also in the body of the post.
\item[\texttt{IsExact}:] a Boolean to define whether the match with the keyword has to be exact.
\item[\texttt{IsAnnotated}:] a Boolean to define whether we want only annotated posts.
\item[\texttt{IsSolved}:] a Boolean to define whether we want only solved posts.
\item[\texttt{ULimit}:] an Integer to define the maximum number or results returned.
\item[\texttt{UOffset}:] an Integer defining the offset.
\end{description}

To see the full code, see the Appendix \ref{appendix:stored_pro:res_fp}.

\section{SearchHistory}\label{bl:sh}

This stored procedure allows the user to search through his search history. The information retrieved will only be queries the user has already done in the \texttt{Basic\_Search}. 
This stored procedure has 4 parameters:

\begin{description}
\item[\texttt{UserSearch}:] a String corresponding to the keywords the user is looking for.
\item[\texttt{Marked}:] the minimum value of the mark we are looking for.
\item[\texttt{IsSolved}:] a Boolean to define whether we want only solved posts.
\item[\texttt{ULimit}:] an Integer to define the maximum number or results returned.
\end{description}

To see the full code, see the Appendix \ref{appendix:stored_pro:sh}.

\section{Insert\_Annotation}\label{bl:ia}

This stored procedure inserts an annotation in the \texttt{postannotation} table only if the post we want to link the annotation to exists. 
This stored procedure has 3 parameters:

\begin{description}
\item[\texttt{Pid}:] the id of the post we want to add an annotation to.
\item[\texttt{Fav}:] a Boolean to set the post as favourite or not. 
\item[\texttt{Anno}:] the annotation the user wants to link at the post.
\end{description}

To see the full code, see the Appendix \ref{appendix:stored_pro:ia}.

\section{Update\_Annotation}\label{bl:ua}

This stored procedure performs the update of annotations. If we try to update a non existing annotation, this procedure calls the \texttt{Insert\_Annotation} procedure so we can create the annotation.
This stored procedure has 3 parameters:

\begin{description}
\item[\texttt{Pid}:] the id of the post whose annotation we want to update (or add if unexsisting). Note that the post id and the annotation id is the same because the relationship is one-to-one.
\item[\texttt{Fav}:] a Boolean to set the post as favourite or not. 
\item[\texttt{Anno}:] the annotation the user wants to link at the post.
\end{description}

To see the full code, see the Appendix \ref{appendix:stored_pro:ua}.

\section{DetailsPosts}\label{bl:dp}

This stored procedure retrieves all the posts linked to a question post: all the answers to that question are returned as a result.\par{}

Its only parameter is the \texttt{PostId} of the question post.\newline

To see the full code, see the Appendix \ref{appendix:stored_pro:dp}.

\section{DetailsPosts\_Comments}\label{bl:dp_c}

This stored procedure retrieves all the comments linked to a post.\par{}
Its only parameter is the \texttt{PostId} of the post.\newline

To see the full code, see the Appendix \ref{appendix:stored_pro:dp_c}.

\chapter{Web Service Layer}\label{wsl}

This chapter describes our api, the URIs that can be called and what they return.

\section{UserApi}

URI: \texttt{/api/users/[id]}\footnotemark{}\par{}

\footnotetext{Each URI should be appended to the domain name like this for example: \url{http://example.com/api/users/666}.}

Used to retrieve a list of users or a certain user (if the optional parameter \texttt{id} is given). Can only be called with the \texttt{GET} HTTP method like this:

\begin{description}
\item[\texttt{GET /api/users/}:] returns a list of users. Some optional parameters can be given in the query string: \texttt{limit} (limit the request to $n$ results) and \texttt{offset} (shift the result set so that it ignores the $n$ first elements). By default, \texttt{limit} = 25 and \texttt{offset} = 0.
\item[\texttt{GET /api/users/666}:] returns the user with the \texttt{id} 666.
\end{description}

From now on through the rest of the document, all the requests that can take a limit and an offset return an object (called \texttt{PagingObject}) that wraps the object list and that contains two Urls: the first one is called \texttt{PrevPage} and redirects to the previous page in order to have the previous results, and the second one is called \texttt{NextPage} and redirects to the next page of the results.\par{}
Also all of the requests return an \texttt{HttpResponseMessage} showing if the operation was perfomed correctly.

\section{PostApi}

URI: \texttt{/api/posts/[id]}\par{}

Used to retrieve a list of posts or a certain post. Can only be called with the \texttt{GET} HTTP method like this:

\begin{description}
\item[\texttt{GET /api/posts}:] returns a list of posts. Optional parameters: \texttt{limit} and \texttt{offset}.
\item[\texttt{GET /api/posts/666}:] returns the post with the \texttt{id} 666.
\end{description}
 

\section{CommentApi}

URI: \texttt{/api/comments/[id]}\par{}

Used to retrieve a list of comments or a certain comment. Can only be called with the \texttt{GET} HTTP method like this:

\begin{description}
\item[\texttt{GET /api/comments}:] returns a list of comments. Optional parameters: \texttt{limit} and \texttt{offset}.
\item[\texttt{GET /api/comments/666}:] returns the comment with the \texttt{id} 666.
\end{description}

\section{SearchHistoryEntryApi}

URI: \texttt{/api/searchhistoryentries/[id]}\par{}

Used to retrieve a list of search history entries or a certain entry. Can only be called with the \texttt{GET} HTTP method like this:

\begin{description}
\item[\texttt{GET /api/searchhistoryentries}:] returns a list of search history entries. Optional parameters: \texttt{limit} and \texttt{offset}.
\item[\texttt{GET /api/searchhistoryentries/666}:] returns the search history entry with the \texttt{id} 666.
\end{description}

\section{SearchHistorySearchApi}

URI: \texttt{/api/searchhistorysearch/[searchString]}\par{}

Retrieves a list of search history entries that match the parameter \texttt{searchString}. If no parameters are specified, it has the same behaviour as the \texttt{SearchHistoryEntryApi}. The \texttt{limit} and \texttt{offset} also work in this case. Another parameter called \texttt{marked} can also be given in the query string. Every search entry in the database has also a field called \texttt{marked} that can be either -1, 0 or 1 and that corresponds to the usefulness of the search (it is set by the user). For example, if the \texttt{marked} parameter in the query string is set to 0, the request will return all of the search entries that have a \texttt{marked} field equal or greater than 0.

\section{TagApi}

URI: \texttt{/api/tags/[id]}\par{}
Used to retrieve a list of tags or a certain tag. Can only be called with the \texttt{GET} HTTP method like this:

\begin{description}
\item[\texttt{GET /api/tags}:] returns a list of tags. Optional parameters: \texttt{limit} and \texttt{offset}.
\item[\texttt{GET /api/tags/666}:] returns the tag with the \texttt{id} 666.
\end{description}

\section{AnnotationApi}

URI: \texttt{api/annotations/[postId]}\par{}

Used to retrieve a list of annotations or a certain annotation. It can be called with the \texttt{GET}, \texttt{POST}, \texttt{PUT} and \texttt{DELETE} HTTP methods like this:

\begin{description}
\item[\texttt{GET /api/annotations}:] returns a list of annotations. Optional parameters: \texttt{limit} and \texttt{offset}.
\item[\texttt{GET /api/annotations/666}:] returns the annotation linked with the post that has the \texttt{postId} 666. Note that the post id and the annotation id is the same because the relationship is one-to-one.
\item[\texttt{PUT /api/annotations/667}:] adds a new annotation to the post with the \texttt{postId} 667. The annotation itself has to be given in the body of the request and has to respect the fields of the \texttt{AnnotationModel} class (see figure \ref{appendix:domains_models:models} in the Appendix \ref{appendix:domains_models}).
\item[\texttt{POST /api/annotations/668}:] updates the annotation linked to the post having the \texttt{postId} 668, and if this post does not have a linked annotation, it is created. As above, the annotation itself has to be given in the body of the request.
\item[\texttt{DELETE /api/annotations/669}:] deletes the annotation linked to the post 669.
\end{description}

\section{PostsSearchApi}

URI: \texttt{/api/search/searchString}\par{}

This is the main api of our project. It returns a list of post search entries where the title of the post match the \texttt{searchString} and can be only called with the \texttt{GET} HTTP method like this:

\begin{description}
\item[\texttt{GET /api/search/sql}:] returns a list of posts search entries with posts containing ``sql'' in their title.
\end{description}

In addition to the \texttt{limit} and \texttt{offset} parameters, the query string can also be given the following:
\begin{itemize}
\item \texttt{isWithBody} (\texttt{boolean}): if true, the search algorithm will also look in the body of the post.
\item \texttt{isExact} (\texttt{boolean}): if true, the search algorithm will look for an exact match of the search string instead of a match of the different words of the string.
\item \texttt{isAnnotated} (\texttt{boolean}): if true, the search algorithm only returns the results that are annotated.
\item \texttt{isSolved} (\texttt{boolean}): if true, the search algorithm only returns the results that are solved.
\end{itemize}

As you can see, the \texttt{searchString} parameter is mandatory.

\section{FavouritesSearchApi}

URI: \texttt{/api/favourites/[searchString]}\par{}

This one behaves exactly the same as the \texttt{PostsSearchApi} but it will only search for the favourited posts. Also it takes the same optional parameters:
\begin{itemize}
\item \texttt{isWithBody} (\texttt{boolean})
\item \texttt{isExact} (\texttt{boolean})
\item \texttt{isAnnotated} (\texttt{boolean})
\item \texttt{isSolved} (\texttt{boolean})
\end{itemize}

In this case, the \texttt{searchString} parameter is optional. If it is not given, all of the favourites will be returned (according to the \texttt{limit} and \texttt{offset} parmeters).

\section{DisplayPostsApi}

URI: \texttt{/api/displayposts/postId}\par{}

This api returns the posts intended for display. This means that it returns the question post with the id \texttt{postId} and all of the answers. Can only be called with a \texttt{GET} HTTP method. Optional parameters: \texttt{limit} and \texttt{offset}.

\section{DisplayCommentsApi}

URI: \texttt{/api/displaycomments/postId}\par{}

This api returns the comments intended for display. It means that it returns all of the comments of the post with the id \texttt{postId}. Can only be called with a \texttt{GET} HTTP method. Optional parameters: \texttt{limit} and \texttt{offset}.


\chapter{Testing}\label{tests}

Due to the fact that our design was not Test-driven, after finishing the application we implemented tests to make sure our SOVA was working properly. Both unit tests and integration tests were made for this purpose. Unit tests also check the behaviour of our application when failing or when a wrong parameter is passed. We can differentiate our tests among layers (Data Access Layer, Web Service Layer and User Interface) as well as, regarding DAL, between repositories. Note that we decided to run the tests within a local version of our database so that afterwards the online database would still remain the same. \par{}

\section{Testing DAL}\label{tests:DAL}

As the Data Access Layer is the core of the backend, which retrieves knowledge from the database, unit tests were implemented for this layer. The DAL is made up of domain objects and repositories. Domain objects do not represent testable code because their purpose in the application is simple: domain objects serve as a representation of the database information, with no additional functionalities. On the other hand, repositories implement testable operations, so we focused on the repositories. \par{}

Inspecting the project the reader can find three different classes inside the folder ``Tests DAL''. Each of them tests one particular type of repository. (see section \ref{tests:Summary} for all the tests) \par{}

\begin{itemize}
\item \texttt{RepositoryTests}: this class was made to check the retrieval of information from the database through the read-only repositories (those implementing the interface \texttt{IRepository}). The following repositories are tested in this file: \texttt{UserRepository}, \texttt{TagRepository}, \texttt{PostRepository} and \texttt{CommentRepository}.
\item \texttt{StoredProcedureTests}: makes sure that the stored procedures work as expected, retrieving the same information as in the database even when passing an incorrect parameter. All the stored procedures and the following repositories are tested in this file: \texttt{DisplayCommentEntryRepository}, \texttt{DisplayPostEntryRepository}, \texttt{FavouriteSearchRepository}, \texttt{PostSearchRepository} and \texttt{Search\-HistoryEntryRepository}.
\item \texttt{UpdatableRepositoryTests}: within this file we test the \texttt{AnnotationRepository} (which implements \texttt{IUpdatableRepository}). Checks were made for all the CRUD operations.
\end{itemize}

\section{Testing WSL}\label{tests:WSL}

The Web Service Layer has a structure similar to the DAL. WSL is composed of Controllers that retrieve information directly from Repositories, and Models which are the translation of the domain data into the objects used in the application display. In order to test the Web Service Layer, we tried to use the Moq library to mock the repository classes and unit test the controllers on their own by removing the dependencies between WSL and DAL. \par{}

Following the example seen in the RawData lectures, we implemented a .cs file called “ControllerTests.cs” located in the TestsWSL folder inside the rawdata-portfolio project. In that file we defined two new classes for each different domain object: a fake repository and a fake controller. The fake repository implements the \texttt{IRepository} interface, but instead of doing the usual IO operations, its methods return the expected result by the tests. By doing so, the controller implementation no longer depends on the particular repository implementation but on the expected result defined in the fake one. Our tests run the new controller using the mock repository. \par{}

Once the first fake controller test was implemented, we realised that it was also necessary to mock the http request. In the lecture example this was done by creating the controller dynamically with a mocked repository as a parameter in the constructor. This solution was incompatible with our design decision of using generics. We tried for days to mock the controller. Url but we could not manage to do it properly. In the end we decided to leave the code commented and check the controllers with integration tests and focused on testing the main unit of logic: the Repositories. \par{}

\section{Testing UI}\label{tests:UI}

Even though we could not run a unit tests for the controllers, we managed to do it for the repositories, the main working classes in charge of retrieving data from the database (after all, the controllers only call the repositories and translate the result into JSON). \par{}

Besides the Unit testing, we tested also the integration of our system by checking our running application. We could test our system both in the Azure website (see Chapter \ref{deploy} for more information) and our local VisualStudio project (with a local database). By running the application, we could easily check that the controllers were working properly as well as the whole SOVA so far. \par{}

In the Appendix \ref{appendix:tests} the reader can find snapshots of some UI tests. \par{}

\section{Test summary}\label{tests:Summary}

In this section we list the unit tests:

\begin{description}
\item[12 Repository Tests:]\hfill
\begin{itemize}
\item \texttt{CommentRepository\_GetAll\_returnIEnumerable}
\item \texttt{CommentRepository\_GetByIdUnexisting\_ReturnsNull}
\item \texttt{CommentRepository\_GetById\_ReturnsComment}
\item \texttt{PostRepository\_GetAll\_returnIEnumerable}
\item \texttt{PostRepository\_GetByIdUnexisting\_ReturnsNull}
\item \texttt{PostRepository\_GetById\_ReturnsPost}
\item \texttt{TagRepository\_GetAll\_returnIEnumerable}
\item \texttt{TagRepository\_GetByIdUnexisting\_ReturnsNull}
\item \texttt{TagRepository\_GetById\_ReturnsTag}
\item \texttt{UserRepository\_GetAll\_returnIEnumerable}
\item \texttt{UserRepository\_GetByIdUnexisting\_ReturnsNull}
\item \texttt{UserRepository\_GetById\_ReturnsUser}
\end{itemize}

\item[9 Updatable Repository Tests:]\hfill
\begin{itemize}
\item \texttt{AnnotationRepository\_DeleteUnexisting\_NothingHappens}
\item \texttt{AnnotationRepository\_Delete\_InstanceDisappears}
\item \texttt{AnnotationRepository\_GetAll\_returnIEnumerable}
\item \texttt{AnnotationRepository\_GetByIdUnexisting\_ReturnsEmpty}
\item \texttt{AnnotationRepository\_GetById\_ReturnsAnnotation}
\item \texttt{AnnotationRepository\_InsertExistingAnnotation\_SqlCrashes}
\item \texttt{AnnotationRepository\_Insert\_CreatesNewInstance}
\item \texttt{AnnotationRepository\_UpdateUnexistingAnnotation\_InsertsNewAnnotation}
\item \texttt{AnnotationRepository\_Update\_ModifiesExistingInstance}
\end{itemize}

\item[14 Stored Procedure tests:]\hfill
\begin{itemize}
\item \texttt{DisplayCommentEntryRepo\_GetResult\_ReturnsIEnumerable}
\item \texttt{DisplayCommentEntryRepo\_GetResultUnexisting\_ReturnsEmpty}
\item \texttt{DisplayPostEntryRepo\_GetResultFromPostWithoutAnswer\_ReturnsIEnumerable}
\item \texttt{DisplayPostEntryRepo\_GetResultFromAnswer\_ReturnsIEnumerable}
\item \texttt{DisplayPostEntryRepo\_GetResultFromPostWithAnswers\_ReturnsIEnumerable}
\item \texttt{DisplayPostEntryRepo\_GetResultUnexisting\_ReturnsEmpty}
\item \texttt{FavouriteSearchRepo\_GetResultDefault\_ReturnsIEnumerable}
\item \texttt{FavouriteSearchRepo\_GetResultFlagsTrue\_ReturnsIEnumerable}
\item \texttt{PostSearchRepo\_GetResultDefault\_RetunsIEnumerable}
\item \texttt{PostSearchRepo\_GetResultFlagsTrue\_ReturnsIEnumerable}
\item \texttt{SearchHistoryEntryRepo\_GetById\_ReturnsIEnumerable}
\item \texttt{SearchHistoryEntryRepo\_GetByIdUnexisting\_ReturnsNull}
\item \texttt{SearchHistoryEntryRepo\_GetResultDefault\_ReturnsIEnumerable}
\item \texttt{SearchHistoryEntryRepo\_GetResultMarked\_ReturnsIEnumerable}
\end{itemize}

\item[1 Controller test:]\hfill
\begin{itemize}
\item \texttt{FakeController\_GetPerson\_ReturnsPerson\_AlwaysSuccess}
\end{itemize}
\end{description}

\chapter{Deployment}\label{deploy}

To make our lifes easier, we decided to use continuous deployment with Microsoft Azure. We linked our BitBucket repository to a Microsoft Azure Web App so that each time someone pushes something on the git, the code is automatically deployed to the website.\par{}
Therefore, our project is available at the address \url{http://raw-portfolio.azurewebsites.net/} with the full documentation generated by VSdocman \href{http://raw-portfolio.azurewebsites.net/doc/}{here}.\par{}
Also, the source of the project is available at this address: \url{https://bitbucket.org/Joan6694/rawdata-portfolio}.

\appendix
\appendixpage
\addappheadtotoc

\chapter{Domain and XML/JSON objects}\label{appendix:domains_models}

Here are two diagrams generated with Visual Studio that represent our Domain and XML/JSON objects.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{Domains}
  \caption{All the domain objects}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{Models}
  \caption{All the XML/JSON (Models) objects}
  \label{appendix:domains_models:models}
\end{figure}

\chapter{Stored Procedures}\label{appendix:stored_pro}

This appendix is the full code of all our stored procedures. We chose to put the code here because we changed the behaviour and we added some stored procedures since the last report.

\section{Result\_BasicSearch}\label{appendix:stored_pro:res_bs}

\lstinputlisting{"../../SQL Scripts/SQL Stored procedures/Final procedures/Result_BasicSearch.sql"}

\section{Result\_FavouritesPost}\label{appendix:stored_pro:res_fp}

\lstinputlisting{"../../SQL Scripts/SQL Stored procedures/Final procedures/Result_FavouritesPost.sql"}

\section{SearchHistory}\label{appendix:stored_pro:sh}

\lstinputlisting{"../../SQL Scripts/SQL Stored procedures/Final procedures/SearchHistory.sql"}

\section{Insert\_Annotation}\label{appendix:stored_pro:ia}

\lstinputlisting{"../../SQL Scripts/SQL Stored procedures/Final procedures/Insert_Annotation.sql"}

\newpage

\section{Update\_Annotation}\label{appendix:stored_pro:ua}

\lstinputlisting{"../../SQL Scripts/SQL Stored procedures/Final procedures/Update_Annotation.sql"}

\section{DetailsPosts}\label{appendix:stored_pro:dp}

\lstinputlisting{"../../SQL Scripts/SQL Stored procedures/Final procedures/DetailsPosts.sql"}

\section{DetailsPosts\_Comments}\label{appendix:stored_pro:dp_c}

\lstinputlisting{"../../SQL Scripts/SQL Stored procedures/Final procedures/DetailsPosts_Comments.sql"}

\chapter{Testing}\label{appendix:tests}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 0 450 0, clip, width=\textwidth]{rawdataTesting1}
  \caption{Screenshot of how the testing worked in HTML before retrieving JSON objects}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{rawdataTesting15}
  \caption{Screenshot of all the tests passed}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 0 350 0, clip, width=\textwidth]{rawdataTesting2}
  \caption{Screenshot of the main page (\url{http://raw-portfolio.azurewebsites.net})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 280 0 0, clip, width=\textwidth]{rawdataTesting3}
  \caption{Screenshot of the UserApi (\url{http://raw-portfolio.azurewebsites.net/api/users})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 500 370 0, clip, width=\textwidth]{rawdataTesting4}
  \caption{Screenshot of a particular user (\url{http://raw-portfolio.azurewebsites.net/api/users/568})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 100 0 0, clip, width=\textwidth]{rawdataTesting5}
  \caption{Screenshot of the PostApi (\url{http://raw-portfolio.azurewebsites.net/api/posts?limit=3\&offset=11})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{rawdataTesting6}
  \caption{Screenshot of the CommentApi (\url{http://raw-portfolio.azurewebsites.net/api/comments})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 250 0 0, clip, width=\textwidth]{rawdataTesting7}
  \caption{Screenshot of the SearchHistoryEntriesApi (\url{http://raw-portfolio.azurewebsites.net/api/searchhistoryentries})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 400 0 0, clip, width=\textwidth]{rawdataTesting9}
  \caption{Screenshot of the AnnotationApi (\url{http://raw-portfolio.azurewebsites.net/api/annotations?limit=7\&offset=2})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 30 0 0, clip, width=\textwidth]{rawdataTesting8}
  \caption{Screenshot of the TagApi (\url{http://raw-portfolio.azurewebsites.net/api/tags?limit=67})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{rawdataTesting10}
  \caption{Screenshot of the DisplayPostsApi (\url{http://raw-portfolio.azurewebsites.net/api/displayposts/142816})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 420 0 0, clip, width=\textwidth]{rawdataTesting11}
  \caption{Screenshot of the DisplayCommentsApi (\url{http://raw-portfolio.azurewebsites.net/api/displaycomments/45325})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 420 0 0, clip, width=\textwidth]{rawdataTesting12}
  \caption{Screenshot of the SearchHistorySearchApi (\url{http://raw-portfolio.azurewebsites.net/api/searchhistorysearch/?marked=1})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 220 0 0, clip, width=\textwidth]{rawdataTesting13}
  \caption{Screenshot of the PostSearchApi (\url{http://raw-portfolio.azurewebsites.net/api/search/ruby?isWithBody=true\&isSolved=true\&isExact=true})}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[trim=0 450 0 0, clip, width=\textwidth]{rawdataTesting14}
  \caption{Screenshot of the FavouriteSearchApi (\url{http://raw-portfolio.azurewebsites.net/api/search/ascii?isAnnotated=true\&isExact=true\&isWithBody=true})}
\end{figure}

\end{document}
