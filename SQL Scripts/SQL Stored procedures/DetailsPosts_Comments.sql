CREATE DEFINER=`root`@`localhost` PROCEDURE `DetailsPosts_Comments`(IN SearchPostId INT)
BEGIN


SELECT P.Id AS PostId, C.Text, C.Score, C.CreationDate, If(C.UserId IS NULL, 'Anonymous', Users.Name) AS Username FROM  posts AS P  LEFT JOIN Users ON OwnerUserId = Users.Id  JOIN Comments C on P.Id = C.PostId
WHERE  P.Id = SearchPostId  OR ParentId = SearchPostId ORDER BY P.Id , CreationDate Asc;

END