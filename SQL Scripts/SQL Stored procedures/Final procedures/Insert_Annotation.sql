CREATE DEFINER=`root`@`localhost` PROCEDURE `Insert_Annotation`(IN pid int, IN fav Bool, IN anno VARCHAR(4096))
BEGIN

Set @NumRows = 0;
SET @s = CONCAT('SELECT Id FROM posts WHERE Id =',pid,';');


PREPARE stmt FROM @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;

IF (@NumRows > 0 || @NumRows is not null) THEN
		INSERT INTO postannotation (PostId,Favourite, Annotation) VALUE(pid, fav, anno);
	END IF;

	DEALLOCATE PREPARE stmt;
END