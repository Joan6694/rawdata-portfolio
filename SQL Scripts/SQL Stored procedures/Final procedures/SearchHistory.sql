CREATE DEFINER=`root`@`localhost` PROCEDURE `SearchHistory`(IN UserSearch VARCHAR(200), IN Marked INT, IN ULimit INT, IN UOffset INT )
BEGIN

SET @s = CONCAT('SELECT Id, SearchString, Mark, Date, ResultsNumber, SearchType FROM searchhistory');

IF (UserSearch != '') THEN
	SET @s = CONCAT(@s,' WHERE (MATCH(SearchString) AGAINST(''', UserSearch,''') OR SearchString LIKE ''%', UserSearch , '%'') ');
    END IF;

IF (UserSearch = '') THEN
	SET @s = CONCAT(@s, ' WHERE ');
ELSE
	SET @s = CONCAT(@s, ' AND ');
END IF;
SET @s = CONCAT(@s,  ' Mark >= ', Marked);
    

SET @s = CONCAT(@s, '  limit ', ULimit,' offset ', UOffset, '  ;');

PREPARE stmt FROM @s;
   EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END