CREATE DEFINER=`root`@`localhost` PROCEDURE `Update_Annotation`(IN pid int, IN fav Bool, IN anno VARCHAR(4096))
BEGIN

Set @NumRows = 0;
SET @s = CONCAT('SELECT PostId FROM postannotation WHERE PostId =',pid,'');


PREPARE stmt FROM @s;
	SELECT @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;

IF (@NumRows > 0 || @NumRows is not null) THEN
		SET @MyUp = CONCAT('UPDATE postannotation SET Favourite=',fav,' , Annotation=''',anno,''' WHERE PostId=',pid);
        PREPARE DoUpdate FROM @MyUp;
        EXECUTE DoUpdate;
       	DEALLOCATE PREPARE DoUpdate;
ELSE
		CALL  Insert_Annotation(@pid, @fav, @anno); 
	END IF;
		
	DEALLOCATE PREPARE stmt;
END