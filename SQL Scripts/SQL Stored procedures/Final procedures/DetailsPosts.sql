CREATE DEFINER=`root`@`localhost` PROCEDURE `DetailsPosts`(IN SearchPostId INT)
BEGIN

SELECT  Title, Body, Score, CreationDate, AcceptedAnswerId, 
		If(OwnerUserId IS NULL, 'Anonymous', users.Name) AS Username, CommentCount, 
        IF(PA.Favourite IS NULL,0, PA.Favourite) AS Favourite, P.Id AS 'Id', Annotation 
FROM posts AS P 
	LEFT JOIN postannotation AS PA ON PA.PostId = P.Id 
	LEFT JOIN users ON OwnerUserId = users.Id
WHERE  P.Id = SearchPostId  OR ParentId = SearchPostId
ORDER BY CreationDate Asc;

END