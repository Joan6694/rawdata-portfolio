CREATE DEFINER=`datalogi`@`%` PROCEDURE `DetailsPosts_Comments`(IN SearchPostId INT)
BEGIN

SELECT  P.Id AS PostId, C.Text, C.Score, C.CreationDate, 
		If(C.UserId IS NULL, 'Anonymous',users.Name) AS Username 
FROM  posts AS P  
LEFT JOIN users ON OwnerUserId = users.Id  
JOIN comments C ON P.Id = C.PostId
WHERE  P.Id = SearchPostId  
ORDER BY P.Id , CreationDate ASC;

END