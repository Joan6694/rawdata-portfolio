CREATE DEFINER=`root`@`localhost` PROCEDURE `SearchHistory`(IN UserSearch VARCHAR(200), IN Marked INT)
BEGIN

SET @s = CONCAT('SELECT SearchString, Mark, Date, ResultsNumber, Id FROM searchhistory');

IF (UserSearch != '') THEN
	SET @s = CONCAT(@s,' WHERE MATCH(SearchString) AGAINST(''', UserSearch,''') ');
    END IF;

IF (Marked = true) THEN
	IF (UserSearch = '') THEN
		SET @s = CONCAT(@s, ' WHERE ');
	ELSE
		SET @s = CONCAT(@s, ' AND ');
	END IF;
	SET @s = CONCAT(@s,  ' Mark >= ',  Marked);
END IF;

SET @s = CONCAT(@s, ' ;');

PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;
    DEALLOCATE PREPARE stmt;

END
