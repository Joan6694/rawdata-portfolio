CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_BasicSearch`(IN UserSearch VARCHAR(200), IN IsWithBody Bool, IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN

DECLARE  SqlEnd VARCHAR(200);
DECLARE nb INT;
DECLARE SearchNum INT;
DECLARE SearchStr VARCHAR(200);
Set SearchStr = UserSearch;
Set @NumRows = 0;
Set SearchNum = IsWithBody | (IsExact << 1) | (IsAnnotated << 2 ) | (IsSolved << 3);
 

SET SqlEnd = ' ORDER BY CreationDate '; 
    
IF (IsWithBody = true) then
	 IF (IsExact = true) then
		Set SqlEnd = CONCAT(' AND Title LIKE ''%', UserSearch, '%'' OR Body LIKE ', '''%', UserSearch, '%''', 'AND Title IS NOT NULL',SqlEnd);
		END IF;
	Set UserSearch = CONCAT('MATCH(Title, Body) AGAINST (''',UserSearch,''')');
ELSE
	IF (IsExact = false) then
		Set UserSearch = CONCAT('Title LIKE ', CONCAT('''','%', REPLACE(UserSearch,' ', '%'), '%', ''''));
	ELSE
		Set UserSearch = CONCAT('Title LIKE ', CONCAT('''','%', UserSearch, '%', ''''));
	END IF;
END IF;
    
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

 IF (IsAnnotated = true) then
	Set SqlEnd = CONCAT(' AND postannotation.Annotation IS NOT NULL ', SqlENd);
	END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''Yes''', ', ', '''No''',') AS ','''Validated answer''',
				', users.Name AS Author, DATE_FORMAT(CreationDate,','''%d-%m-%Y''',') AS CreationDate, postannotation.Favourite, IF(postannotation.Annotation IS NULL,', '''No''',', ', '''Yes''',') AS Annotations, PostId
                FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND ',UserSearch, SqlEnd); 

SELECT Id into nb FROM searchhistory WHERE SearchString = SearchStr;


    PREPARE stmt FROM @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;
    
IF (nb = 0 || nb IS null) then
	INSERT INTO searchhistory (SearchString, Date,ResultsNumber, SearchType) VALUE(SearchStr, CURRENT_TIMESTAMP(), @NumRows, SearchNum);
ELSE
	UPDATE searchhistory SET Date = CURRENT_TIMESTAMP() WHERE Id = nb;
END IF;

    DEALLOCATE PREPARE stmt;
END
