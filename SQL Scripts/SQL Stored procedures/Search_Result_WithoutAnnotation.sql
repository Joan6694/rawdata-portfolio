CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_WithoutAnnotationModel`(IN UserSearch VARCHAR(200), IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN

DECLARE  SqlEnd VARCHAR(200);

SET SqlEnd = 'ORDER BY CreationDate'; 

SET UserSearch = CONCAT('''','%', UserSearch, '%', '''');

IF (IsExact = false) then
	Set UserSearch = Replace(UserSearch, ' ', '%');
    END IF;
    
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

-- IF (IsAnnotated = true) then
		
--    END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''yes''', ', ', '''no''',') AS ','''Validated answer''',
				', Users.Name AS Author, CreationDate FROM Posts JOIN Users ON  Users.Id = Posts.OwnerUserId WHERE title LIKE ', UserSearch, SqlEnd); 
 
    PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END