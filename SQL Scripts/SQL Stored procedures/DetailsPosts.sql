CREATE DEFINER=`root`@`localhost` PROCEDURE `DetailsPosts`(IN SearchPostId INT)
BEGIN

SELECT Title, Body, Score, CreationDate, AcceptedAnswerId, If(OwnerUserId IS NULL, 'Anonymous', Users.Name) AS Username, CommentCount, Favourite, Annotation from posts AS P 
LEFT JOIN postannotation AS PA ON PA.PostId = P.Id LEFT JOIN Users ON OwnerUserId = Users.Id
WHERE  P.Id = SearchPostId  OR ParentId = SearchPostId ORDER BY CreationDate Asc;

END
