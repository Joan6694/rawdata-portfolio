CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_FavouritesPost`(IN UserSearch VARCHAR(200), IN IsWithBody Bool, IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN


DECLARE  SqlEnd VARCHAR(200);

SET SqlEnd = ' ORDER BY CreationDate '; 

IF (UserSearch = '') then
	SET UserSearch = ' ';
ELSE
	IF (IsWithBody = true) then
		IF (IsExact = true) then
			Set SqlEnd = CONCAT(' AND Title LIKE ''%', UserSearch, '%'' OR Body LIKE ', '''%', UserSearch, '%''', ' AND Title IS NOT NULL ', SqlEnd);
			END IF;
		Set UserSearch = CONCAT(' AND MATCH(Title, Body) AGAINST (''',UserSearch,''')');
	ELSE
		IF (IsExact = false) then
			Set UserSearch = CONCAT(' AND Title LIKE ', CONCAT('''','%', REPLACE(UserSearch,' ', '%'), '%', ''''));
		ELSE
			Set UserSearch = CONCAT(' AND Title LIKE ', CONCAT('''','%', UserSearch, '%', ''''));
		END IF;
	END IF;
END IF;
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

 IF (IsAnnotated = true) then
	Set SqlEnd = CONCAT(' AND postannotation.Annotation IS NOT NULL ', SqlENd);
	END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''Yes''', ', ', '''No''',') AS ','''Validated answer''',
				', Users.Name AS Author, CreationDate, IF(postannotation.Annotation IS NULL,', '''No''',', ', '''Yes''',') AS Annotations, PostId
                FROM Posts JOIN Users ON  Users.Id = Posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = Posts.Id 
                WHERE Title IS NOT NULL AND postannotation.Favourite = 1  ',UserSearch, SqlEnd); 
 
    PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END
