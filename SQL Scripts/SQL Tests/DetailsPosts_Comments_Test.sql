SET @NumErrors = 0;

/******************* 1 ********************/
CALL stackoverflow_model.DetailsPosts_Comments(666820);
SELECT @NumRows INTO @NumResultsPost666820Procedure;

SELECT COUNT(*) FROM posts AS P 
LEFT JOIN Users ON OwnerUserId = Users.Id JOIN Comments C on P.Id = C.PostId
WHERE P.Id = 666820 OR ParentId = 666820 ORDER BY P.Id , C.CreationDate Asc
INTO @NumResultsPost666820Query;

SELECT IF(
@NumResultsPost666820Procedure = @NumResultsPost666820Query, 
'The function works correctly with post 666820',
'The function doesn\'t work correctly with post 666820'
);

/******************* 2 ********************/
CALL stackoverflow_model.DetailsPosts_Comments(142816);
SELECT @NumRows INTO @NumResultsPost142816Procedure;

SELECT COUNT(*) FROM posts AS P 
LEFT JOIN Users ON OwnerUserId = Users.Id JOIN Comments C on P.Id = C.PostId
WHERE P.Id = 142816 OR ParentId = 142816 ORDER BY P.Id , C.CreationDate Asc
INTO @NumResultsPost142816Query;

SELECT IF(
@NumResultsPost142816Procedure = @NumResultsPost142816Query, 
'The function works correctly with post 142816',
'The function doesn\'t work correctly with post 142816'
);