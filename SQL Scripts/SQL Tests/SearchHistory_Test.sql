/******************* 1 ********************/
CALL stackoverflow_model.SearchHistory('SQL Server', -1);
SELECT @NumRows INTO @NumResultsMinusOneProcedure;
SELECT COUNT(*) FROM searchhistory 
				WHERE MATCH(SearchString) AGAINST('SQL Server') AND Mark >= '-1'
				INTO @NumResultsMinusOneQuery;

SELECT @NumResultsMinusOneProcedure, @NumResultsMinusOneQuery;
SELECT IF(
@NumResultsMinusOneProcedure = @NumResultsMinusOneQuery, 
'The function works correctly with the mark as minus one',
'The function doesn\'t work correctly with the mark as minus one'
);

/******************* 2 ********************/
CALL stackoverflow_model.SearchHistory('SQL Server', 0);
SELECT @NumRows INTO @NumResultsZeroProcedure;
SELECT COUNT(*) FROM searchhistory 
				WHERE MATCH(SearchString) AGAINST('SQL Server') AND Mark >= '0'
				INTO @NumResultsZeroQuery;

SELECT @NumResultsZeroProcedure, @NumResultsZeroQuery;
SELECT IF(
@NumResultsZeroProcedure = @NumResultsZeroQuery, 
'The function works correctly with the mark as zero',
'The function doesn\'t work correctly with the mark as zero'
);

/******************* 3 ********************/
CALL stackoverflow_model.SearchHistory('SQL Server', 1);
SELECT @NumRows INTO @NumResultsOneProcedure;
SELECT COUNT(*) FROM searchhistory 
				WHERE MATCH(SearchString) AGAINST('SQL Server') AND Mark >= '1'
				INTO @NumResultsOneQuery;

SELECT @NumResultsOneProcedure, @NumResultsOneQuery;
SELECT IF(
@NumResultsOneProcedure = @NumResultsOneQuery, 
'The function works correctly with the mark as one',
'The function doesn\'t work correctly with the mark as one'
);