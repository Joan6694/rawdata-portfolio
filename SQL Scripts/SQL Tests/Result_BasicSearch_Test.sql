/******************* 1 ********************/
CALL stackoverflow_model.Result_BasicSearch('SQL Server', false, false, false, false);
SELECT @NumRows INTO @NumResultsAllFalseProcedure;
SELECT COUNT(*) FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND Title LIKE '%SQL%Server%' ORDER BY CreationDate
                INTO @NumResultsAllFalseQuery;
SELECT IF(
@NumResultsAllFalseProcedure = @NumResultsAllFalseQuery, 
'The function works correctly with all the parameters as false',
'The function doesn\'t work correctly with all the parameters as false'
);

/******************* 2 ********************/
CALL stackoverflow_model.Result_BasicSearch('SQL Server', true, false, false, false);
SELECT @NumRows INTO @NumResultsIsWithBodyProcedure;
SELECT COUNT(*) FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND MATCH(Title,Body) AGAINST('SQL Server') ORDER BY CreationDate
                INTO @NumResultsIsWithBodyQuery;
SELECT IF(
@NumResultsIsWithBodyProcedure = @NumResultsIsWithBodyQuery, 
'The function works correctly with IsWithBody as true',
'The function doesn\'t work correctly with IsWithBody as true'
);


/******************* 3 ********************/
CALL stackoverflow_model.Result_BasicSearch('SQL Server', true, true, false, false);
SELECT @NumRows INTO @NumResultsIsExactProcedure;
SELECT COUNT(*) FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND MATCH(Title, Body) AGAINST('SQL Server') 
                AND (Title LIKE '%SQL Server%' OR Body LIKE '%SQL Server%') ORDER BY CreationDate 
                INTO @NumResultsIsExactQuery;
SELECT IF(
@NumResultsIsExactProcedure = @NumResultsIsExactQuery, 
'The function works correctly with IsExact as true',
'The function doesn\'t work correctly with IsExact as true'
);

/******************* 4 ********************/
CALL stackoverflow_model.Result_BasicSearch('SQL Server', true, false, true, false);
SELECT @NumRows INTO @NumResultsIsAnnotatedProcedure;
SELECT COUNT(*) FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND MATCH(Title,Body) AGAINST('SQL Server') AND postannotation.Annotation IS NOT NULL 
                ORDER BY CreationDate
                INTO @NumResultsIsAnnotatedQuery;
SELECT IF(
@NumResultsIsAnnotatedProcedure = @NumResultsIsAnnotatedQuery, 
'The function works correctly with IsAnnotated as true',
'The function doesn\'t work correctly with IsAnnotated as true'
);

/******************* 5 ********************/
CALL stackoverflow_model.Result_BasicSearch('SQL Server', true, false, false, true);
SELECT @NumRows INTO @NumResultsIsSolvedProcedure;
SELECT COUNT(*) FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND MATCH(Title,Body) AGAINST('SQL Server') AND AcceptedAnswerId IS NOT NULL 
                ORDER BY CreationDate
                INTO @NumResultsIsSolvedQuery;
SELECT IF(
@NumResultsIsSolvedProcedure = @NumResultsIsSolvedQuery, 
'The function works correctly with IsSolved as true',
'The function doesn\'t work correctly with IsSolved as true'
);
