SET @NumErrors = 0;

/******************* 1 ********************/
CALL stackoverflow_model.DetailsPosts(142816);

SELECT @NumRows INTO @NumResultsPost142816Procedure;
SELECT COUNT(*) from posts AS P 
LEFT JOIN postannotation AS PA ON PA.PostId = P.Id LEFT JOIN Users ON OwnerUserId = Users.Id
WHERE P.Id = 142816 OR ParentId = 142816 ORDER BY CreationDate Asc into @NumResultsPost142816Query;

SELECT IF(
@NumResultsPost142816Procedure = @NumResultsPost142816Query, 
'The function works correctly with post 142816',
'The function doesn\'t work correctly with post 142816'
);

/******************* 2 ********************/
CALL stackoverflow_model.DetailsPosts(45325);

SELECT @NumRows INTO @NumResultsPost45325Procedure;
SELECT COUNT(*) from posts AS P 
LEFT JOIN postannotation AS PA ON PA.PostId = P.Id LEFT JOIN Users ON OwnerUserId = Users.Id
WHERE P.Id = 45325 OR ParentId = 45325 ORDER BY CreationDate Asc into @NumResultsPost45325Query;

SELECT IF(
@NumResultsPost45325Procedure = @NumResultsPost45325Query, 
'The function works correctly with post 45325',
'The function doesn\'t work correctly with post 45325'
);