-- -----------------------------------------------------
-- Schema stackoverflow_model
-- -----------------------------------------------------
USE `stackoverflow_model`;

SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- INSERTIONS INTO QA-MODEL
-- -----------------------------------------------------
INSERT INTO `stackoverflow_model`.`users` (
SELECT `stackoverflow_sample`.`users`.`Id`, 
`stackoverflow_sample`.`users`.`DisplayName`,
`stackoverflow_sample`.`users`.`UpVotes`,
`stackoverflow_sample`.`users`.`DownVotes`
FROM `stackoverflow_sample`.`users`);

INSERT INTO `stackoverflow_model`.`posts` (
SELECT `stackoverflow_sample`.`posts`.`Id`,
`stackoverflow_sample`.`posts`.`PostTypeId`,
`stackoverflow_sample`.`posts`.`ParentId`,
`stackoverflow_sample`.`posts`.`AcceptedAnswerId`,
`stackoverflow_sample`.`posts`.`CreationDate`,
`stackoverflow_sample`.`posts`.`Score`,
`stackoverflow_sample`.`posts`.`ViewCount`,
`stackoverflow_sample`.`posts`.`Body`,
`stackoverflow_sample`.`posts`.`OwnerUserId`,
`stackoverflow_sample`.`posts`.`Title`,
`stackoverflow_sample`.`posts`.`Tags`,
`stackoverflow_sample`.`posts`.`AnswerCount`,
`stackoverflow_sample`.`posts`.`CommentCount`
 FROM `stackoverflow_sample`.`posts`);

INSERT INTO `stackoverflow_model`.`comments` (
SELECT `stackoverflow_sample`.`comments`.`Id`,
`stackoverflow_sample`.`comments`.`PostId`,
`stackoverflow_sample`.`comments`.`Score`,
`stackoverflow_sample`.`comments`.`Text`,
`stackoverflow_sample`.`comments`.`CreationDate`,
`stackoverflow_sample`.`comments`.`UserId`
 FROM `stackoverflow_sample`.`comments`);

INSERT INTO `stackoverflow_model`.`postlinks` (
SELECT `stackoverflow_sample`.`postlinks`.`Id`,
`stackoverflow_sample`.`postlinks`.`CreationDate`,
`stackoverflow_sample`.`postlinks`.`PostId`,
`stackoverflow_sample`.`postlinks`.`RelatedPostId`,
`stackoverflow_sample`.`postlinks`.`PostLinkTypeId`
 FROM `stackoverflow_sample`.`postlinks`);

INSERT INTO `stackoverflow_model`.`tags` (
SELECT `stackoverflow_sample`.`tags`.`Id`,
`stackoverflow_sample`.`tags`.`tagName`,
`stackoverflow_sample`.`tags`.`Count`
 FROM `stackoverflow_sample`.`tags`);

INSERT INTO `stackoverflow_model`.`ti` (
SELECT `stackoverflow_sample`.`ti`.`pid`,
`stackoverflow_sample`.`ti`.`tid`
 FROM `stackoverflow_sample`.`ti`);

INSERT INTO `stackoverflow_model`.`votes` (
SELECT `stackoverflow_sample`.`votes`.`Id`,
`stackoverflow_sample`.`votes`.`PostId`,
`stackoverflow_sample`.`votes`.`VoteTypeId`,
`stackoverflow_sample`.`votes`.`CreationDate`,
`stackoverflow_sample`.`votes`.`UserId`
 FROM `stackoverflow_sample`.`votes`);

-- -----------------------------------------------------
-- INSERTION INTO ANNOTATION-MODEL
-- -----------------------------------------------------
TRUNCATE TABLE `stackoverflow_model`.`postannotation`;
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('7664', true, "Integrate C++ code BSD Project");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('263952', true, "Make eclipse output stdout");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('45325', false, "Visual Studio control error");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('85553', true, "MSDN struct");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('105568', false, "Best RTOS at Windows");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('105975', true, "QNX at Windows (VMWare)");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('559839', false, "Teaching Java crash course");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('840190', true, "Java change working directory programatically");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('645840', false, "Javascript web project");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('1310378', true, "Java applet image resize");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('543515', false, "Structs & Classes at C++");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('5117171', true, "C++ solid ascii block");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('951791', false, "Javascript global error handling");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('7178137', false, "Java RMI code dev errors");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('5878536', true, "Remove leading number labels Javascript");

TRUNCATE TABLE `stackoverflow_model`.`searchhistory`;
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('01', 'Java error outofbounds', '2013-08-30 19:05:00', '0', '50', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('02', 'Javascript not running', '2013-08-30 19:05:01', '1', '0', '2');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('03', 'C++ struct explaination', '2013-08-30 19:05:02', '-1', '10', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('04', 'C sucks', '2013-08-30 19:05:03', '0', '50', '2');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('05', 'What is the difference between PC and Mac', '2020-08-30 19:05:09', '1', '500', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('06', 'I still do not understand Java exceptions', '2013-08-30 19:06:04', '-1', '20', '2');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('07', 'Java error stack overflow', '2013-09-02 19:07:05', '0', '25', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('08', 'searchedString8', '2000-09-02 19:04:59', '1', '2', '2');

SET FOREIGN_KEY_CHECKS=1;
