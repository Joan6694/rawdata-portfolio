-- -----------------------------------------------------
-- Schema stackoverflow_model
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `stackoverflow_model`;
CREATE SCHEMA IF NOT EXISTS `stackoverflow_model`;
USE `stackoverflow_model`;


SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- QA-MODEL
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `stackoverflow_model`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`users` (
  `Id` INT(11) UNSIGNED,
  `Name` TEXT,
  `UpVotes` INT(11) UNSIGNED ,
  `DownVotes` INT(11) UNSIGNED ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`posts` (
  `Id` INT(11) UNSIGNED,
  `PostTypeId` INT(11) ,
  `ParentId` INT(11) UNSIGNED,
  `AcceptedAnswerId` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `Score` INT(11) ,
  `ViewCount` INT(11) UNSIGNED,
  `Body` LONGTEXT ,
  `OwnerUserId` INT(11) UNSIGNED,
  `Title` MEDIUMTEXT ,
  `Tags` MEDIUMTEXT ,
  `AnswerCount` INT(11) UNSIGNED,
  `CommentCount` INT(11) UNSIGNED,
  INDEX `fk_posts_parentId_idx` (`ParentId` ASC),
  CONSTRAINT `fk_posts_users1`
    FOREIGN KEY (`OwnerUserId`)
    REFERENCES `stackoverflow_model`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posts_posts1`
    FOREIGN KEY (`AcceptedAnswerId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posts_posts2`
    FOREIGN KEY (`ParentId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`comments` (
  `Id` INT(11) UNSIGNED,
  `PostId` INT(11) UNSIGNED,
  `Score` INT(11) ,
  `Text` LONGTEXT ,
  `CreationDate` DATETIME ,
  `UserId` INT(11) UNSIGNED,
  INDEX `fk_comments_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_comments_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_model`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`postlinks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`postlinks` (
  `Id` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `PostId` INT(11) UNSIGNED,
  `RelatedPostId` INT(11) UNSIGNED,
  `PostLinkTypeId` INT(11) UNSIGNED,
  INDEX `fk_postlinks_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_postlinks_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_postlinks_posts2`
    FOREIGN KEY (`RelatedPostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`tags` (
  `Id` INT(11) UNSIGNED,
  `TagName` LONGTEXT ,
  `Count` BIGINT(21) UNSIGNED NOT NULL DEFAULT '0' ,
    PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`ti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`ti` (
  `PostId` INT(11) UNSIGNED,
  `TagId` INT(11) UNSIGNED,
  CONSTRAINT `fk_ti_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ti_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `stackoverflow_model`.`tags` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`PostId`,`TagId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`votes` (
  `Id` INT(11) UNSIGNED,
  `PostId` INT(11) UNSIGNED,
  `VoteTypeId` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `UserId` INT(11) UNSIGNED,
  INDEX `fk_votes_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_votes_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_votes_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_model`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- INSERTIONS INTO QA-MODEL
-- -----------------------------------------------------
insert into `stackoverflow_model`.`users` (
select `stackoverflow_sample`.`users`.`Id`, 
`stackoverflow_sample`.`users`.`DisplayName`,
`stackoverflow_sample`.`users`.`UpVotes`,
`stackoverflow_sample`.`users`.`DownVotes`
from `stackoverflow_sample`.`users`);

insert into `stackoverflow_model`.`posts` (
select `stackoverflow_sample`.`posts`.`Id`,
`stackoverflow_sample`.`posts`.`PostTypeId`,
`stackoverflow_sample`.`posts`.`ParentId`,
`stackoverflow_sample`.`posts`.`AcceptedAnswerId`,
`stackoverflow_sample`.`posts`.`CreationDate`,
`stackoverflow_sample`.`posts`.`Score`,
`stackoverflow_sample`.`posts`.`ViewCount`,
`stackoverflow_sample`.`posts`.`Body`,
`stackoverflow_sample`.`posts`.`OwnerUserId`,
`stackoverflow_sample`.`posts`.`Title`,
`stackoverflow_sample`.`posts`.`Tags`,
`stackoverflow_sample`.`posts`.`AnswerCount`,
`stackoverflow_sample`.`posts`.`CommentCount`
 from `stackoverflow_sample`.`posts`);

insert into `stackoverflow_model`.`comments` (
select `stackoverflow_sample`.`comments`.`Id`,
`stackoverflow_sample`.`comments`.`PostId`,
`stackoverflow_sample`.`comments`.`Score`,
`stackoverflow_sample`.`comments`.`Text`,
`stackoverflow_sample`.`comments`.`CreationDate`,
`stackoverflow_sample`.`comments`.`UserId`
 from `stackoverflow_sample`.`comments`);

insert into `stackoverflow_model`.`postlinks` (
select `stackoverflow_sample`.`postlinks`.`Id`,
`stackoverflow_sample`.`postlinks`.`CreationDate`,
`stackoverflow_sample`.`postlinks`.`PostId`,
`stackoverflow_sample`.`postlinks`.`RelatedPostId`,
`stackoverflow_sample`.`postlinks`.`PostLinkTypeId`
 from `stackoverflow_sample`.`postlinks`);

insert into `stackoverflow_model`.`tags` (
select `stackoverflow_sample`.`tags`.`Id`,
`stackoverflow_sample`.`tags`.`tagName`,
`stackoverflow_sample`.`tags`.`Count`
 from `stackoverflow_sample`.`tags`);

insert into `stackoverflow_model`.`ti` (
select `stackoverflow_sample`.`ti`.`pid`,
`stackoverflow_sample`.`ti`.`tid`
 from `stackoverflow_sample`.`ti`);

insert into `stackoverflow_model`.`votes` (
select `stackoverflow_sample`.`votes`.`Id`,
`stackoverflow_sample`.`votes`.`PostId`,
`stackoverflow_sample`.`votes`.`VoteTypeId`,
`stackoverflow_sample`.`votes`.`CreationDate`,
`stackoverflow_sample`.`votes`.`UserId`
 from `stackoverflow_sample`.`votes`);


-- -----------------------------------------------------
-- ANNOTATION MODEL
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `stackoverflow_model`.`searchhistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`searchhistory` (
  `Id` INT(11) UNSIGNED AUTO_INCREMENT,
  `SearchString` VARCHAR(255),
  `Date` DATETIME,
  `Mark` INT(1) DEFAULT 0,
  `ResultsNumber` INT UNSIGNED,
  `SearchType` INT(1) UNSIGNED,
  PRIMARY KEY (`Id`) )
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`postannotation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`postannotation` (
  `PostId` INT(11) UNSIGNED,
  `Favourite` BOOLEAN,
  `Annotation` TEXT,
  CONSTRAINT `fk_postAnnotation_postAnnotation1`
  FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  PRIMARY KEY (`postId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- INSERTION INTO ANNOTATION-MODEL
-- -----------------------------------------------------
TRUNCATE TABLE `stackoverflow_model`.`postannotation`;

INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('7664', true, "Integrate C++ code BSD Project");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('45325', false, "Visual Studio control error");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('85553', true, "MSDN struct");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('105568', false, "Best RTOS at Windows");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('105975', true, "QNX at Windows (VMWare)");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('559839', false, "Teaching Java crash course");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('840190', true, "Java change working directory programatically");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('645840', false, "Javascript web project");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('1310378', true, "Java applet image resize");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('543515', false, "Structs & Classes at C++");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('5117171', true, "C++ solid ascii block");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('951791', false, "Javascript global error handling");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('7178137', false, "Java RMI code dev errors");
INSERT INTO `stackoverflow_model`.`postannotation`
VALUES('5878536', true, "Remove leading number labels Javascript");


TRUNCATE TABLE `stackoverflow_model`.`searchhistory`;

INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('01', 'Java error outofbounds', '2013-08-30 19:05:00', '0', '50', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('02', 'Javascript not running', '2013-08-30 19:05:01', '1', '0', '2');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('03', 'C++ struct explaination', '2013-08-30 19:05:02', '-1', '10', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('04', 'C sucks', '2013-08-30 19:05:03', '0', '50', '2');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('05', 'What is the difference between PC and Mac', '2020-08-30 19:05:09', '1', '500', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('06', 'I still do not understand Java exceptions', '2013-08-30 19:06:04', '-1', '20', '2');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('07', 'Java error stack overflow', '2013-09-02 19:07:05', '0', '25', '1');
INSERT INTO `stackoverflow_model`.`searchhistory`
VALUES('08', 'searchedString8', '2000-09-02 19:04:59', '1', '2', '2');


-- -----------------------------------------------------
-- ADDITION OF FULLTEXT INDEXES
-- -----------------------------------------------------

ALTER TABLE posts ADD FULLTEXT(Body, title);
ALTER TABLE searchhistory ADD FULLTEXT(SearchString);


SET FOREIGN_KEY_CHECKS=1;


-- -----------------------------------------------------
-- STORED PROCEDURES
-- -----------------------------------------------------

drop procedure if exists `DetailsPosts`;
drop procedure if exists `DetailsPosts_Comments`;
drop procedure if exists `Result_BasicSearch`;
drop procedure if exists `Result_FavouritesPost`;
drop procedure if exists `Result_WithoutAnnotationModel`;
drop procedure if exists `SearchHistory`;
drop procedure if exists `InsertAnnotation`;
drop procedure if exists `UpdateAnnotation`;


-- -----------------------------------------------------
-- DetailsPosts
-- -----------------------------------------------------
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DetailsPosts`(IN SearchPostId INT)
BEGIN

SELECT Title, Body, Score, CreationDate, AcceptedAnswerId, If(OwnerUserId IS NULL, 'Anonymous', users.Name) AS Username,
 CommentCount, IF(PA.Favourite IS NULL,0, PA.Favourite) AS Favourite, P.Id AS 'Id', Annotation from posts AS P 
LEFT JOIN postannotation AS PA ON PA.PostId = P.Id LEFT JOIN users ON OwnerUserId = users.Id
WHERE  P.Id = SearchPostId  OR ParentId = SearchPostId ORDER BY CreationDate Asc;

END$$
DELIMITER ;

-- -----------------------------------------------------
-- DetailsPosts_Comments
-- -----------------------------------------------------
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DetailsPosts_Comments`(IN SearchPostId INT)
BEGIN


SELECT P.Id AS PostId, C.Text, C.Score, C.CreationDate, If(C.UserId IS NULL, 'Anonymous', users.Name) AS Username FROM  posts AS P  LEFT JOIN users ON OwnerUserId = users.Id  JOIN comments C on P.Id = C.PostId
WHERE  P.Id = SearchPostId  OR ParentId = SearchPostId ORDER BY P.Id , CreationDate Asc;

END$$
DELIMITER ;


-- -----------------------------------------------------
-- Result_BasicSearch
-- -----------------------------------------------------
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_BasicSearch`(IN UserSearch VARCHAR(200), IN IsWithBody Bool, IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN

DECLARE  SqlEnd VARCHAR(200);
DECLARE nb INT;
DECLARE SearchNum INT;
DECLARE SearchStr VARCHAR(200);
Set SearchStr = UserSearch;
Set @NumRows = 0;
Set SearchNum = IsWithBody | (IsExact << 1) | (IsAnnotated << 2 ) | (IsSolved << 3);
 

SET SqlEnd = ' ORDER BY CreationDate '; 
    
IF (IsWithBody = true) then
	 IF (IsExact = true) then
		Set SqlEnd = CONCAT(' AND Title LIKE ''%', UserSearch, '%'' OR Body LIKE ', '''%', UserSearch, '%''', 'AND Title IS NOT NULL',SqlEnd);
		END IF;
	Set UserSearch = CONCAT('MATCH(Title, Body) AGAINST (''',UserSearch,''')');
ELSE
	IF (IsExact = false) then
		Set UserSearch = CONCAT('Title LIKE ', CONCAT('''','%', REPLACE(UserSearch,' ', '%'), '%', ''''));
	ELSE
		Set UserSearch = CONCAT('Title LIKE ', CONCAT('''','%', UserSearch, '%', ''''));
	END IF;
END IF;
    
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

 IF (IsAnnotated = true) then
	Set SqlEnd = CONCAT(' AND postannotation.Annotation IS NOT NULL ', SqlENd);
	END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''Yes''', ', ', '''No''',') AS ','''ValidatedAnswer''',
				', users.Name AS Author, DATE_FORMAT(CreationDate,','''%d-%m-%Y''',') AS CreationDate, postannotation.Favourite, IF(postannotation.Annotation IS NULL,', '''No''',', ', '''Yes''',') AS Annotations, PostId
                FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND ',UserSearch, SqlEnd); 

SELECT Id into nb FROM searchhistory WHERE SearchString = SearchStr;


    PREPARE stmt FROM @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;
    
IF (nb = 0 || nb IS null) then
	INSERT INTO searchhistory (SearchString, Date,ResultsNumber, SearchType) VALUE(SearchStr, CURRENT_TIMESTAMP(), @NumRows, SearchNum);
ELSE
	UPDATE searchhistory SET Date = CURRENT_TIMESTAMP() WHERE Id = nb;
END IF;

    DEALLOCATE PREPARE stmt;
END$$
DELIMITER ;



-- -----------------------------------------------------
-- Result_FavouritesPost
-- -----------------------------------------------------
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_FavouritesPost`(IN UserSearch VARCHAR(200), IN IsWithBody Bool, IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN


DECLARE  SqlEnd VARCHAR(200);

SET SqlEnd = ' ORDER BY CreationDate '; 

IF (UserSearch = '') then
	SET UserSearch = ' ';
ELSE
	IF (IsWithBody = true) then
		IF (IsExact = true) then
			Set SqlEnd = CONCAT(' AND Title LIKE ''', UserSearch, ''' OR Body LIKE ', '''%', UserSearch, '%''', ' AND Title IS NOT NULL ', SqlEnd);
			END IF;
		Set UserSearch = CONCAT(' AND MATCH(Title, Body) AGAINST (''',UserSearch,''')');
	ELSE
		IF (IsExact = false) then
			Set UserSearch = CONCAT(' AND Title LIKE ', CONCAT('''','%', REPLACE(UserSearch,' ', '%'), '%', ''''));
		ELSE
			Set UserSearch = CONCAT(' AND Title LIKE ', CONCAT('''','%', UserSearch, '%', ''''));
		END IF;
	END IF;
END IF;
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

 IF (IsAnnotated = true) then
	Set SqlEnd = CONCAT(' AND postannotation.Annotation IS NOT NULL ', SqlENd);
	END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''Yes''', ', ', '''No''',') AS ','''Validated answer''',
				', users.Name AS Author, CreationDate, IF(postannotation.Annotation IS NULL,', '''No''',', ', '''Yes''',') AS Annotations, PostId
                FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND postannotation.Favourite = 1  ',UserSearch, SqlEnd); 
 
    PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END$$
DELIMITER ;


-- -----------------------------------------------------
-- Result_WithoutAnnotationModel
-- -----------------------------------------------------
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_WithoutAnnotationModel`(IN UserSearch VARCHAR(200), IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN

DECLARE  SqlEnd VARCHAR(200);

SET SqlEnd = 'ORDER BY CreationDate'; 

SET UserSearch = CONCAT('''','%', UserSearch, '%', '''');

IF (IsExact = false) then
	Set UserSearch = Replace(UserSearch, ' ', '%');
    END IF;
    
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

-- IF (IsAnnotated = true) then
		
--    END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''yes''', ', ', '''no''',') AS ','''Validated answer''',
				', users.Name AS Author, CreationDate FROM posts JOIN users ON  users.Id = posts.OwnerUserId WHERE title LIKE ', UserSearch, SqlEnd); 
 
    PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END$$
DELIMITER ;


-- -----------------------------------------------------
-- SearchHistory
-- -----------------------------------------------------
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SearchHistory`(IN UserSearch VARCHAR(200), IN Marked INT)
BEGIN

SET @s = CONCAT('SELECT SearchString, Mark, Date, ResultsNumber, Id FROM searchhistory');

IF (UserSearch != '') THEN
	SET @s = CONCAT(@s,' WHERE MATCH(SearchString) AGAINST(''', UserSearch,''') ');
    END IF;

IF (Marked = true) THEN
	IF (UserSearch = '') THEN
		SET @s = CONCAT(@s, ' WHERE ');
	ELSE
		SET @s = CONCAT(@s, ' AND ');
	END IF;
	SET @s = CONCAT(@s,  ' Mark >= ',  Marked);
END IF;

SET @s = CONCAT(@s, ' ;');

PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END$$
DELIMITER ;


-- -----------------------------------------------------
-- Insert Annotation
-- -----------------------------------------------------
DELIMITER $$
USE `stackoverflow_model`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Insert_Annotation`(IN pid int, IN fav Bool, IN anno VARCHAR(4096))
BEGIN

Set @NumRows = 0;
SET @s = CONCAT('SELECT Id FROM posts WHERE Id =',pid,';');


PREPARE stmt FROM @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;

IF (@NumRows > 0 || @NumRows is not null) THEN
		INSERT INTO postannotation (PostId,Favourite, Annotation) VALUE(pid, fav, anno);
	END IF;

	DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;


-- -----------------------------------------------------
-- Update Annotation
-- -----------------------------------------------------
DELIMITER $$
USE `stackoverflow_model`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Update_Annotation`(IN pid int, IN fav Bool, IN anno VARCHAR(4096))
BEGIN

Set @NumRows = 0;
SET @s = CONCAT('SELECT PostId FROM postannotation WHERE PostId =',pid,';');


PREPARE stmt FROM @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;

IF (@NumRows > 0 || @NumRows is not null) THEN
		SET @MyUp = CONCAT('UPDATE postannotation SET Favourite=',fav,' , Annotation=''',anno,''' WHERE PostId=',pid);
        PREPARE DoUpdate FROM @MyUp;
        EXECUTE DoUpdate;
       	DEALLOCATE PREPARE DoUpdate;
ELSE
		CALL Insert_Annotation(@pid, @fav, @anno); 
	END IF;
		
	DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;

