-- -----------------------------------------------------
-- Schema stackoverflow_test
-- -----------------------------------------------------
drop schema if exists `stackoverflow_test`;
CREATE SCHEMA IF NOT EXISTS `stackoverflow_test`;
USE `stackoverflow_test`;

SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- Table `stackoverflow_test`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`users` (
  `Id` INT(11) ,
  -- `Reputation` INT(11) ,
  -- `DisplayName` TEXT ,
  `Name` TEXT,
  -- `CreationDate` DATETIME ,
  -- `LastAccessDate` DATETIME ,
  -- `WebsiteUrl` TEXT ,
  -- `Location` TEXT ,
  -- `AboutMe` LONGTEXT ,
  -- `ProfileImageUrl` TEXT ,
  -- `Views` INT(11) ,
  `UpVotes` INT(11) ,
  `DownVotes` INT(11) ,
  -- `Age` INT(11) ,
  -- `AccountId` INT(11) ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_test`.`badges`
-- -----------------------------------------------------
/*
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`badges` (
  `Id` INT(11) ,
  `UserId` INT(11) ,
  `Name` TEXT ,
  `Date` DATETIME ,
  INDEX `fk_badges_users_idx` (`UserId` ASC) ,
  CONSTRAINT `fk_badges_users`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_test`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
    )
ENGINE = InnoDB;
*/

-- -----------------------------------------------------
-- Table `stackoverflow_test`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`posts` (
  `Id` INT(11) ,
  `PostTypeId` INT(11) ,
  `ParentId` INT(11) ,
  `AcceptedAnswerId` INT(11) ,
  `CreationDate` DATETIME ,
  `Score` INT(11) ,
  `ViewCount` INT(11) ,
  `Body` LONGTEXT ,
  `OwnerUserId` INT(11) ,
  -- `LastEditorUserId` INT(11) ,
  -- `LastEditorDisplayName` INT(11) ,
  -- `LastEditDate` DATETIME ,
  -- `LastActivityDate` DATETIME ,
  -- `CommunityOwnedDate` DATETIME ,
  -- `ClosedDate` DATETIME ,
  `Title` MEDIUMTEXT ,
  `Tags` MEDIUMTEXT ,
  `AnswerCount` INT(11) ,
  `CommentCount` INT(11) ,
  -- `FavoriteCount` INT(11) ,
  CONSTRAINT `fk_posts_users1`
    FOREIGN KEY (`OwnerUserId`)
    REFERENCES `stackoverflow_test`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
 /* CONSTRAINT `fk_posts_users2`
    FOREIGN KEY (`LastEditorUserId`)
    REFERENCES `stackoverflow_test`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,*/
  CONSTRAINT `fk_posts_posts1`
    FOREIGN KEY (`AcceptedAnswerId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posts_posts2`
    FOREIGN KEY (`ParentId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_test`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`comments` (
  `Id` INT(11) ,
  `PostId` INT(11) ,
  `Score` INT(11) ,
  `Text` LONGTEXT ,
  `CreationDate` DATETIME ,
  `UserId` INT(11) ,
  INDEX `fk_comments_posts1_idx` (`PostId` ASC)  ,
  INDEX `fk_comments_users1_idx` (`UserId` ASC)  ,
  CONSTRAINT `fk_comments_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_test`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_test`.`posthistory`
-- -----------------------------------------------------
/*
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`posthistory` (
  `Id` INT(11) ,
  `PostHistoryTypeId` INT(11) ,
  `PostId` INT(11) ,
  `RevisionGUID` TEXT ,
  `CreationDate` DATETIME ,
  `UserId` INT(11) ,
  `UserDisplayName` TEXT ,
  `Comment` LONGTEXT ,
  `Text` LONGTEXT ,
  `CloseReasonId` INT(11) ,
  INDEX `fk_posthistory_posts1_idx` (`PostId` ASC)  ,
  INDEX `fk_posthistory_users1_idx` (`UserId` ASC)  ,
  CONSTRAINT `fk_posthistory_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posthistory_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_test`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;
*/


-- -----------------------------------------------------
-- Table `stackoverflow_test`.`postlinks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`postlinks` (
  `Id` INT(11) ,
  `CreationDate` DATETIME ,
  `PostId` INT(11) ,
  `RelatedPostId` INT(11) ,
  `PostLinkTypeId` INT(11) ,
  INDEX `fk_postlinks_posts1_idx` (`PostId` ASC)  ,
  INDEX `fk_postlinks_posts2_idx` (`RelatedPostId` ASC)  ,
  CONSTRAINT `fk_postlinks_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_postlinks_posts2`
    FOREIGN KEY (`RelatedPostId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_test`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`tags` (
  `Id` INT(11) ,
  `TagName` LONGTEXT ,
  `Count` BIGINT(21) NOT NULL DEFAULT '0' ,
  -- `excerptpostid` INT(11) ,
  -- `wikipostid` INT(11),
    PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_test`.`ti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`ti` (
  `PostId` INT(11) ,
  `TagId` INT(11) ,
  INDEX `fk_ti_posts1_idx` (`PostId` ASC)  ,
  INDEX `fk_ti_tags1_idx` (`TagId` ASC)  ,
  CONSTRAINT `fk_ti_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ti_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `stackoverflow_test`.`tags` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`PostId`,`TagId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_test`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`votes` (
  `Id` INT(11) ,
  `PostId` INT(11) ,
  `VoteTypeId` INT(11) ,
  `CreationDate` DATETIME ,
  `UserId` INT(11) ,
  -- `BountyAmount` INT(11) ,
  INDEX `fk_votes_posts1_idx` (`PostId` ASC)  ,
  INDEX `fk_votes_users1_idx` (`UserId` ASC)  ,
  CONSTRAINT `fk_votes_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_test`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_votes_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_test`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;

/*
-- -----------------------------------------------------
-- Table `stackoverflow_test`.`words`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_test`.`words` (
`Id` INT(11),
`tablename`	TEXT,			
`what` TEXT,	
`sen` INT(11),
`idx` INT(11),
`word` TEXT,
`pos` TEXT,
`lemma` TEXT)
ENGINE = InnoDB;
*/

-- -----------------------------------------------------
-- INSERTIONS
-- -----------------------------------------------------
insert into `stackoverflow_test`.`users` (
select `stackoverflow_sample`.`users`.`Id`, 
`stackoverflow_sample`.`users`.`DisplayName`,
`stackoverflow_sample`.`users`.`UpVotes`,
`stackoverflow_sample`.`users`.`DownVotes`
from `stackoverflow_sample`.`users`);

insert into `stackoverflow_test`.`posts` (
select `stackoverflow_sample`.`posts`.`Id`,
`stackoverflow_sample`.`posts`.`PostTypeId`,
`stackoverflow_sample`.`posts`.`ParentId`,
`stackoverflow_sample`.`posts`.`AcceptedAnswerId`,
`stackoverflow_sample`.`posts`.`CreationDate`,
`stackoverflow_sample`.`posts`.`Score`,
`stackoverflow_sample`.`posts`.`ViewCount`,
`stackoverflow_sample`.`posts`.`Body`,
`stackoverflow_sample`.`posts`.`OwnerUserId`,
`stackoverflow_sample`.`posts`.`Title`,
`stackoverflow_sample`.`posts`.`Tags`,
`stackoverflow_sample`.`posts`.`AnswerCount`,
`stackoverflow_sample`.`posts`.`CommentCount`
 from `stackoverflow_sample`.`posts`);

insert into `stackoverflow_test`.`comments` (
select `stackoverflow_sample`.`comments`.`Id`,
`stackoverflow_sample`.`comments`.`PostId`,
`stackoverflow_sample`.`comments`.`Score`,
`stackoverflow_sample`.`comments`.`Text`,
`stackoverflow_sample`.`comments`.`CreationDate`,
`stackoverflow_sample`.`comments`.`UserId`
 from `stackoverflow_sample`.`comments`);

insert into `stackoverflow_test`.`postlinks` (
select `stackoverflow_sample`.`postlinks`.`Id`,
`stackoverflow_sample`.`postlinks`.`CreationDate`,
`stackoverflow_sample`.`postlinks`.`PostId`,
`stackoverflow_sample`.`postlinks`.`RelatedPostId`,
`stackoverflow_sample`.`postlinks`.`PostLinkTypeId`
 from `stackoverflow_sample`.`postlinks`);

insert into `stackoverflow_test`.`tags` (
select `stackoverflow_sample`.`tags`.`Id`,
`stackoverflow_sample`.`tags`.`tagName`,
`stackoverflow_sample`.`tags`.`Count`
 from `stackoverflow_sample`.`tags`);

insert into `stackoverflow_test`.`ti` (
select `stackoverflow_sample`.`ti`.`pid`,
`stackoverflow_sample`.`ti`.`tid`
 from `stackoverflow_sample`.`ti`);

insert into `stackoverflow_test`.`votes` (
select `stackoverflow_sample`.`votes`.`Id`,
`stackoverflow_sample`.`votes`.`PostId`,
`stackoverflow_sample`.`votes`.`VoteTypeId`,
`stackoverflow_sample`.`votes`.`CreationDate`,
`stackoverflow_sample`.`votes`.`UserId`
 from `stackoverflow_sample`.`votes`);
 
 /*
insert into `stackoverflow_test`.`words` (
select * from `words`.`words`);
*/

-- -----------------------------------------------------
-- INDEXES
-- create index <index-name> on <relation-name> (<attribute-list>);
-- -----------------------------------------------------
/*
CREATE INDEX `index_user_name` on `stackoverflow_test`.`users` (`Name`);

CREATE INDEX `index_post_parentId` on `stackoverflow_test`.`post` (`ParentId`);
CREATE INDEX `index_post_title` on `stackoverflow_test`.`post` (`Title`);
CREATE INDEX `index_post_score` on `stackoverflow_test`.`post` (`Score`);

CREATE INDEX `index_comments_postId` on `stackoverflow_test`.`comments` (`PostId`);
CREATE INDEX `index_comments_score` on `stackoverflow_test`.`comments` (`Score`);

CREATE INDEX `index_postLinks_postId` on `stackoverflow_test`.`postLinks` (`PostId`);

CREATE INDEX `index_votes_postId` on `stackoverflow_test`.`votes` (`PostId`);
CREATE INDEX `index_votes_userId` on `stackoverflow_test`.`votes` (`UserId`);
*/

SET FOREIGN_KEY_CHECKS=1;