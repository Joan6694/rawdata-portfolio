-- -----------------------------------------------------
-- Schema stackoverflow_model
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `stackoverflow_model`;
CREATE SCHEMA IF NOT EXISTS `stackoverflow_model`;
USE `stackoverflow_model`;


SET FOREIGN_KEY_CHECKS=0;


-- -----------------------------------------------------
-- QA-MODEL
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `stackoverflow_model`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`users` (
  `Id` INT(11) UNSIGNED,
  `Name` TEXT,
  `UpVotes` INT(11) UNSIGNED ,
  `DownVotes` INT(11) UNSIGNED ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`posts` (
  `Id` INT(11) UNSIGNED,
  `PostTypeId` INT(11) ,
  `ParentId` INT(11) UNSIGNED,
  `AcceptedAnswerId` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `Score` INT(11) ,
  `ViewCount` INT(11) UNSIGNED,
  `Body` LONGTEXT ,
  `OwnerUserId` INT(11) UNSIGNED,
  `Title` MEDIUMTEXT ,
  `Tags` MEDIUMTEXT ,
  `AnswerCount` INT(11) UNSIGNED,
  `CommentCount` INT(11) UNSIGNED,
  INDEX `fk_posts_parentId_idx` (`ParentId` ASC),
  CONSTRAINT `fk_posts_users1`
    FOREIGN KEY (`OwnerUserId`)
    REFERENCES `stackoverflow_model`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posts_posts1`
    FOREIGN KEY (`AcceptedAnswerId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posts_posts2`
    FOREIGN KEY (`ParentId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`comments` (
  `Id` INT(11) UNSIGNED,
  `PostId` INT(11) UNSIGNED,
  `Score` INT(11) ,
  `Text` LONGTEXT ,
  `CreationDate` DATETIME ,
  `UserId` INT(11) UNSIGNED,
  INDEX `fk_comments_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_comments_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_model`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`postlinks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`postlinks` (
  `Id` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `PostId` INT(11) UNSIGNED,
  `RelatedPostId` INT(11) UNSIGNED,
  `PostLinkTypeId` INT(11) UNSIGNED,
  INDEX `fk_postlinks_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_postlinks_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_postlinks_posts2`
    FOREIGN KEY (`RelatedPostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`tags` (
  `Id` INT(11) UNSIGNED,
  `TagName` LONGTEXT ,
  `Count` BIGINT(21) UNSIGNED NOT NULL DEFAULT '0' ,
    PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`ti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`ti` (
  `PostId` INT(11) UNSIGNED,
  `TagId` INT(11) UNSIGNED,
  CONSTRAINT `fk_ti_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ti_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `stackoverflow_model`.`tags` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`PostId`,`TagId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`votes` (
  `Id` INT(11) UNSIGNED,
  `PostId` INT(11) UNSIGNED,
  `VoteTypeId` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `UserId` INT(11) UNSIGNED,
  INDEX `fk_votes_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_votes_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_votes_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_model`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;



-- -----------------------------------------------------
-- ANNOTATION MODEL
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`searchhistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`searchhistory` (
  `Id` INT(11) UNSIGNED AUTO_INCREMENT,
  `SearchString` VARCHAR(255),
  `Date` DATETIME,
  `Mark` INT(1) DEFAULT 0,
  `ResultsNumber` INT UNSIGNED,
  `SearchType` INT(1) UNSIGNED,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model`.`postannotation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model`.`postannotation` (
  `PostId` INT(11) UNSIGNED,
  `Favourite` BOOLEAN,
  `Annotation` TEXT DEFAULT NULL,
  CONSTRAINT `fk_postAnnotation_postAnnotation1`
  FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  PRIMARY KEY (`postId`) )
ENGINE = InnoDB;


SET FOREIGN_KEY_CHECKS=1;
