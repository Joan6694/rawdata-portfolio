use stackoverflow_model;

drop procedure if exists `DetailsPosts`;
drop procedure if exists `DetailsPosts_Comments`;
drop procedure if exists `Result_BasicSearch`;
drop procedure if exists `Result_FavouritesPost`;
drop procedure if exists `Result_WithoutAnnotationModel`;
drop procedure if exists `SearchHistory`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DetailsPosts`(IN SearchPostId INT)
BEGIN

SELECT Title, Body, Score, CreationDate, AcceptedAnswerId, If(OwnerUserId IS NULL, 'Anonymous', Users.Name) AS Username, CommentCount, Favourite, Annotation from posts AS P 
LEFT JOIN postannotation AS PA ON PA.PostId = P.Id LEFT JOIN Users ON OwnerUserId = Users.Id
WHERE  Id = SearchPostId  OR ParentId = SearchPostId ORDER BY CreationDate Asc;

END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DetailsPosts_Comments`(IN SearchPostId INT)
BEGIN


SELECT P.Id AS PostId, C.Text, C.Score, C.CreationDate, If(C.UserId IS NULL, 'Anonymous', Users.Name) AS Username FROM  posts AS P  LEFT JOIN Users ON OwnerUserId = Users.Id  JOIN Comments C on P.Id = C.PostId
WHERE  P.Id = SearchPostId  OR ParentId = SearchPostId ORDER BY P.Id , CreationDate Asc;

END$$
DELIMITER ;



DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_BasicSearch`(IN UserSearch VARCHAR(200), IN IsWithBody Bool, IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN

DECLARE  SqlEnd VARCHAR(200);
DECLARE nb INT;
DECLARE SearchNum INT;
DECLARE SearchStr VARCHAR(200);
Set SearchStr = UserSearch;
Set @NumRows = 0;
Set SearchNum = IsWithBody | (IsExact << 1) | (IsAnnotated << 2 ) | (IsSolved << 3);
 

SET SqlEnd = ' ORDER BY CreationDate '; 
    
IF (IsWithBody = true) then
	 IF (IsExact = true) then
		Set SqlEnd = CONCAT(' AND Title LIKE ''', UserSearch, ''' OR Body LIKE ', '''%', UserSearch, '%''', ' AND Title IS NOT NULL ', SqlEnd);
		END IF;
	Set UserSearch = CONCAT('MATCH(Title, Body) AGAINST (''',UserSearch,''')');
ELSE
	IF (IsExact = false) then
		Set UserSearch = CONCAT('Title LIKE ', CONCAT('''','%', REPLACE(UserSearch,' ', '%'), '%', ''''));
	ELSE
		Set UserSearch = CONCAT('Title LIKE ', CONCAT('''','%', UserSearch, '%', ''''));
	END IF;
END IF;
    
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

 IF (IsAnnotated = true) then
	Set SqlEnd = CONCAT(' AND postannotation.Annotation IS NOT NULL ', SqlENd);
	END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''Yes''', ', ', '''No''',') AS ','''Validated answer''',
				', users.Name AS Author, DATE_FORMAT(CreationDate,','''%d-%m-%Y''',') AS CreationDate, postannotation.Favourite, IF(postannotation.Annotation IS NULL,', '''No''',', ', '''Yes''',') AS Annotations, PostId
                FROM posts JOIN users ON  users.Id = posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = posts.Id 
                WHERE Title IS NOT NULL AND ',UserSearch, SqlEnd); 

SELECT Id into nb FROM searchhistory WHERE SearchString = SearchStr;


    PREPARE stmt FROM @s;
    EXECUTE stmt;
    SELECT FOUND_ROWS() INTO @NumRows;
    
IF (nb = 0 || nb IS null) then
	INSERT INTO searchhistory (SearchString, Date,ResultsNumber, SearchType) VALUE(SearchStr, CURRENT_TIMESTAMP(), @NumRows, SearchNum);
ELSE
	UPDATE searchhistory SET Date = CURRENT_TIMESTAMP() WHERE Id = nb;
END IF;

    DEALLOCATE PREPARE stmt;
END$$
DELIMITER ;



DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_FavouritesPost`(IN UserSearch VARCHAR(200), IN IsWithBody Bool, IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN


DECLARE  SqlEnd VARCHAR(200);

SET SqlEnd = ' ORDER BY CreationDate '; 

IF (UserSearch = '') then
	SET UserSearch = ' ';
ELSE
	IF (IsWithBody = true) then
		IF (IsExact = true) then
			Set SqlEnd = CONCAT(' AND Title LIKE ''', UserSearch, ''' OR Body LIKE ', '''%', UserSearch, '%''', ' AND Title IS NOT NULL ', SqlEnd);
			END IF;
		Set UserSearch = CONCAT(' AND MATCH(Title, Body) AGAINST (''',UserSearch,''')');
	ELSE
		IF (IsExact = false) then
			Set UserSearch = CONCAT(' AND Title LIKE ', CONCAT('''','%', REPLACE(UserSearch,' ', '%'), '%', ''''));
		ELSE
			Set UserSearch = CONCAT(' AND Title LIKE ', CONCAT('''','%', UserSearch, '%', ''''));
		END IF;
	END IF;
END IF;
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

 IF (IsAnnotated = true) then
	Set SqlEnd = CONCAT(' AND postannotation.Annotation IS NOT NULL ', SqlENd);
	END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''Yes''', ', ', '''No''',') AS ','''Validated answer''',
				', Users.Name AS Author, CreationDate, IF(postannotation.Annotation IS NULL,', '''No''',', ', '''Yes''',') AS Annotations, PostId
                FROM Posts JOIN Users ON  Users.Id = Posts.OwnerUserId LEFT JOIN postannotation ON postannotation.PostId = Posts.Id 
                WHERE Title IS NOT NULL AND postannotation.Favourite = 1  ',UserSearch, SqlEnd); 
 
    PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END$$
DELIMITER ;



DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Result_WithoutAnnotationModel`(IN UserSearch VARCHAR(200), IN IsExact Bool, IN IsAnnotated Bool, IN IsSolved Bool)
BEGIN

DECLARE  SqlEnd VARCHAR(200);

SET SqlEnd = 'ORDER BY CreationDate'; 

SET UserSearch = CONCAT('''','%', UserSearch, '%', '''');

IF (IsExact = false) then
	Set UserSearch = Replace(UserSearch, ' ', '%');
    END IF;
    
IF (IsSolved = true) then
	Set SqlEnd = CONCAT(' AND AcceptedAnswerId IS NOT NULL ', SqlEnd);
	END IF;

-- IF (IsAnnotated = true) then
		
--    END IF;

SET @s = CONCAT('Select title AS Title, Body, AnswerCount, IF(AcceptedAnswerId IS NOT NULL,', '''yes''', ', ', '''no''',') AS ','''Validated answer''',
				', Users.Name AS Author, CreationDate FROM Posts JOIN Users ON  Users.Id = Posts.OwnerUserId WHERE title LIKE ', UserSearch, SqlEnd); 
 
    PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END$$
DELIMITER ;



DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SearchHistory`(IN UserSearch VARCHAR(200), IN Marked INT)
BEGIN

SET @s = CONCAT('SELECT SearchString, Mark, Date, ResultsNumber, Id FROM searchhistory');

IF (UserSearch != '') THEN
	SET @s = CONCAT(@s,' WHERE MATCH(SearchString) AGAINST(''', UserSearch,''') ');
    END IF;

IF (Marked = true) THEN
	IF (UserSearch = '') THEN
		SET @s = CONCAT(@s, ' WHERE ');
	ELSE
		SET @s = CONCAT(@s, ' AND ');
	END IF;
	SET @s = CONCAT(@s,  ' Mark >= ',  Marked);
END IF;

SET @s = CONCAT(@s, ' ;');

PREPARE stmt FROM @s;
    SELECT @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END$$
DELIMITER ;