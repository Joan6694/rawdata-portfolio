-- -----------------------------------------------------
-- Schema stackoverflow_model_big3
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `stackoverflow_model_big3`;
CREATE SCHEMA IF NOT EXISTS `stackoverflow_model_big3`;
USE `stackoverflow_model_big3`;


SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- QA-MODEL
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`users` (
  `Id` INT(11),
  `Name` TEXT,
  `UpVotes` INT(11) UNSIGNED ,
  `DownVotes` INT(11) UNSIGNED ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`posts` (
  `Id` INT(11) UNSIGNED,
  `PostTypeId` INT(11) ,
  `ParentId` INT(11) UNSIGNED,
  `AcceptedAnswerId` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `Score` INT(11) ,
  `ViewCount` INT(11) UNSIGNED,
  `Body` LONGTEXT ,
  `OwnerUserId` INT(11),
  `Title` MEDIUMTEXT ,
  `Tags` MEDIUMTEXT ,
  `AnswerCount` INT(11) UNSIGNED,
  `CommentCount` INT(11) UNSIGNED,
  INDEX `fk_posts_parentId_idx` (`ParentId` ASC),
  CONSTRAINT `fk_posts_users1`
    FOREIGN KEY (`OwnerUserId`)
    REFERENCES `stackoverflow_model_big3`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posts_posts1`
    FOREIGN KEY (`AcceptedAnswerId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_posts_posts2`
    FOREIGN KEY (`ParentId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`comments` (
  `Id` INT(11) UNSIGNED,
  `PostId` INT(11) UNSIGNED,
  `Score` INT(11) ,
  `Text` LONGTEXT ,
  `CreationDate` DATETIME ,
  `UserId` INT(11),
  INDEX `fk_comments_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_comments_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_model_big3`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`postlinks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`postlinks` (
  `Id` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `PostId` INT(11) UNSIGNED,
  `RelatedPostId` INT(11) UNSIGNED,
  `PostLinkTypeId` INT(11) UNSIGNED,
  INDEX `fk_postlinks_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_postlinks_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_postlinks_posts2`
    FOREIGN KEY (`RelatedPostId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`tags` (
  `Id` INT(11) UNSIGNED,
  `TagName` LONGTEXT ,
  `Count` BIGINT(21) UNSIGNED NOT NULL DEFAULT '0' ,
    PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`ti`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`ti` (
  `PostId` INT(11) UNSIGNED,
  `TagId` INT(11) UNSIGNED,
  CONSTRAINT `fk_ti_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ti_tags1`
    FOREIGN KEY (`tagId`)
    REFERENCES `stackoverflow_model_big3`.`tags` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`PostId`,`TagId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`votes` (
  `Id` INT(11) UNSIGNED,
  `PostId` INT(11) UNSIGNED,
  `VoteTypeId` INT(11) UNSIGNED,
  `CreationDate` DATETIME ,
  `UserId` INT(11),
  INDEX `fk_votes_postId_idx` (`PostId` ASC),
  CONSTRAINT `fk_votes_posts1`
    FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_votes_users1`
    FOREIGN KEY (`UserId`)
    REFERENCES `stackoverflow_model_big3`.`users` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;

/*
-- -----------------------------------------------------
-- INSERTIONS INTO QA-MODEL
-- -----------------------------------------------------
insert into `stackoverflow_model_big3`.`users` (
select `stackoverflow_portfolio`.`users`.`Id`, 
`stackoverflow_portfolio`.`users`.`DisplayName`,
`stackoverflow_portfolio`.`users`.`UpVotes`,
`stackoverflow_portfolio`.`users`.`DownVotes`
from `stackoverflow_portfolio`.`users`);

insert into `stackoverflow_model_big3`.`posts` (
select `stackoverflow_portfolio`.`posts`.`Id`,
`stackoverflow_portfolio`.`posts`.`PostTypeId`,
`stackoverflow_portfolio`.`posts`.`ParentId`,
`stackoverflow_portfolio`.`posts`.`AcceptedAnswerId`,
`stackoverflow_portfolio`.`posts`.`CreationDate`,
`stackoverflow_portfolio`.`posts`.`Score`,
`stackoverflow_portfolio`.`posts`.`ViewCount`,
`stackoverflow_portfolio`.`posts`.`Body`,
`stackoverflow_portfolio`.`posts`.`OwnerUserId`,
`stackoverflow_portfolio`.`posts`.`Title`,
`stackoverflow_portfolio`.`posts`.`Tags`,
`stackoverflow_portfolio`.`posts`.`AnswerCount`,
`stackoverflow_portfolio`.`posts`.`CommentCount`
 from `stackoverflow_portfolio`.`posts`);

insert into `stackoverflow_model_big3`.`comments` (
select `stackoverflow_portfolio`.`comments`.`Id`,
`stackoverflow_portfolio`.`comments`.`PostId`,
`stackoverflow_portfolio`.`comments`.`Score`,
`stackoverflow_portfolio`.`comments`.`Text`,
`stackoverflow_portfolio`.`comments`.`CreationDate`,
`stackoverflow_portfolio`.`comments`.`UserId`
 from `stackoverflow_portfolio`.`comments`);

insert into `stackoverflow_model_big3`.`postlinks` (
select `stackoverflow_portfolio`.`postlinks`.`Id`,
`stackoverflow_portfolio`.`postlinks`.`CreationDate`,
`stackoverflow_portfolio`.`postlinks`.`PostId`,
`stackoverflow_portfolio`.`postlinks`.`RelatedPostId`,
`stackoverflow_portfolio`.`postlinks`.`PostLinkTypeId`
 from `stackoverflow_portfolio`.`postlinks`);

insert into `stackoverflow_model_big3`.`tags` (
select `stackoverflow_portfolio`.`tags`.`Id`,
`stackoverflow_portfolio`.`tags`.`tagName`,
`stackoverflow_portfolio`.`tags`.`Count`
 from `stackoverflow_portfolio`.`tags`);

insert into `stackoverflow_model_big3`.`ti` (
select `stackoverflow_portfolio`.`ti`.`pid`,
`stackoverflow_portfolio`.`ti`.`tid`
 from `stackoverflow_portfolio`.`ti`);

insert into `stackoverflow_model_big3`.`votes` (
select `stackoverflow_portfolio`.`votes`.`Id`,
`stackoverflow_portfolio`.`votes`.`PostId`,
`stackoverflow_portfolio`.`votes`.`VoteTypeId`,
`stackoverflow_portfolio`.`votes`.`CreationDate`,
`stackoverflow_portfolio`.`votes`.`UserId`
 from `stackoverflow_portfolio`.`votes`);


-- -----------------------------------------------------
-- ADDITION OF FULLTEXT INDEXES
-- -----------------------------------------------------

ALTER TABLE Posts ADD FULLTEXT(Body, Title);
ALTER TABLE searchhistory ADD FULLTEXT(SearchString);
*/
-- -----------------------------------------------------
-- ANNOTATION MODEL
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`searchhistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`searchhistory` (
  `Id` INT(11) UNSIGNED,
  `SearchString` VARCHAR(255),
  `Date` DATETIME,
  `Mark` INT(1) DEFAULT 0,
  `ResultsNumber` INT UNSIGNED,
  `SearchType` INT(1) UNSIGNED,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stackoverflow_model_big3`.`postannotation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stackoverflow_model_big3`.`postannotation` (
  `PostId` INT(11) UNSIGNED,
  `Favourite` BOOLEAN,
  `Annotation` TEXT,
  CONSTRAINT `fk_postAnnotation_postAnnotation1`
  FOREIGN KEY (`PostId`)
    REFERENCES `stackoverflow_model_big3`.`posts` (`Id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  PRIMARY KEY (`postId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- INSERTION INTO ANNOTATION-MODEL
-- -----------------------------------------------------
TRUNCATE TABLE `stackoverflow_model_big3`.`postannotation`;

INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('7664', true, "Integrate C++ code BSD Project");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('45325', false, "Visual Studio control error");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('85553', true, "MSDN struct");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('105568', false, "Best RTOS at Windows");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('105975', true, "QNX at Windows (VMWare)");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('559839', false, "Teaching Java crash course");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('840190', true, "Java change working directory programatically");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('645840', false, "Javascript web project");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('1310378', true, "Java applet image resize");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('543515', false, "Structs & Classes at C++");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('5117171', true, "C++ solid ascii block");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('951791', false, "Javascript global error handling");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('7178137', false, "Java RMI code dev errors");
INSERT INTO `stackoverflow_model_big3`.`postannotation`
VALUES('5878536', true, "Remove leading number labels Javascript");


TRUNCATE TABLE `stackoverflow_model_big3`.`searchhistory`;

INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('01', 'Java error outofbounds', '2013-08-30 19:05:00', '0', '50', '1');
INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('02', 'Javascript not running', '2013-08-30 19:05:01', '1', '0', '2');
INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('03', 'C++ struct explaination', '2013-08-30 19:05:02', '-1', '10', '1');
INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('04', 'C sucks', '2013-08-30 19:05:03', '0', '50', '2');
INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('05', 'What is the difference between PC and Mac', '2020-08-30 19:05:09', '1', '500', '1');
INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('06', 'I still do not understand Java exceptions', '2013-08-30 19:06:04', '-1', '20', '2');
INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('07', 'Java error stack overflow', '2013-09-02 19:07:05', '0', '25', '1');
INSERT INTO `stackoverflow_model_big3`.`searchhistory`
VALUES('08', 'searchedString8', '2000-09-02 19:04:59', '1', '2', '2');


SET FOREIGN_KEY_CHECKS=1;
