SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- INSERTIONS INTO QA-MODEL
-- -----------------------------------------------------
insert into `stackoverflow_model_big3`.`users` (
select `stackoverflow_portfolio`.`users`.`Id`, 
`stackoverflow_portfolio`.`users`.`DisplayName`,
`stackoverflow_portfolio`.`users`.`UpVotes`,
`stackoverflow_portfolio`.`users`.`DownVotes`
from `stackoverflow_portfolio`.`users`);

insert into `stackoverflow_model_big3`.`posts` (
select `stackoverflow_portfolio`.`posts`.`Id`,
`stackoverflow_portfolio`.`posts`.`PostTypeId`,
`stackoverflow_portfolio`.`posts`.`ParentId`,
`stackoverflow_portfolio`.`posts`.`AcceptedAnswerId`,
`stackoverflow_portfolio`.`posts`.`CreationDate`,
`stackoverflow_portfolio`.`posts`.`Score`,
`stackoverflow_portfolio`.`posts`.`ViewCount`,
`stackoverflow_portfolio`.`posts`.`Body`,
`stackoverflow_portfolio`.`posts`.`OwnerUserId`,
`stackoverflow_portfolio`.`posts`.`Title`,
`stackoverflow_portfolio`.`posts`.`Tags`,
`stackoverflow_portfolio`.`posts`.`AnswerCount`,
`stackoverflow_portfolio`.`posts`.`CommentCount`
 from `stackoverflow_portfolio`.`posts`);

insert into `stackoverflow_model_big3`.`comments` (
select `stackoverflow_portfolio`.`comments`.`Id`,
`stackoverflow_portfolio`.`comments`.`PostId`,
`stackoverflow_portfolio`.`comments`.`Score`,
`stackoverflow_portfolio`.`comments`.`Text`,
`stackoverflow_portfolio`.`comments`.`CreationDate`,
`stackoverflow_portfolio`.`comments`.`UserId`
 from `stackoverflow_portfolio`.`comments`);

insert into `stackoverflow_model_big3`.`postlinks` (
select `stackoverflow_portfolio`.`postlinks`.`Id`,
`stackoverflow_portfolio`.`postlinks`.`CreationDate`,
`stackoverflow_portfolio`.`postlinks`.`PostId`,
`stackoverflow_portfolio`.`postlinks`.`RelatedPostId`,
`stackoverflow_portfolio`.`postlinks`.`PostLinkTypeId`
 from `stackoverflow_portfolio`.`postlinks`);

insert into `stackoverflow_model_big3`.`tags` (
select `stackoverflow_portfolio`.`tags`.`Id`,
`stackoverflow_portfolio`.`tags`.`tagName`,
`stackoverflow_portfolio`.`tags`.`Count`
 from `stackoverflow_portfolio`.`tags`);

insert into `stackoverflow_model_big3`.`ti` (
select `stackoverflow_portfolio`.`ti`.`pid`,
`stackoverflow_portfolio`.`ti`.`tid`
 from `stackoverflow_portfolio`.`ti`);

insert into `stackoverflow_model_big3`.`votes` (
select `stackoverflow_portfolio`.`votes`.`Id`,
`stackoverflow_portfolio`.`votes`.`PostId`,
`stackoverflow_portfolio`.`votes`.`VoteTypeId`,
`stackoverflow_portfolio`.`votes`.`CreationDate`,
`stackoverflow_portfolio`.`votes`.`UserId`
 from `stackoverflow_portfolio`.`votes`);


-- -----------------------------------------------------
-- ADDITION OF FULLTEXT INDEXES
-- -----------------------------------------------------

ALTER TABLE posts ADD FULLTEXT(Body, Title);
ALTER TABLE searchhistory ADD FULLTEXT(SearchString);

SET FOREIGN_KEY_CHECKS=1;
