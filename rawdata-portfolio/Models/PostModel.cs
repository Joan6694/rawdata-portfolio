﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model for the posts.
	/// </summary>
	public class PostFillableModel : IFillableModel<Post>
    {
		/// <summary>
		/// Fill a post with a domain object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
		public void FillWithDomain(Post source, UrlHelper urlHelper)
	    {
		    this.PostType = source.PostTypeId;
		    this.PostUrl = urlHelper.Link("PostApi", new {id = source.Id});
			this.ParentPostUrl = source.ParentId != null ? urlHelper.Link("PostApi", new { id = source.ParentId }) : null;
			this.AcceptedAnswerUrl = source.AcceptedAnswerId != null ? urlHelper.Link("PostApi", new { id = source.AcceptedAnswerId }) : null;
			this.AnswerCount = source.AnswerCount;
			this.CreationDate = source.CreationDate;
			this.Score = source.Score;
			this.ViewCount = source.ViewCount;
			this.CommentCount = source.CommentCount;
			this.Title = source.Title;
			this.Body = source.Body;
			this.Tags = source.Tags;
			this.OwnerUserUrl = urlHelper.Link("UserApi", new { id = source.OwnerUserId });
			this.AnnotationUrl = urlHelper.Link("AnnotationApi", new { id = source.Id });
			this.CommentsUrl = urlHelper.Link("PostCommentApi", new { id = source.Id });
		}

        public int PostType { get; set; }
        public string PostUrl { get; set; }
        public string ParentPostUrl { get; set; } //if answer type
        public string AcceptedAnswerUrl { get; set; } //if question type
        public int? AnswerCount { get; set; } //if question type

        public DateTime CreationDate { get; set; }
        public int Score { get; set; }
        public int ViewCount { get; set; }
        public int CommentCount { get; set; }

        public string Title { get; set; }
        public string Body { get; set; }
        public string Tags { get; set; }
        public string OwnerUserUrl { get; set; }
		public string AnnotationUrl { get; set; }
		public string CommentsUrl { get; set; }
	}
}