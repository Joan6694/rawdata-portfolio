﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model for the search history entries.
	/// </summary>
	public class SearchHistoryEntryModel : IFillableModel<SearchHistoryEntry>
    {
		/// <summary>
		/// Fill a search history entry with a domain.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
		public void FillWithDomain(SearchHistoryEntry source, UrlHelper urlHelper)
	    {
		    this.SearchString = source.SearchString;
		    this.Date = source.Date.ToString("yy/MM/dd HH:MM"); 
            this.Mark = source.Mark;
			this.IsWithBody = (source.SearchType & 1) == 1;
			this.IsExact = (source.SearchType & 2) == 2;
			this.IsAnnotated = (source.SearchType & 4) == 4;
			this.IsSolved = (source.SearchType & 8) == 8;
			Dictionary<string, object> dict = new Dictionary<string, object>();
			dict.Add("searchString", this.SearchString);
			if (this.IsWithBody)
				dict.Add("isWithBody", true);
			if (this.IsExact)
				dict.Add("isExact", true);
			if (this.IsAnnotated)
				dict.Add("isAnnotated", true);
			if (this.IsSolved)
				dict.Add("isSolved", true);
			this.SearchUrl = urlHelper.Link("PostsSearchApi", dict);
			this.ResultNumber = source.ResultsNumber;
			this.Url = urlHelper.Link("SearchHistoryEntryApi", dict);
	    }
        public string Url { get; set; }
        public string SearchString { get; set; }
        public string Date { get; set; }
        public string SearchUrl { get; set; }
        public int ResultNumber { get; set; }
        public int Mark { get; set; }
		public bool IsWithBody { get; set; }
		public bool IsExact { get; set; }
		public bool IsAnnotated { get; set; }
		public bool IsSolved { get; set; }
    }
}