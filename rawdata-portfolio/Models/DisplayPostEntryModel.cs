﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model class for posts intended for display.
	/// </summary>
    public class DisplayPostEntryModel : IFillableModel<DisplayPostEntry>
    {
		/// <summary>
		/// Fill a display post with a domain object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
	    public void FillWithDomain(DisplayPostEntry source, UrlHelper urlHelper)
	    {
		    this.Url = urlHelper.Link("PostApi", new {id = source.PostId});
		    this.Title = source.Title;
		    this.Body = source.Body;
		    this.Score = source.Score;
		    this.CreationDate = source.CreationDate.ToString("yy/MM/dd"); ;
		    this.AcceptedAnswerUrl = source.AcceptedAnswerId == null ? null : urlHelper.Link("PostApi", new {id = source.AcceptedAnswerId});
		    this.Username = source.Username;
		    this.CommentCount = source.CommentCount;
		    this.Favourite = source.Favourite;
		    this.Annotation = source.Annotation;
			this.AnnotationUrl = urlHelper.Link("AnnotationApi", new { id = source.PostId });
			this.CommentsUrl = urlHelper.Link("DisplayCommentsApi", new {postId = source.PostId});
	    }

        public string Title { get; set; }
        public string Body { get; set; }
        public int Score { get; set; }
        public string CreationDate { get; set; }
        public string AcceptedAnswerUrl { get; set; }
        public string Username { get; set; }
        public int CommentCount { get; set; }
        public bool Favourite { get; set; }
        public string Annotation { get; set; }
        public string Url { get; set; }
		public string AnnotationUrl { get; set; }
		public string CommentsUrl { get; set; }
	}
}