﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace rawdata_portfolio.Models
{
    /// <summary>
    /// Object to get the pagination links.
    /// </summary>
    /// <typeparam name="T"></typeparam>
	public class PagingObject<T>
	{
		public PagingObject(int uLimit, int uOffset, UrlHelper uh, string routeName, IEnumerable<T> results, IDictionary<string, object> queryStringDictionary = null, dynamic customData = null)
		{
			string prevPage = null;
			string nextPage = null;

			if (uOffset != 0)
			{
				IDictionary<string, object> dict;
				if (queryStringDictionary == null)
					dict = new Dictionary<string, object>();
				else
					dict = new Dictionary<string, object>(queryStringDictionary);
				dict.Add("limit", uLimit);
				dict.Add("offset", (uOffset - uLimit) < 0 ? 0 : (uOffset - uLimit));
				prevPage = uh.Link(routeName, dict);
			}
			if (results.Count() == uLimit)
			{
				IDictionary<string, object> dict;
				if (queryStringDictionary == null)
					dict = new Dictionary<string, object>();
				else
					dict = new Dictionary<string, object>(queryStringDictionary);
				dict.Add("limit", uLimit);
				dict.Add("offset", uOffset + uLimit);
				nextPage = uh.Link(routeName, dict);
			}
			this.PrevPage = prevPage;
			this.NextPage = nextPage;
			this.Results = results;
			this.CustomData = customData;
		}
		public string PrevPage { get; set; }
		public string NextPage { get; set; }
		public IEnumerable<T> Results { get; set; }
		public dynamic CustomData { get; set; }
	}
}