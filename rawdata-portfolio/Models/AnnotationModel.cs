﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model class for the annotations.
	/// </summary>
	public class AnnotationModel : IDumpableModel<Annotation>
    {
		/// <summary>
		/// Fill the annotation model with a domain object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
		public void FillWithDomain(Annotation source, UrlHelper urlHelper)
	    {
		    this.Url = urlHelper.Link("AnnotationApi", new {id = source.PostId});
		    this.Favourite = source.Favourite;
		    this.Text = source.Text;
	    }

		/// <summary>
		/// Create a domain object with the annotation model.
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public Annotation DumpDomain(HttpRequestContext requestContext)
		{
			ulong id = ulong.Parse(requestContext.RouteData.Values["id"] as string);
			return new Annotation
			{
				PostId = id,
				Favourite = this.Favourite,
				Text = this.Text
			};
		}
        public bool Favourite { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
    }
}