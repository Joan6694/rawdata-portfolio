﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Interface for the dumpable models
	/// </summary>
	/// <typeparam name="Domain"></typeparam>
	public interface IDumpableModel<Domain> : IFillableModel<Domain> where Domain : class
	{
		/// <summary>
		/// Dump a domain object from the model object.
		/// </summary>
		/// <param name="requestContext"></param>
		/// <returns></returns>
		Domain DumpDomain(HttpRequestContext requestContext);
	}
}
