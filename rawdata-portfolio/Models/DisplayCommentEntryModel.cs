﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model class for comments intended for display.
	/// </summary>
    public class DisplayCommentEntryModel : IFillableModel<DisplayCommentEntry>
    {
		/// <summary>
		/// Fill the display comment with a domain object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
	    public void FillWithDomain(DisplayCommentEntry source, UrlHelper urlHelper)
	    {
		    this.Url = urlHelper.Link("CommentApi", new {id = source.PostId});
            this.Username = source.Username;
            this.CreationDate = source.CreationDate.ToString("yy/MM/dd HH:MM"); ;
            this.Score = source.Score;
            this.Body = source.Text;
	    }

        public string Body { get; set; }
        public int Score { get; set; }
        public string CreationDate { get; set; }
        public string Username { get; set; }
        public string Url { get; set; }
    }
}