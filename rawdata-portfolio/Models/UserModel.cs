﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model for the users.
	/// </summary>
    public class UserModel : IFillableModel<User>
    {
		/// <summary>
		/// Fill a user with a domain object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
	    public void FillWithDomain(User source, UrlHelper urlHelper)
	    {
		    this.Url = urlHelper.Link("UserApi", new {id = source.Id});
		    this.Name = source.Name;
		    this.UpVotes = source.UpVotes;
		    this.DownVotes = source.DownVotes;
	    }

        public string Url { get; set; }
        public string Name { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
    }
}