﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Interface for the fillable models.
	/// </summary>
	/// <typeparam name="Domain"></typeparam>
	public interface IFillableModel<Domain> where Domain : class
	{
		/// <summary>
		/// Filla a model with a domain object.
		/// </summary>
		/// <param name="domainObject"></param>
		/// <param name="urlHelper"></param>
		void FillWithDomain(Domain domainObject, UrlHelper urlHelper);
	}
}
