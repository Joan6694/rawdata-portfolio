﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model class for the comments
	/// </summary>
	public class CommentModel : IFillableModel<Comment>
    {
		/// <summary>
		/// Fill the comment model with a domain object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
		public void FillWithDomain(Comment source, UrlHelper urlHelper)
	    {
            this.CommentUrl = urlHelper.Link("CommentApi", new { id = source.Id });
            this.PostUrl = urlHelper.Link("PostApi", new {id = source.PostId});
		    this.UserUrl = urlHelper.Link("UserApi", new {id = source.UserId});
		    this.Score = source.Score;
		    this.Text = source.Text;
            this.CreationDate = source.CreationDate.ToString("yy/MM/dd");
        }
        public string CommentUrl { get; set; }
        public string PostUrl { get; set; }
        public string UserUrl { get; set; }
        public int Score { get; set; }
        public string CreationDate { get; set; }
        public string Text { get; set; }
    }
}