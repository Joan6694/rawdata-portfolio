﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model for the posts search entries.
	/// </summary>
	public class PostsSearchEntryModel : IFillableModel<PostSearchEntry>
    {
		/// <summary>
		/// Fill the post search entry with a domain.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
		public void FillWithDomain(PostSearchEntry source, UrlHelper urlHelper)
	    {
            this.Url = urlHelper.Link("PostApi", new { id = source.Id });
            this.Annotation = source.Annotation;
            this.CreationDate = source.CreationDate.ToString("yy/MM/dd");
            this.Author = source.Author;
            this.ValidatedAnswer = source.ValidatedAnswer;
            this.AnswerCount = source.AnswerCount;
            this.Body = this.StripHtmlTags(source.Body);
            this.Title = source.Title;
            this.Favourite = source.Favourite;
			this.AnnotationUrl = urlHelper.Link("AnnotationApi", new { id = source.Id });
			this.DisplayUrl = urlHelper.Link("DisplayPostsApi", new { postId = source.Id });
		}

        public string Title { get; set; }
        public string Body { get; set; }
        public int AnswerCount { get; set; }
        public string ValidatedAnswer { get; set; }
        public string Author { get; set; }
        public string CreationDate { get; set; }
        public string Annotation { get; set; }
        public bool Favourite { get; set; }
        public string Url { get; set; }
		public string AnnotationUrl { get; set; }
		public string DisplayUrl { get; set; }

		private string StripHtmlTags(string source)
		{
			char[] array = new char[source.Length];
			int arrayIndex = 0;
			int insideCount = 0;

			for (int i = 0; i < source.Length; i++)
			{
				char letter = source[i];
				if (letter == '<')
				{
					++insideCount;
					continue;
				}
				if (letter == '>')
				{
					--insideCount;
					continue;
				}
				if (insideCount == 0)
				{
					array[arrayIndex] = letter;
					arrayIndex++;
				}
			}
			return new string(array, 0, arrayIndex);
		}
    }
}