﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using DAL.Domain;

namespace rawdata_portfolio.Models
{
	/// <summary>
	/// Model for the tags.
	/// </summary>
	public class TagModel : IFillableModel<Tag>
    {
		/// <summary>
		/// Fill a tag with a domain object.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="urlHelper"></param>
		public void FillWithDomain(Tag source, UrlHelper urlHelper)
		{
		    this.Url = urlHelper.Link("TagApi", new {id = source.Id});
		    this.Name = source.TagName;
		    this.Count = source.Count;
	    }
        public string Url { get; set; }
        public string Name { get; set; }
        public long Count { get; set; }
    }
}