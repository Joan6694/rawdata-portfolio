﻿$(document).ready(function () {
	BasicSearchDal = function() {
		var search = function(searchString, isWithBody, isExact, isAnnotated, isSolved, callBack) {
			    $.getJSON("/api/search", {
					    searchString: searchString,
					    isWithBody: isWithBody,
					    isExact: isExact,
					    isAnnotated: isAnnotated,
					    isSolved: isSolved
				    },
				    function(data) {
					    callBack(data.results, data.prevPage, data.nextPage, data.customData);
				    });
		    },
			loadPage = function(url, callBack) {
				$.getJSON(url,
					function(data) {
						callBack(data.results, data.prevPage, data.nextPage, data.customData);
					});
			},
			setFavourite = function(url, favourite, callBack) {
				$.getJSON(url).done(function(data) {
						$.post(url, { favourite: favourite, text: null, url: "" }, function() {
							callBack();
						}, 'json');
					})
					.fail(function(jqxhr, textStatus, error) {
						$.ajax({
							url: url,
							data: { favourite: favourite, text: null, url: "" },
							type: 'PUT',
							dataType: 'json'
						}).done(function() {
							callBack();
						});
					});
			},
			rateHistory = function(url, rating, callBack) {
				$.post(url, {
					url: null,
					searchString: null,
					date: "2008-09-27T03:01:31",
					searchUrl: null,
					resultNumber: 0,
					mark: rating
				}, function(response) {
					callBack(response);
				}, 'json');
			};
		return ({
			search: search,
			loadPage: loadPage,
			setFavourite: setFavourite,
			rateHistory: rateHistory
		});
	};

	PostsDal = function() {
		var getPost = function(url, callBack) {
			    $.getJSON(url,
				    function(data) {
					    callBack(data);
				    });
		    },
			getComments = function(url, callBack) {
				$.getJSON(url,
					function(data) {
						callBack(data);
					});
			},

			deleteAnnotation = function(url, callBack) {
				$.ajax({
					url: url,
					type: 'DELETE',
				}).done(function() {
					callBack(null);
				});
			},

			setFavourite = function(url, favourite, callBack) {
				$.getJSON(url).done(function(data) {
						$.post(url, { favourite: favourite, text: null, url: "" }, function() {
							callBack();
						}, 'json');
					})
					.fail(function(jqxhr, textStatus, error) {
						$.ajax({
							url: url,
							data: { favourite: favourite, text: null, url: "" },
							type: 'PUT',
							dataType: 'json'
						}).done(function() {
							callBack();
						});
					});
			},
			setAnnotation = function(url, annotation, favourite, callBack) {
				$.getJSON(url).done(function(data) {
						$.post(url, { favourite: favourite, text: annotation, url: "" }, function() {
							callBack();
						}, 'json');
					})
					.fail(function(jqxhr, textStatus, error) {
						$.ajax({
							url: url,
							data: { favourite: favourite, text: annotation, url: "" },
							type: 'PUT',
							dataType: 'json'
						}).done(function() {
							callBack();
						});
					});
			};
		return ({
			getPost: getPost,
			getComments: getComments,
			setFavourite: setFavourite,
			deleteAnnotation: deleteAnnotation,
			setAnnotation: setAnnotation
		});
	};

	SearchHistoryDal = function() {
		var search = function(searchString, marked, callBack) {
			    $.getJSON("/api/searchhistorysearch", {
					    searchString: searchString,
					    marked: marked
				    },
				    function(data) {
				    	callBack(data.results, data.prevPage, data.nextPage, data.customData);
				    });
		    },
			loadPage = function(url, callBack) {
				$.getJSON(url,
					function(data) {
						callBack(data.results, data.prevPage, data.nextPage, data.customData);
					});
			};
		return ({
			search: search,
			loadPage: loadPage
		});
	};
	FavouriteSearchDal = function() {
		var search = function(searchString, isWithBody, isExact, isAnnotated, isSolved, callBack) {
			    $.getJSON("/api/favourites", {
					    searchString: searchString,
					    isWithBody: isWithBody,
					    isExact: isExact,
					    isAnnotated: isAnnotated,
					    isSolved: isSolved
				    },
				    function(data) {
				    	callBack(data.results, data.prevPage, data.nextPage, data.customData);
				    });
		    },
			loadPage = function(url, callBack) {
				$.getJSON(url,
					function(data) {
						callBack(data.results, data.prevPage, data.nextPage, data.customData);
					});
			},
			setFavourite = function (url, favourite, callBack) {
				$.getJSON(url).done(function (data) {
					$.post(url, { favourite: favourite, text: null, url: "" }, function () {
						callBack();
					}, 'json');
				})
					.fail(function (jqxhr, textStatus, error) {
						$.ajax({
							url: url,
							data: { favourite: favourite, text: null, url: "" },
							type: 'PUT',
							dataType: 'json'
						}).done(function () {
							callBack(null);
						});
					});
			};
		return ({
			search: search,
			loadPage: loadPage,
			setFavourite: setFavourite,
		});
	};
});