﻿requirejs.config({
	baseUrl: './'
});

require(["Scripts/text"], function (module) {

	//Bootstrap switch function
	ko.bindingHandlers.bootstrapSwitch = {
		init: function (element, valueAccessor, allBindingsAccessor) {
			$(element).bootstrapSwitch();
			$(element).bootstrapSwitch('state', valueAccessor()());
			$(element).on('switchChange.bootstrapSwitch', function (event, state) {
				var observable = valueAccessor();
				observable(state);
			});
			var options = allBindingsAccessor().bootstrapSwitchOptions || {};
			for (var property in options) {
				$(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
			}
			ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
				$(element).bootstrapSwitch("destroy");
			});
		},
		update: function (element, valueAccessor, allBindingsAccessor) {
			var value = ko.utils.unwrapObservable(valueAccessor());
			var options = allBindingsAccessor().bootstrapSwitchOptions || {};
			for (var property in options) {
				$(element).bootstrapSwitch(property, ko.utils.unwrapObservable(options[property]));
			}
			$(element).bootstrapSwitch("state", value);
		}
	};

	//BASIC SEARCH component
	ko.components.register("basicSearch-component", {
		viewModel: function (params) {
			var results = ko.observableArray([]).extend({ notify: 'always' }),
				prevPage = ko.observable(null),
				nextPage = ko.observable(null),
                customData = ko.observable({}),
                searchPerformed = ko.observable(false),
                showFlag = ko.observable(false),
				searchString = ko.observable(params.root.currentSearch().searchString),
				isWithBody = ko.observable(params.root.currentSearch().isWithBody),
				isExact = ko.observable(params.root.currentSearch().isExact),
				isAnnotated = ko.observable(params.root.currentSearch().isAnnotated),
				isSolved = ko.observable(params.root.currentSearch().isSolved),
                selectedRating = ko.observable(0),
				dataAccess = BasicSearchDal();

			function fillResults(rResults, rPrevPage, rNextPage, rCustomData) {
				results(rResults);
				prevPage(rPrevPage);
				nextPage(rNextPage);
				customData(rCustomData);
			}

			var basicSearch = function () {
				if (searchString() !== "") {
					params.root.setCurrentSearch(searchString(), isWithBody(), isExact(), isAnnotated(), isSolved());
					dataAccess.search(searchString(), isWithBody(), isExact(), isAnnotated(), isSolved(),
						function (rResults, rPrevPage, rNextPage, rCustomData) {
							searchPerformed(true);
							showFlag(true);
							fillResults(rResults, rPrevPage, rNextPage, rCustomData);
						});
				}
			},

		        isPostSolved = function (post) {
		        	return (post.validatedAnswer === "Yes");
		        },

		        isPostAnnotated = function (post) {
		        	return (post.annotation === "Yes");
		        },

		        switchFavourite = function (post) {
		        	post.favourite = !post.favourite;
		        	var sav = results();
		        	results([]);
		        	results(sav);
		        	dataAccess.setFavourite(post.annotationUrl, post.favourite, function () { });
		        },

		        loadNextPage = function () {
		        	if (nextPage() != null) {
		        		dataAccess.loadPage(nextPage(), function (rResults, rPrevPage, rNextPage, rCustomData) {
		        			$("html, body").animate({ scrollTop: 0 }, 600);
		        			fillResults(rResults, rPrevPage, rNextPage, rCustomData);
		        		});
		        	}
		        },

		        loadPrevPage = function () {
		        	if (prevPage() != null) {
		        		dataAccess.loadPage(prevPage(), function (rResults, rPrevPage, rNextPage, rCustomData) {
		        			$("html, body").animate({ scrollTop: 0 }, 600);
		        			fillResults(rResults, rPrevPage, rNextPage, rCustomData);
		        		});
		        	}
		        },

		        isNextPage = function () {
		        	return (nextPage() != null);
		        },

		        isPrevPage = function () {
		        	return (prevPage() != null);
		        },

		        rateHistory = function (value) {
		        	selectedRating(value);
		        	dataAccess.rateHistory(customData().searchHistoryEntryUrl, value, function () { });
		        },

		        hideButton = function () {
		        	showFlag(false);
		        },

		        handleEnterKey = function (data, event) {
		        	if (event.which == 13)
		        		basicSearch();
		        	return (true);
		        };

			basicSearch();
			$.fn.bootstrapSwitch.defaults.size = 'mini';

			return ({
				results: results,
				prevPage: prevPage,
				nextPage: nextPage,
				searchPerformed: searchPerformed,
				showFlag: showFlag,
				searchString: searchString,
				isWithBody: isWithBody,
				isExact: isExact,
				isAnnotated: isAnnotated,
				isSolved: isSolved,
				selectedRating: selectedRating,
				hideButton: hideButton,
				basicSearch: basicSearch,
				isPostSolved: isPostSolved,
				isPostAnnotated: isPostAnnotated,
				switchFavourite: switchFavourite,
				loadNextPage: loadNextPage,
				loadPrevPage: loadPrevPage,
				isNextPage: isNextPage,
				isPrevPage: isPrevPage,
				rateHistory: rateHistory,
				handleEnterKey: handleEnterKey
			});
		},
		template: { require: "Scripts/text!Templates/basicSearch.html" }
	});

	//POST component
	ko.components.register("post-component", {
		viewModel: function (params) {
			var userAnnotation = ko.observable(""),
				question = ko.observable({}).extend({ notify: 'always' }),
				answers = ko.observableArray([]),
				dataAccess = PostsDal(),
				ready = ko.observable(false),
				commentsShown = ko.observable(false),

				displayPost = function (url) {
					dataAccess.getPost(url, function (posts) {
						question(posts.shift());
						question().commentsShown = false;
						answers(posts);
						var i = 0;
						while (i < answers().length) {
							answers()[i].commentsShown = false;
							i++;
						}
						ready(true);
					});
				},

				saveAnnotation = function () {
					if (userAnnotation() == "") {
						dataAccess.deleteAnnotation(question().annotationUrl, function () {
						});
						$("#myModal").modal("show");
					} else {
						dataAccess.setAnnotation(question().annotationUrl, userAnnotation(), question().favourite, function () {
						});
						$("#myModal").modal("show");
					}
				},

				isValidatedAnswer = function (post) {
					return (question().acceptedAnswerUrl === post.url);
				},

				showQuestionComments = function () {
					question().commentsShown = (!question().commentsShown);
					if (question().commentsShown === true && typeof question().comments === 'undefined') {
						dataAccess.getComments(question().commentsUrl, function (data) {
							console.log(data);
							question().comments = data;
							var sav = question();
							question(sav);
						});
					} else {
						var sav = question();
						question(sav);
					}
				},

				showComments = function (post) {
					post.commentsShown = (!post.commentsShown);
					if (post.commentsShown === true && typeof post.comments === 'undefined') {
						var i = 0;
						while (i < answers().length) {
							if (answers()[i].url === post.url) {
								dataAccess.getComments(post.commentsUrl, function (data) {
									answers()[i].comments = data;
									var sav = answers();
									answers([]);
									answers(sav);
								});
								break;
							}
							i++;
						}
					} else {
						var sav = answers();
						answers([]);
						answers(sav);
					}
				},

				areCommentsShown = function (post) {
					return (post.commentsShown);
				},

				isPostSolved = function (post) {
					return (post.acceptedAnswerUrl != null);
				},

				isPostAnnotated = function (post) {
					return (post.annotation != null && post.annotation !== "");
				},

				switchFavourite = function (post) {
					question().favourite = !question().favourite;
					var sav = question();
					question(sav);
					dataAccess.setFavourite(question().annotationUrl, question().favourite, function () { });
				};

			displayPost(params.root.currentDisplayUrl());

			return ({
				answers: answers,
				question: question,
				isPostSolved: isPostSolved,
				isPostAnnotated: isPostAnnotated,
				isValidatedAnswer: isValidatedAnswer,
				switchFavourite: switchFavourite,
				userAnnotation: userAnnotation,
				saveAnnotation: saveAnnotation,
				showComments: showComments,
				displayPost: displayPost,
				ready: ready,
				commentsShown: commentsShown,
				areCommentsShown: areCommentsShown,
				showQuestionComments: showQuestionComments
			});
		},
		template: { require: "Scripts/text!Templates/post.html" }
	});

	//FAVOURITE SEARCH component
	ko.components.register("favouriteSearch-component", {
		viewModel: function (params) {
			var results = ko.observableArray([]).extend({ notify: 'always' }),
		        prevPage = ko.observable(null),
		        nextPage = ko.observable(null),

		        searchPerformed = ko.observable(false),

		        searchString = ko.observable(""),
		        isWithBody = ko.observable(false),
		        isExact = ko.observable(false),
		        isAnnotated = ko.observable(false),
		        isSolved = ko.observable(false),

		        dataAccess = FavouriteSearchDal();

			function fillResults(rResults, rPrevPage, rNextPage, rCustomData) {
				var i = 0;
				while (i < rResults.length) {
					rResults[i].favourite = true;
					i++;
				}
				results(rResults);
				prevPage(rPrevPage);
				nextPage(rNextPage);
			};

			var favouriteSearch = function () {
				dataAccess.search(searchString(), isWithBody(), isExact(), isAnnotated(), isSolved(),
					function (rResults, rPrevPage, rNextPage, rCustomData) {
						searchPerformed(true);
						fillResults(rResults, rPrevPage, rNextPage, rCustomData);
					});
			},

		        isPostSolved = function (post) {
		        	return (post.validatedAnswer === "Yes");
		        },

		        isPostAnnotated = function (post) {
		        	return (post.annotation === "Yes");
		        },

		        switchFavourite = function (post) {
		        	post.favourite = !post.favourite;
		        	var sav = results();
		        	results([]);
		        	results(sav);
		        	dataAccess.setFavourite(post.annotationUrl, post.favourite, function () { });
		        },

		        loadNextPage = function () {
		        	if (nextPage() != null) {
		        		dataAccess.loadPage(nextPage(), function (rResults, rPrevPage, rNextPage, rCustomData) {
		        			$("html, body").animate({ scrollTop: 0 }, 600);
		        			fillResults(rResults, rPrevPage, rNextPage, rCustomData);
		        		});
		        	}
		        },

		        loadPrevPage = function () {
		        	if (prevPage() != null) {
		        		dataAccess.loadPage(prevPage(), function (rResults, rPrevPage, rNextPage, rCustomData) {
		        			$("html, body").animate({ scrollTop: 0 }, 600);
		        			fillResults(rResults, rPrevPage, rNextPage, rCustomData);
		        		});
		        	}
		        },

		        isNextPage = function () {
		        	return (nextPage() != null);
		        },

		        isPrevPage = function () {
		        	return (prevPage() != null);
		        },

		        handleEnterKey = function (data, event) {
		        	if (event.which == 13)
		        		favouriteSearch();
		        	return (true);
		        };
			favouriteSearch();
			$.fn.bootstrapSwitch.defaults.size = 'mini';
			return ({
				results: results,
				prevPage: prevPage,
				nextPage: nextPage,
				searchPerformed: searchPerformed,
				searchString: searchString,
				isWithBody: isWithBody,
				isExact: isExact,
				isAnnotated: isAnnotated,
				isSolved: isSolved,
				favouriteSearch: favouriteSearch,
				isPostSolved: isPostSolved,
				isPostAnnotated: isPostAnnotated,
				switchFavourite: switchFavourite,
				loadNextPage: loadNextPage,
				loadPrevPage: loadPrevPage,
				isNextPage: isNextPage,
				isPrevPage: isPrevPage,
				handleEnterKey: handleEnterKey
			});
		},
		template: { require: "Scripts/text!Templates/favouriteSearch.html" }
	});

	//HISTORY SEARCH component
	ko.components.register("searchHistory-component", {
		viewModel: function (params) {
			var dropdownItems = ko.observableArray(["Unrated or Good", "Good only", "All"]),
		        currentItem = ko.observable("Unrated or Good"),
		        dropdownValues = ko.observableArray([0, 1, -1]),
		        currentValue = ko.observable(-1),
		        searchString = ko.observable(""),
		        results = ko.observableArray([]),
		        prevPage = ko.observable(null),
		        nextPage = ko.observable(null),
		        searchPerformed = ko.observable(false),
		        dataAccess = SearchHistoryDal();

			function fillResults(rResults, rPrevPage, rNextPage, rCustomData) {
				results(rResults);
				prevPage(rPrevPage);
				nextPage(rNextPage);
			}

			var changeSelection = function () {
				currentItem(arguments[0]);
				currentValue(arguments[1]);
			},

		        historySearch = function () {
		        	dataAccess.search(searchString(), currentValue(), function (rResults, rPrevPage, rNextPage, rCustomData) {
		        		searchPerformed(true);
		        		fillResults(rResults, rPrevPage, rNextPage, rCustomData);
		        	});
		        },

		        isSelected = function (value) {
		        	return value === currentValue();
		        },
		        handleEnterKey = function (data, event) {
		        	if (event.which == 13)
		        		historySearch();
		        	return (true);
		        },

		        loadNextPage = function () {
		        	if (nextPage() != null) {
		        		dataAccess.loadPage(nextPage(), function (rResults, rPrevPage, rNextPage, rCustomData) {
		        			$("html, body").animate({ scrollTop: 0 }, 600);
		        			fillResults(rResults, rPrevPage, rNextPage, rCustomData);
		        		});
		        	}
		        },

		        loadPrevPage = function () {
		        	if (prevPage() != null) {
		        		dataAccess.loadPage(prevPage(), function (rResults, rPrevPage, rNextPage, rCustomData) {
		        			$("html, body").animate({ scrollTop: 0 }, 600);
		        			fillResults(rResults, rPrevPage, rNextPage, rCustomData);
		        		});
		        	}
		        },
		        isNextPage = function () {
		        	return (nextPage() != null);
		        },
		        isPrevPage = function () {
		        	return (prevPage() != null);
		        },
		        isMark = function (searchEntry, value) {
		        	return (searchEntry.mark == value);
		        };

			historySearch();
			return ({
				changeSelection: changeSelection,
				historySearch: historySearch,
				isSelected: isSelected,
				results: results,
				prevPage: prevPage,
				nextPage: nextPage,
				handleEnterKey: handleEnterKey,
				loadNextPage: loadNextPage,
				loadPrevPage: loadPrevPage,
				isNextPage: isNextPage,
				isPrevPage: isPrevPage,
				searchString: searchString,
				isMark: isMark,
				searchPerformed: searchPerformed
			});
		},
		template: { require: "Scripts/text!Templates/searchHistory.html" }
	});

	//HOMEPAGE component
	ko.components.register("home-component", {
		viewModel: function (params) {

			return ({});
		},
		template: { require: "Scripts/text!Templates/home.html" }
	});

	//INDEX (navbar)
	var indexViewModel = function () {
		var menuItems = ko.observableArray(["Home", "Search", "Favourites", "History"]),
			currentMenu = ko.observable("Home"),
			componentItems = ko.observableArray(["home-component", "basicSearch-component", "favouriteSearch-component", "searchHistory-component"]),
			currentComponentName = ko.observable("home-component"),
			currentDisplayUrl = ko.observable(""),
			currentSearch = ko.observable({
				searchString: "",
				isWithBody: false,
				isExact: false,
				isSolved: false,
				isAnnotated: false
			}),
			changePage = function (menu) {
				$("#bs-example-navbar-collapse-1").collapse('hide');
				currentMenu(menu);
				var idx = menuItems.indexOf(menu);
				currentComponentName(componentItems()[idx]);

			},

			isActive = function (menu) {
				return menu === currentMenu();
			},
			showPost = function (post) {
				console.log(post);
				currentDisplayUrl(post.displayUrl);
				currentMenu("");
				currentComponentName("post-component");
			},
			setCurrentSearch = function (searchString, isWithBody, isExact, isAnnotated, isSolved) {
				currentSearch().searchString = searchString;
				currentSearch().isWithBody = isWithBody;
				currentSearch().isExact = isExact;
				currentSearch().isSolved = isSolved;
				currentSearch().isAnnotated = isAnnotated;
			},
        	performSearch = function (searchHistoryEntry) {
        		currentSearch().searchString = searchHistoryEntry.searchString;
        		currentSearch().isWithBody = searchHistoryEntry.isWithBody;
        		currentSearch().isExact = searchHistoryEntry.isExact;
        		currentSearch().isSolved = searchHistoryEntry.isSolved;
        		currentSearch().isAnnotated = searchHistoryEntry.isAnnotated;
        		currentMenu("Search");
        		currentComponentName("basicSearch-component");
        	};
		return ({
			menuItems: menuItems,
			currentMenu: currentMenu,
			componentItems: componentItems,
			currentComponentName: currentComponentName,
			changePage: changePage,
			isActive: isActive,
			showPost: showPost,
			currentDisplayUrl: currentDisplayUrl,
			performSearch: performSearch,
			currentSearch: currentSearch,
			setCurrentSearch: setCurrentSearch
		});
	};

	ko.applyBindings(indexViewModel);

});
