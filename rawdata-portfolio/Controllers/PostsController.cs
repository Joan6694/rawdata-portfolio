using DAL.Domain;
using DAL.Repository;
using rawdata_portfolio.Models;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Posts controller.
	/// </summary>
	public class PostsController : AbstractController<PostRepository, Post, PostFillableModel>
	{
		protected override string _routeName
		{
			get { return ("PostApi"); }
		}
	}
}