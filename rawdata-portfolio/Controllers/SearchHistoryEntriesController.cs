﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL;
using DAL.Domain;
using DAL.Repository;
using rawdata_portfolio.Models;

namespace rawdata_portfolio.Controllers
{
    /// <summary>
    /// Controller for the search history entries.
    /// </summary>
    public class SearchHistoryEntriesController : ApiController
    {
		/// <summary>
		/// The repository.
		/// </summary>
		private SearchHistoryEntryRepository _repository = new SearchHistoryEntryRepository();

	    private string _routeName = "SearchHistorySearchApi";

		/// <summary>
		/// Search through the search history.
		/// </summary>
		/// <param name="searchString">The search string. If it's empty, the function returns all of the results</param>
		/// <param name="marked">Returns all the entries that have their marked value equal or greater than this one.</param>
		/// <param name="limit">The maximum number of results.</param>
		/// <param name="offset">The offset.</param>
		/// <returns></returns>
		[AcceptVerbs("GET")]
		[Route("api/searchhistorysearch")]
	    public HttpResponseMessage Search([FromUri] string searchString = "", [FromUri] int marked = -1,
			[FromUri] int limit = 25, [FromUri] int offset = 0)
		{
			if (searchString == null)
				searchString = "";
			if (limit > 100)
				limit = 100;
			Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
			queryStringDictionary.Add("searchString", searchString);
			queryStringDictionary.Add("marked", marked);
			return Request.CreateResponse(HttpStatusCode.OK,
				new PagingObject<SearchHistoryEntryModel>(limit, offset, Url, this._routeName,
					this._repository.GetResult(searchString, marked, limit, offset).Select(domainObject =>
			{
				SearchHistoryEntryModel m = new SearchHistoryEntryModel();
				m.FillWithDomain(domainObject, Url);
				return (m);
			}), queryStringDictionary));
	    }

		/// <summary>
		/// Get the search history entry with the search string <paramref name="searchString"/> and the search type represented by the four booleans.
		/// </summary>
		/// <param name="searchString"></param>
		/// <param name="isWithBody"></param>
		/// <param name="isExact"></param>
		/// <param name="isAnnotated"></param>
		/// <param name="isSolved"></param>
		/// <returns></returns>
		[AcceptVerbs("GET")]
	    public HttpResponseMessage Get([FromUri] string searchString, [FromUri] bool isWithBody = false,
			[FromUri] bool isExact = false, [FromUri] bool isAnnotated = false, [FromUri] bool isSolved = false)
	    {
			SearchHistoryEntry domainObject = _repository.GetBySearchStringAndType(searchString, isWithBody, isExact, isAnnotated, isSolved);
			if (domainObject == null)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound);
			}
			SearchHistoryEntryModel m = new SearchHistoryEntryModel();
			m.FillWithDomain(domainObject, Url);
			return (Request.CreateResponse(HttpStatusCode.OK, m));
	    }

		/// <summary>
		/// Updates the mark of the search history entry with the search string <paramref name="searchString"/> and the search type represented by the four booleans.
		/// </summary>
		/// <param name="searchString"></param>
		/// <param name="model"></param>
		/// <param name="isWithBody"></param>
		/// <param name="isExact"></param>
		/// <param name="isAnnotated"></param>
		/// <param name="isSolved"></param>
		/// <returns></returns>
		[AcceptVerbs("POST")]
		public HttpResponseMessage Post([FromUri] string searchString, [FromBody] SearchHistoryEntryModel model, [FromUri] bool isWithBody = false,
			[FromUri] bool isExact = false, [FromUri] bool isAnnotated = false, [FromUri] bool isSolved = false)
		{
			if (this._repository.UpdateMark(searchString, model.Mark, isWithBody, isExact, isAnnotated, isSolved))
				return Request.CreateResponse(HttpStatusCode.OK);
			return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Resource not found");
		}
    }
}
