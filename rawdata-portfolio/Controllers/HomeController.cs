﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace rawdata_portfolio.Controllers
{
    /// <summary>
    /// Controller for the home page.
    /// </summary>
    public class HomeController : Controller
    {
	    public ActionResult Index()
	    {
		    return View();
	    }

	    public ActionResult ApiHome()
	    {
		    return View();
	    }
    }
}
