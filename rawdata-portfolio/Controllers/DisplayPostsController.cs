﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Repository;
using rawdata_portfolio.Models;

namespace rawdata_portfolio.Controllers
{
    /// <summary>
    /// Controller for the display of posts.
    /// </summary>
    public class DisplayPostsController : ApiController
    {
		/// <summary>
		/// The repository.
		/// </summary>
		private DisplayPostEntryRepository _repository = new DisplayPostEntryRepository();

		/// <summary>
		/// Get a post intended for display identified by the id <paramref name="postId"/>
		/// </summary>
		/// <param name="postId"></param>
		/// <returns></returns>
	    public HttpResponseMessage Get(int postId)
	    {
			return Request.CreateResponse(HttpStatusCode.OK, _repository.GetResult(postId).Select(domainObject =>
			{
				DisplayPostEntryModel m = new DisplayPostEntryModel();
				m.FillWithDomain(domainObject, Url);
				return (m);
			}));
	    }
    }
}
