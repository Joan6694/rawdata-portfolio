﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Repository;
using rawdata_portfolio.Models;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Controller for the posts searches.
	/// </summary>
	public class PostsSearchController : ApiController
	{
		/// <summary>
		/// The repository.
		/// </summary>
		private PostSearchRepository _repository = new PostSearchRepository();

		private string _routeName = "PostsSearchApi";

		/// <summary>
		/// Get the results of a posts search.
		/// </summary>
		/// <param name="searchString">The search string</param>
		/// <param name="isWithBody">If true, the algorithm will also look in the body of the post</param>
		/// <param name="isExact">If true, the algorithm won't look for each word separately in the sentence bu will look for the exact search string.</param>
		/// <param name="isAnnotated">If true, show only annotated results</param>
		/// <param name="isSolved">If true, show only solved posts</param>
		/// <param name="limit">The number of results (default = 25)</param>
		/// <param name="offset">The offset of the search</param>
		/// <returns></returns>
		public HttpResponseMessage Get([FromUri] string searchString,
			[FromUri] bool isWithBody = false, [FromUri] bool isExact = false, [FromUri] bool isAnnotated = false,
			[FromUri] bool isSolved = false,
			[FromUri] int limit = 25, [FromUri] int offset = 0)
		{
			if (searchString != null && searchString.Trim() != "")
			{
				if (limit > 100)
					limit = 100;
				Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
				queryStringDictionary.Add("searchString", searchString);
				if (isWithBody)
					queryStringDictionary.Add("isWithBody", true);
				if (isExact)
					queryStringDictionary.Add("isExact", true);
				if (isAnnotated)
					queryStringDictionary.Add("isAnnotated", true);
				if (isSolved)
					queryStringDictionary.Add("isSolved", true);
				return Request.CreateResponse(HttpStatusCode.OK,
					new PagingObject<PostsSearchEntryModel>(limit, offset, Url, this._routeName,
						this._repository.GetResult(searchString, isWithBody, isExact, isAnnotated, isSolved, limit, offset)
							.Select(domainObject =>
							{
								PostsSearchEntryModel m = new PostsSearchEntryModel();
								m.FillWithDomain(domainObject, Url);
								return (m);
							}), queryStringDictionary, new {SearchHistoryEntryUrl = Url.Link("SearchHistoryEntryApi", queryStringDictionary)}));
			}
			else
				return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Bad parameter 'searchString'"));
		}
	}
}
