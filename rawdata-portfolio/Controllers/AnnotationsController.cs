﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using rawdata_portfolio.Models;
using DAL.Repository;
using DAL.Domain;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Controller for  annotations.
	/// </summary>
	public class AnnotationsController : AbstractUpdatableController<AnnotationRepository, Annotation, AnnotationModel>
	{
		protected override string _routeName
		{
			get { return ("AnnotationApi"); }
		}
	}
}
