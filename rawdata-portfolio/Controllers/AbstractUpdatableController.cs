﻿using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;
using rawdata_portfolio.Models;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Generic class inherited by read/write controllers. It inherits from the AbstractController class.
	/// It implements a Post, Put and Delete method which are respectively used to Update, Insert and Delete resources.
	/// </summary>
	/// <typeparam name="UpdatableRepository"></typeparam>
	/// <typeparam name="Domain"></typeparam>
	/// <typeparam name="Model"></typeparam>
    public abstract class AbstractUpdatableController<UpdatableRepository, Domain, Model> : AbstractController<UpdatableRepository, Domain, Model>
        where UpdatableRepository : IUpdatableRepository<Domain>, new()
        where Domain : class
		where Model : class, IDumpableModel<Domain>, new()
    {

        IUpdatableRepository<Domain> _updatableRepository = new UpdatableRepository();

        /// <summary>
        /// Update the resource identified by the id <paramref name="id"/> with the fields contained in the <paramref name="model"/> object.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage Post(ulong id, [FromBody] Model model)
        {
            Domain domainObject = model.DumpDomain(RequestContext);
            if (_updatableRepository.Update(domainObject, id))
	            return Request.CreateResponse(HttpStatusCode.OK);
	        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Resource not found");
        }

		/// <summary>
		/// Insert a new resource with the <paramref name="model"/> object fields
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
        public HttpResponseMessage Put([FromBody] Model model)
        {
			Domain domainObject = model.DumpDomain(RequestContext);
            if (_updatableRepository.Insert(domainObject))
	            return Request.CreateResponse(HttpStatusCode.Created, model);
	        return Request.CreateErrorResponse(HttpStatusCode.Conflict, "Duplicate resource");
        }

        /// <summary>
        /// Deleta the resource identified by the id <paramref name="id"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Delete(ulong id)
        {
            if (_updatableRepository.Delete(id))
	            return Request.CreateResponse(HttpStatusCode.OK);
	        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Resource not found");
        }
    }
}