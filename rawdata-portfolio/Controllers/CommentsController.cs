﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using rawdata_portfolio.Models;
using DAL.Repository;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Comments controller.
	/// </summary>
	public class CommentsController : AbstractController<CommentRepository, Comment, CommentModel>
	{
		protected override string _routeName
		{
			get { return ("CommentApi"); }
		}

		/// <summary>
		/// Function that returns the comments of a certain post with the id <paramref name="id"/>
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public HttpResponseMessage GetPostComments(ulong id)
		{
			return Request.CreateResponse(HttpStatusCode.OK,
				(this._repository as CommentRepository).GetPostComments(id).Select(domainObject =>
				{
					CommentModel m = new CommentModel();
					m.FillWithDomain(domainObject, Url);
					return (m);
				}));
		}
	}
}
