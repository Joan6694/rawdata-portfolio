﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Repository;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Web.Http.Routing;
using rawdata_portfolio.Models;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Generic classe inherited by read only controllers. It implements two Get methods, one that returns "everything" (with a limit) and the other that returns a resource with a specific id.
	/// </summary>
	/// <typeparam name="Repository"></typeparam>
	/// <typeparam name="Domain"></typeparam>
	/// <typeparam name="Model"></typeparam>
	public abstract class AbstractController<Repository, Domain, Model> : ApiController
		where Model : class, IFillableModel<Domain>, new()
		where Repository : IRepository<Domain>, new()
		where Domain : class
	{
		/// <summary>
		/// The repository.
		/// </summary>
		protected IRepository<Domain> _repository = new Repository();

		protected abstract string _routeName { get; }

		/// <summary>
		/// Function that returns a list of model objects with a certain limit and a certain offset.
		/// </summary>
		/// <param name="limit"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public HttpResponseMessage Get([FromUri] int limit = 25, [FromUri] int offset = 0)
		{
			if (limit > 100)
				limit = 100;

			return Request.CreateResponse(HttpStatusCode.OK,
				new PagingObject<Model>(limit, offset, this.Url, this._routeName,
					this._repository.GetAll(limit, offset).Select(domainObject =>
			{
				Model m = new Model();
				m.FillWithDomain(domainObject, Url);
				return (m);
			})));
		}

		/// <summary>
		/// Function that returns a resource with the id <paramref name="id"/>
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public HttpResponseMessage Get(ulong id)
		{
			Domain domainObject = _repository.GetById(id);
			if (domainObject == null)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound);
			}
			Model m = new Model();
			m.FillWithDomain(domainObject, Url);
			return (Request.CreateResponse(HttpStatusCode.OK, m));
		}
	}
}