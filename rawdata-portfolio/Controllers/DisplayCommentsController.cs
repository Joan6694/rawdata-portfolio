﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Repository;
using rawdata_portfolio.Models;

namespace rawdata_portfolio.Controllers
{
    /// <summary>
    /// Controller for the display of comments.
    /// </summary>
    public class DisplayCommentsController : ApiController
    {
		/// <summary>
		/// The repository
		/// </summary>
        private DisplayCommentEntryRepository _repository = new DisplayCommentEntryRepository();

		/// <summary>
		/// Get the comments of a post identified by <paramref name="postId"/>
		/// </summary>
		/// <param name="postId"></param>
		/// <returns></returns>
        public HttpResponseMessage Get(int postId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, this._repository.GetResult(postId).Select(domainObject =>
            {
                DisplayCommentEntryModel m = new DisplayCommentEntryModel();
                m.FillWithDomain(domainObject, Url);
                return (m);
            }));
        }
    }
}