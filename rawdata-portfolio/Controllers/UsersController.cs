﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using rawdata_portfolio.Models;
using DAL.Repository;
using DAL;
using DAL.Domain;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Users controller.
	/// </summary>
	public class UsersController : AbstractController<UserRepository, User, UserModel>
	{
		protected override string _routeName
		{
			get { return ("UserApi"); }
		}
	}
}
