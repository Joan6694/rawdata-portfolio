﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using rawdata_portfolio.Models;
using DAL.Repository;
using DAL.Domain;

namespace rawdata_portfolio.Controllers
{
	/// <summary>
	/// Tags controller.
	/// </summary>
	public class TagsController : AbstractController<TagRepository, Tag, TagModel>
	{
		protected override string _routeName
		{
			get { return ("TagApi"); }
		}
	}
}
