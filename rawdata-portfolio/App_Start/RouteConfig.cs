﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace rawdata_portfolio
{
    /// <summary>
    /// Class for the routing configuration
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Route for the homepage
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

	        routes.RouteExistingFiles = true;

			routes.MapRoute(
				name: "ApiHome",
				url: "api/",
				defaults: new { controller = "Home", action = "ApiHome" }
			);
		}
    }
}
