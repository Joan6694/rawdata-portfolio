﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace rawdata_portfolio
{
    /// <summary>
    ///  Class for the web api config
    /// </summary>
	public static class WebApiConfig
	{
        /// <summary>
        /// Routes for all our controllers
        /// </summary>
        /// <param name="config"></param>
		public static void Register(HttpConfiguration config)
		{
			// Configuration et services API Web

			// Itinéraires de l'API Web
			config.MapHttpAttributeRoutes();

			config.Routes.MapHttpRoute(
				name: "UserApi",
				routeTemplate: "api/users/{id}",
				defaults: new { controller = "Users", id = RouteParameter.Optional }
			);

			config.Routes.MapHttpRoute(
				name: "PostApi",
				routeTemplate: "api/posts/{id}",
				defaults: new { controller = "Posts", id = RouteParameter.Optional }
			);

			config.Routes.MapHttpRoute(
				name: "CommentApi",
				routeTemplate: "api/comments/{id}",
				defaults: new { controller = "Comments", id = RouteParameter.Optional }
			);

			config.Routes.MapHttpRoute(
				name: "PostCommentApi",
				routeTemplate: "api/posts/{id}/comments/",
				defaults: new { controller = "Comments", action = "GetPostComments" }
			);

			config.Routes.MapHttpRoute(
				name: "SearchHistoryEntryApi",
				routeTemplate: "api/searchhistoryentries/",
				defaults: new { controller = "SearchHistoryEntries" }
			);

			config.Routes.MapHttpRoute(
				name: "SearchHistorySearchApi",
				routeTemplate: "api/searchhistorysearch/",
				defaults: new { controller = "SearchHistoryEntries", action = "Search" }
			);

			config.Routes.MapHttpRoute(
				name: "TagApi",
				routeTemplate: "api/tags/{id}",
				defaults: new { controller = "Tags", id = RouteParameter.Optional }
			);

			config.Routes.MapHttpRoute(
				name: "AnnotationApi",
				routeTemplate: "api/posts/{id}/annotation",
				defaults: new { controller = "Annotations", id = RouteParameter.Optional }
			);

			config.Routes.MapHttpRoute(
				name: "AnnotationAllApi",
				routeTemplate: "api/annotations",
				defaults: new { controller = "Annotations" }
			);

			config.Routes.MapHttpRoute(
				name: "PostsSearchApi",
				routeTemplate: "api/search",
				defaults: new { controller = "PostsSearch" }
			);

			config.Routes.MapHttpRoute(
				name: "FavouritesSearchApi",
				routeTemplate: "api/favourites",
				defaults: new { controller = "FavouritesSearch" }
			);

			config.Routes.MapHttpRoute(
				name: "DisplayPostsApi",
				routeTemplate: "api/displayposts/{postId}",
				defaults: new { controller = "DisplayPosts" }
			);

			config.Routes.MapHttpRoute(
				name: "DisplayCommentsApi",
				routeTemplate: "api/displayposts/{postId}/comments",
				defaults: new { controller = "DisplayComments" }
			);

			var formatters = GlobalConfiguration.Configuration.Formatters;
			var jsonFormatter = formatters.JsonFormatter;
			var settings = jsonFormatter.SerializerSettings;
			settings.Formatting = Formatting.Indented;
			settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

			//config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

		}
	}
}
